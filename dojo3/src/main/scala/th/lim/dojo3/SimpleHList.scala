package th.lim.dojo3

import scala.language.implicitConversions

trait MetaValue[A] {
  def meta(value: A): Int
}

object MetaValue {
  given MetaValue[String] with {
    def meta(value: String): Int = value.length
  }

  given MetaValue[Int] with {
    def meta(value: Int): Int = math.min(50, value)
  }

  // replaces implicit def apply[A: MetaValue]: MetaValue[A] = implicitly
  def apply[A: MetaValue]: MetaValue[A] = summon
}

object MetaValueExt {
  given MetaValue[Boolean] with {
    def meta(value: Boolean): Int = if (value) 1 else 0
  }
}

object SimpleHList extends App {

  import MetaValueExt.given_MetaValue_Boolean

  case class AsInt(i: Int) extends AnyVal

  // replaces implicit def toAsInt[A: MetaValue](a: A): AsInt = AsInt(MetaValue[A].meta(a))
  given toAsInt[A: MetaValue]: Conversion[A, AsInt] with {
    def apply(a: A): AsInt = AsInt(MetaValue[A].meta(a))
  }

  // WARNING: If AsInt is replaced with Int, implicit IntMetaValue will not be executed for Int type
  val foo: List[AsInt] = List(true)
  val sample: List[AsInt] = foo ++ List("abc", 123, 12)
  assert(sample.map(_.i).equals(List(1, 3, 50, 12)))

  // WARNING: false :: (sample :+ true) does not work. It will have the type List[Any]
  val all: List[AsInt] = List(false, "abc", 123, 12, true)
  assert(all.map(_.i).equals(List(0, 3, 50, 12, 1)))
}
