package th.lim.dojo3

object Phantom extends App {

  trait Something[A] {
    def doSomething(xs: List[A]): Int
  }

  given SomethingInt: Something[Int] = (xs: List[Int]) => xs.sum

  given SomethingString: Something[String] = (xs: List[String]) => xs.mkString.length

  def foo[A](a: List[A])(using ev: Something[A]): Int = ev.doSomething(a)

  println(foo(List(1, 2, 3)))
  println(foo(List("1", "2", "3")))
  object Const {
    opaque type UserId = Int
    opaque type UserName = String

    object UserId {
      def apply(i: Int): UserId = i
    }

    object UserName {
      def apply(i: String): UserName = i
    }

    object UserAlias {
      def apply(i: String): UserName = i
    }
  }

  import Const.*

  case class User(id: UserId, name: UserName)

  private val u: User = User(Const.UserId(123), UserAlias("abcdef"))

  println(u)
}
