package th.lim.dojo3.server

import cats.Monad
import cats.data.Kleisli
import cats.effect.std.{Console, CountDownLatch}
import cats.effect.{Concurrent, ExitCode, IO, IOApp, Ref, Async}
import cats.implicits.toSemigroupKOps
import com.comcast.ip4s.{ipv4, port}
import fs2.io.net.Network
import org.http4s.dsl.io.*
import org.http4s.server.middleware.Logger as ServerLogger
import org.http4s.{HttpApp, HttpRoutes, Request, Response, Status}
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

import java.util.concurrent.TimeUnit
import scala.concurrent.duration.Duration
import scala.util.Try
//import org.http4s.dsl.io.{/, GET, Root}
import cats.syntax.all.{toFlatMapOps, toFunctorOps}
import org.http4s.ember.server.*
import org.http4s.implicits.*

object OptionalMessageQueryParam       extends OptionalQueryParamDecoderMatcher[String]("m")
object OptionalShutdownDelayQueryParam extends OptionalQueryParamDecoderMatcher[String]("ts")

object Main extends IOApp:
  val defaultShutdownDelay = 2

  private def echoRoutes[F[_]: Monad] = HttpRoutes.of[F]:
    case GET -> Root / "echo" :? OptionalMessageQueryParam(message) =>
      Monad[F].pure(
        Response[F](
          Status.Ok,
          body = fs2.Stream.emits(s"""You said, "${message.fold("nothing")(identity)}".""".getBytes()).covary[F]
        )
      )

  private def setupAdminRoutes[F[_]: Concurrent: Monad]: F[(Ref[F, Int], CountDownLatch[F], HttpRoutes[F])] =
    for
      shutdownDelay <- Ref.of[F, Int](0)
      exitLatch     <- CountDownLatch[F](1)
    yield (
      shutdownDelay,
      exitLatch,
      HttpRoutes.of[F]:
        case GET -> Root / "shutdown" :? OptionalShutdownDelayQueryParam(delay) =>
          for
            parsedDelay <- Monad[F].pure(parseNum(delay))
            _           <- shutdownDelay.set(parsedDelay)
            _           <- exitLatch.release
          yield Response(Status.Ok, body = fs2.Stream.emits(s"Shutdown in ${parsedDelay}s".getBytes).covary[F])
    )

  private def parseNum(raw: Option[String]): Int =
    raw.flatMap(s => Try(s.toInt).toOption).getOrElse(defaultShutdownDelay)

  private def makeHttpApp[F[_]: Async](routes: List[HttpRoutes[F]]): HttpApp[F] =
    routes.reduce(_ <+> _).orNotFound

  def loggerService[F[_]: Logger: Async]: HttpApp[F] => HttpApp[F] = ServerLogger.httpApp[F](
    logHeaders = false,
    logBody = true,
    redactHeadersWhen = _ => false,
    logAction = Some((msg: String) => Logger[F].info(msg))
  )

  def runServer[F[_]: Async: Network: Console]: F[Unit] =
    for
      (shutdownDelay, exitLatch, adminRoutes) <- setupAdminRoutes[F]
      given Logger[F]                         <- Slf4jLogger.create[F]
      _ <- EmberServerBuilder
        .default[F]
        .withHost(ipv4"0.0.0.0")
        .withPort(port"8080")
        .withHttpApp(loggerService[F].apply(makeHttpApp(List(echoRoutes, adminRoutes))))
        .build
        .use(_ =>
          for
            _     <- exitLatch.await
            delay <- shutdownDelay.get
            _     <- Console[F].println(s"Shutting down in ${delay}s...")
            _     <- Async[F].sleep(Duration.create(delay, TimeUnit.SECONDS))
          yield ()
        )
    yield ()

  def run(args: List[String]): IO[ExitCode] =
    runServer[IO] *> IO.println("Done.").as(ExitCode.Success) >> IO.never
