package th.lim.dojo3.concurrent

import cats.effect.*
import cats.effect.std.{Queue, Random}
import cats.syntax.all.*
import cats.syntax.apply.catsSyntaxApply
import cats.{Applicative, Monad}
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

import scala.concurrent.Future
import scala.concurrent.duration.{FiniteDuration, *}

object CatsSaloon extends IOApp.Simple {
  val maxChairs: Int = 20
  val openingSeconds: FiniteDuration = 3.second

  import cats.syntax.functor.*

  override def run: IO[Unit] =
    for {
      given Random[IO] <- Random.scalaUtilRandom[IO]
      logger <- Slf4jLogger.create[IO]
      closeShop <- Ref.of[IO, Boolean](false)
      _ <- signalCloseAfter[IO](closeShop, openingSeconds).start
      shopQueue <- Queue.bounded[IO, Option[Int]](maxChairs)
      _ <- customers[IO](closeShop, 10L, 20L, shopQueue, logger, 4).start
      f <- barber("Barber A", shopQueue, 50L, 100L, closeShop, logger).start
      g <- barber("Barber B", shopQueue, 100L, 200L, closeShop, logger).start
      g <- barber("Barber C", shopQueue, 100L, 300L, closeShop, logger).start
      _ <- f.joinWithNever
      _ <- g.joinWithNever
    } yield ()

  private def signalCloseAfter[F[_] : Temporal](
      closeShop: Ref[F, Boolean],
      closingSeconds: FiniteDuration
  ): F[Unit] =
    Temporal[F].sleep(closingSeconds) *> closeShop.set(true)

  private def delayBetween[F[_] : Random : Temporal](
    minRateMillis: Long,
    maxRateMillis: Long
  ): F[Unit] =
    Random[F]
      .betweenLong(minRateMillis, maxRateMillis)
      .flatMap(rate => Temporal[F].sleep(rate.millis))

  def barber[F[_]: Applicative: Temporal: Random](
      name: String,
      channel: Queue[F, Option[Int]],
      minRateMillis: Long,
      maxRateMillis: Long,
      closeShop: Ref[F, Boolean],
      logger: Logger[F]
  ): F[Unit] = {

    def barber_(
      id: Int
    ): F[Unit] =
      logger.info(s"$name cutting customer $id hair") *>
        delayBetween(minRateMillis, maxRateMillis) *>
        barber(name, channel, minRateMillis, maxRateMillis, closeShop, logger)

    channel.take.flatMap(_.fold(channel.offer(None) *> Applicative[F].unit)(id => barber_(id)))
  }

  def customers[F[_] : Temporal : Monad : Random](
      closeShop: Ref[F, Boolean],
      minRateMillis: Long,
      maxRateMillis: Long,
      channel: Queue[F, Option[Int]],
      logger: Logger[F],
      counter: Int
  ): F[Unit] =
    closeShop.get >>= (close =>
      if (close) channel.offer(None) *> logger.info("Shop is closed")
      else {
        delayBetween(minRateMillis, maxRateMillis) *>
          channel
            .tryOffer(Some(counter))
            .flatMap(available =>
              if (available) logger.info(s"Customer $counter walked in")
              else logger.info(s"Customer $counter LEAVE")
            ) *>
          customers(closeShop, minRateMillis, maxRateMillis, channel, logger, counter + 1)
      }
    )

  def foo: IO[List[Int]] = List(IO.pure(100)).sequence
}

/*
 private[this] val foo: cats.effect.IO[List[Int]] = cats.syntax.`package`.traverse.toTraverseOps[List, cats.effect.IO[Int]](scala.`package`.List.apply[cats.effect.IO[Int]](cats.effect.IO.pure[Int](100)))(cats.this.UnorderedFoldable.catsTraverseForList).sequence[cats.effect.IO, Int](scala.this.<:<.refl[cats.effect.IO[Int]], effect.this.IO.asyncForIO);
    
 */