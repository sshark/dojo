package th.lim.dojo3.concurrent

import kyo.*
import kyo.Ansi.*
import kyo.Result.{Fail, Success}

import scala.util.Try

object KyoRandNumGen extends KyoApp:
  val defaultParallel = 100
  val maxParallel     = 200

  run:
    Abort
      .run:
        for
          numParallel <- parseMaxRndArg(args)
          xs          <- tasks(numParallel)
        yield xs
      .map:
        case Fail(f) =>
          f match
            case x: String => Console.println(x.red)
            case _         =>
        case Success(ys) => Kyo.foreachDiscard(ys)(s => Console.println(s.green))

  private def parseMaxRndArg[F[_]](args: Array[String]): Int < Abort[String] =
    if args.isEmpty then defaultParallel
    else
      Result
        .fromTry(Try(args.head.toInt))
        .fold(_ => Abort.fail(s"""Cannot parse "${args.head}" as the max random ints"""))(r =>
          Abort.ensuring(r > 0 && r <= maxParallel, r)(s"Max random ints must be between 0 and $maxParallel")
        )

  private def tasks(max: Int): Seq[String] < (Async & Abort[Exception]) = Async.parallel((1 to max).map(tag))

  private def tag(id: Int): String < (IO & Abort[Exception]) =
    for
      rnd <- Random.nextInt.map(i => s"$id: ${math.abs(i)}")
      _   <- Console.println(rnd)
    yield rnd
end KyoRandNumGen

object StreamApp extends KyoApp:
  def lazyNums: Stream[Int, Abort[String]] =
    Stream
      .init(1 to 100)
      .map(_ * 10)
      .map(x => if x > 200 then Abort.fail(s"$x is greater than 20") else Abort.get(Right(x)))

  // for App
  val effect: Unit < (Abort[String] & IO) = lazyNums.runForeach(Console.println)

  import kyo.AllowUnsafe.embrace.danger

  val result: Result[String, Unit] = effect
    .pipe(Abort.run(_))
    .pipe(IO.Unsafe.run)
    .eval

  result match
    case Fail(t) => println(t)
    case _       =>

  // with KyoApp
  run {
    Abort
      .run {
        effect
      }
      .map {
        case Fail(t) => Console.println(t)
        case _       => ()
      }
  }

object KyoAsyncParallel extends KyoApp:
  val cos: Double < Async =
    for
      rad   <- Random.nextInt(90).map(_.toDouble)
      ratio <- Async.run(math.cos(rad)).map(_.get)
    yield ratio

  val program: Seq[Double] < (Async & Abort[Exception]) =
    for
      xs <- Async.parallel((1 to 20).map(_ => cos))
      _  <- Kyo.foreach(xs)(Console.println)
    yield xs

  var bar: Unit < Async =
    Abort
      .run(program)
      .map:
        case Success(xs) => Console.println(s"Success, found ${xs.size} elements")
        case _           => ()

  run:
    bar
end KyoAsyncParallel

object KyoParallelSleep extends KyoApp:
  val program: (Seq[Int], Long) < Async =
    for
      startMillis  <- IO(java.lang.System.currentTimeMillis())
      xs           <- Async.parallel((1 to 2_000).map(_ => Async.sleep(1.second).map(_ => 1)))
      elapsedMills <- IO(java.lang.System.currentTimeMillis() - startMillis)
    yield (xs, elapsedMills)

  var task: Unit < Async =
    Abort
      .run(program)
      .map:
        case Success(result) => Console.println(s"Success, ${result._1.sum} elements slept for ${result._2}ms")
        case _               => ()

  run:
    task // approx 7s to complete

object RawThread extends App:
  val foo: Runnable = () => Thread.sleep(1000)
  val start         = java.lang.System.currentTimeMillis()
  val tasks         = (1 to 2_000_000).map(_ => Thread(foo))
  tasks.foreach(_.start())
  tasks.foreach(_.join())
  println(s"Done, it took ${java.lang.System.currentTimeMillis() - start}ms")
  // approx 353s to complete
