package th.lim.dojo3.concurrent

import cats.effect.std.{Random, UUIDGen}
import cats.effect.{IO, IOApp, Ref}
import cats.syntax.all.*

import scala.concurrent.duration.*

case class ProducerProps(minWait: Long, maxWait: Long, rndGen: IO[Random[IO]])

object CatsWorld extends IOApp.Simple {

  val GOLD = "XAU"
  override def run: IO[Unit] = for {
    conflatingRef <- Ref[IO].of(false)
    lastUUIDRef   <- Ref[IO].of("")
    storageRef    <- Ref[IO].of(Map.empty[String, (String, Double)])
    startTime     <- IO.realTime
    _ <- producer(
      GOLD,
      ProducerProps(200L, 800L, Random.scalaUtilRandom[IO]),
      storageRef,
      startTime
    ).foreverM.start
    _ <- consumer(GOLD, storageRef, conflatingRef, lastUUIDRef, startTime).foreverM.start
    _ <- IO.sleep(60.second)
  } yield ()

  def consumer(
      name: String,
      storageRef: Ref[IO, Map[String, (String, Double)]],
      conflatingRef: Ref[IO, Boolean],
      lastUUIDRef: Ref[IO, String],
      startTime: FiniteDuration
  ): IO[Unit] =
    for {
      conflating <- conflatingRef.get
      lastUUID   <- lastUUIDRef.get
      storage    <- storageRef.get
      pairOption = storage.get(name)
      now <- IO.realTime
      diff = (now - startTime).toSeconds
      _ <-
        if (conflating)
          pairOption.fold(IO.unit)(pair => if (lastUUID == pair._1) conflatingRef.set(false)
          else
            lastUUIDRef.set(pair._1) *> IO.println(s"[${diff}s] ### $pair") >> IO.sleep(1.second))
        else pairOption.fold(IO.unit)(pair => if (lastUUID == pair._1) IO.unit else lastUUIDRef.set(pair._1) *> IO.println(s"[${diff}s] >>> $pair") *>
            IO.sleep(700.millis) *> conflatingRef.set(true))
    } yield ()

  def producer(
      name: String,
      props: ProducerProps,
      storage: Ref[IO, Map[String, (String, Double)]],
      startTime: FiniteDuration
  ): IO[Unit] =
    for {
      currentTime <- IO.realTime
      rnd         <- props.rndGen
      duration    <- rnd.betweenLong(200L, 800L)
      price       <- rnd.betweenDouble(1d, 1.5d)
      uuid        <- UUIDGen.randomString[IO]
      _ <-
        if (tens(currentTime - startTime) % 2 == 0) IO.sleep(duration.millis)
        else IO.sleep(1.second + duration.millis)
      _ <- storage.updateAndGet(m => m.updated(name, (uuid, price)))
    } yield ()

  def tens(x: FiniteDuration) = (x.toSeconds / 10).toString.last - 0x30
}
