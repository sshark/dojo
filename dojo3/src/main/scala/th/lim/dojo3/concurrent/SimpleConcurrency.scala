package th.lim.dojo3.concurrent

import cats.effect.{Async, ExitCode, IO, IOApp}

import java.util.concurrent.{Executors, TimeUnit}
import scala.concurrent.duration.Duration
import scala.concurrent.{ExecutionContext, ExecutionContextExecutor}

object SimpleConcurrency extends IOApp {
  def delayTask(name: String, millis: Long): IO[Unit] =
    for {
      threadName <- IO(Thread.currentThread().getName)
      _ <- IO(println(s"Starting $name... in $threadName"))
      _ <- IO.sleep(Duration(millis, TimeUnit.MILLISECONDS))
      _ <- IO(println(s"$name done."))
    } yield ()

  override def run(args: List[String]): IO[ExitCode] = {
    val ec1 = Executors.newSingleThreadExecutor()
    val ec2 = Executors.newFixedThreadPool(8)
    val singleCE: ExecutionContextExecutor = ExecutionContext.fromExecutor(ec1)
    val manyCE: ExecutionContextExecutor = ExecutionContext.fromExecutor(ec2)

    timedRun(
      Async[IO]
        .evalOn(timedRun(runAsync), singleCE)
        .flatMap(elapsed => IO(println(s"Time taken: ${elapsed}ms"))) *> IO(
        ec1.shutdown()
      ) *>
        Async[IO]
          .evalOn(timedRun(runAsync), manyCE)
          .flatMap(elapsed => IO(println(s"Time taken: ${elapsed}ms"))) *> IO(
        ec2.shutdown()
      ) *>
        timedRun(runAsync).flatMap(elapsed => IO(println(s"Time taken: ${elapsed}ms")))
    ).flatMap(elapsed => IO(println(s"Total time taken: ${elapsed}ms"))) *> IO(ExitCode.Success)
  }

  private def timedRun(ioTask: IO[Unit]): IO[Long] =
    for {
      start <- IO(System.currentTimeMillis())
      _ <- ioTask
      now <- IO(System.currentTimeMillis())
    } yield now - start

  private def runAsync: IO[Unit] =
    for {
      a <- delayTask("A", 1000).start
      b <- delayTask("B", 1000).start
      _ <- a.joinWithNever
      _ <- b.joinWithNever
    } yield ()
}
