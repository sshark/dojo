package th.lim.dojo3.concurrent

import cats.effect.*
import cats.effect.std.{Console, Random}
import cats.effect.syntax.all.genSpawnOps
import cats.syntax.all.{toFlatMapOps, toFunctorOps, toTraverseOps}
import cats.{Monad, MonadError}

import scala.util.{Failure, Success, Try}

object CatsRandNumGen extends IOApp:
  val defaultParallel = 100
  val maxParallel     = 200

  override def run(args: List[String]): IO[ExitCode] =
    for
      given Random[IO] <- Random.scalaUtilRandom[IO]
      exitCode         <- program[IO](args).handleErrorWith(t => IO.println(t.getMessage).as(ExitCode.Error))
    yield exitCode

  def program[F[_]: Console: Random: Spawn: [g[_]] =>> MonadError[g, Throwable]](args: List[String]): F[ExitCode] =
    for
      maxNums <- parseRowsArg[F](args)
      xs      <- (1 to maxNums).toList.traverse(i => positiveRndInt.map(j => s"$i: $j"))
      _       <- xs.map(s => Console[F].println(s).start).sequence
    yield ExitCode.Success

  def positiveRndInt[F[_]: Monad: Random]: F[Int] = Random[F].nextInt.map(math.abs)

  private def parseRowsArg[F[_]](args: List[String])(implicit me: MonadError[F, Throwable]): F[Int] =
    if args.isEmpty then me.pure(defaultParallel)
    else
      me.fromTry(
        Try(args.head.toInt).transform(
          r =>
            if r > 0 && r <= maxParallel then Success(r)
            else Failure(Exception(s"Max random ints must be between 0 and $maxParallel")),
          _ => Failure(Exception(s"""Cannot parse "${args.head}" as the max random ints"""))
        )
      )
