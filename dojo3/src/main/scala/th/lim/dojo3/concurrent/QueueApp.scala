package th.lim.dojo3.concurrent

import cats.effect.{Deferred, IO, IOApp, Ref}

import scala.concurrent.duration.*

object QueueApp extends IOApp.Simple {
  override def run: IO[Unit] = for {
    start <- IO.realTime
    queue <- Queue[Int]
    _     <- (IO.println("Put 1") *> queue.put(1)).start
    _     <- (IO.println("Put 2") *> queue.put(2)).start
    _     <- (IO.println("Put 3") *> queue.put(3)).start
    _     <- (IO.println("Put 4") *> queue.put(4)).start
    a     <- queue.take
    b     <- queue.take
    c     <- queue.take
    d     <- queue.take
    f3    <- queue.take.start
    _     <- IO.sleep(1.second) *> queue.put(10)
    e     <- f3.joinWithNever
    _     <- IO.println(s"output: ${List(a, b, c, d, e)}")
    end   <- IO.realTime
    _     <- IO.println(s"Total time taken: ${(end - start).toMillis}ms")
  } yield ()
}

trait Queue[A] {
  def put(a: A): IO[Unit]
  def tryPut(a: A): IO[Boolean]
  def take: IO[A]
  def tryTake: IO[Option[A]]
  def peek: IO[Option[A]]
}

final case class State[A](values: List[A], waiter: List[Deferred[IO, A]])

object Queue {
  def apply[A]: IO[Queue[A]] = for {
    stateRef <- IO.ref(State(List.empty[A], List.empty[Deferred[IO, A]]))
  } yield new Queue[A] {
    override def put(a: A): IO[Unit] =
      stateRef.flatModify(s =>
        if (s.waiter.isEmpty) {
          val xs = s.values :+ a
          State(xs, s.waiter) -> IO.println(s"put($a): $xs")
        } else
          State(List.empty[A], s.waiter.tail) ->
            (s.waiter.head.complete(a) *>
              IO.println(s"put($a): complete deferred value"))
      )

    /** A more complicated solution using `Ref.access` but it only creates `Deferred` once and only
      * if necessarily. However, it does not necessarily gives better performance compared to the
      * simpler `flatModify` implementation
      */
    @SuppressWarnings(Array("org.wartremover.warts.Null"))
    def take_access: IO[A] = {
      def take_(ref: Ref[IO, State[A]], deferred: Deferred[IO, A]): IO[A] =
        ref.access.flatMap((state, setter) =>
          if (state.values.isEmpty)
            if (deferred == null)
              IO.deferred[A]
                .flatMap(deferred =>
                  setter(State(state.values, state.waiter :+ deferred))
                    .ifM(deferred.get, take_(ref, deferred))
                )
            else
              setter(State(state.values, state.waiter :+ deferred))
                .ifM(deferred.get, take_(ref, deferred))
          else
            setter(State(state.values.tail, state.waiter))
              .ifM(IO.pure(state.values.head), take_(ref, deferred))
        )

      take_(stateRef, null)
    }

    /** A simpler alternative to take_access
      */
    override def take: IO[A] =
      IO.deferred[A]
        .flatMap(deferred =>
          stateRef.flatModify(state =>
            if (state.values.isEmpty)
              State(state.values, state.waiter :+ deferred) -> deferred.get
            else
              State(state.values.tail, state.waiter) -> IO.pure(state.values.head)
          )
        )

    override def tryPut(a: A): IO[Boolean] =
      stateRef.modify(s => (State(s.values :+ a, s.waiter), true))

    override def tryTake: IO[Option[A]] = stateRef.flatModify(state =>
      if (state.values.isEmpty) state             -> IO.pure(None)
      else State(state.values.tail, state.waiter) -> IO.pure(state.values.headOption)
    )

    override def peek: IO[Option[A]] = stateRef.modify(state =>
      if (state.values.isEmpty) state -> None else state -> state.values.headOption
    )
  }
}
