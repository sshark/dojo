package th.lim.dojo3.concurrent

import cats.effect.{IO, IOApp}
import cats.implicits.catsSyntaxFlatMapOps

object ParTraverseApp extends IOApp.Simple {
  override def run: IO[Unit] = {
    def parTraverse[A, B](as: List[A])(f: A => IO[B]): IO[List[B]] =
      as.map(a => f(a).start)
        .foldLeft(IO.pure(List.empty[B]))((ioList, ioFibre) =>
          IO.both(ioList, ioFibre).flatMap((xs, f) => f.joinWithNever.map(xs :+ _))
        )

    (parTraverse(List(1, 2, 3))(x => IO.pure(x + 1))) >>= (IO.println)
  }
}
