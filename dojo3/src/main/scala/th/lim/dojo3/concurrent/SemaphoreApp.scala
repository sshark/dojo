package th.lim.dojo3.concurrent

import cats.effect.{Deferred, IO, IOApp}

import scala.concurrent.duration.*

object SemaphoreApp extends IOApp.Simple {
  override def run: IO[Unit] = for {
    s <- Semaphore(3)
    start <- IO.realTime
    f1 <- (s.acquire *> IO.println("Task 1") *> IO.sleep(1.second) *> s.release).start
    f2 <- (s.acquire *> IO.println("Task 2") *> IO.sleep(2.second) *> s.release).start
    f3 <- (s.acquire *> IO.println("Task 3") *> IO.sleep(3.second) *> s.release).start
    _ <- IO.sleep(100.millis)
    f4 <- (s.acquire *> IO.println("Task 4") *> IO.sleep(3.second) *> s.release).start
    f5 <- (s.acquire *> IO.println("Task 5") *> s.release).start
    _ <- f1.joinWithNever
    _ <- f4.joinWithNever
    _ <- f5.joinWithNever
    _ <- f2.joinWithNever
    _ <- f3.joinWithNever
    finish <- IO.realTime
    _ <- IO.println(s"Total time taken to complete is ${(finish - start).toMillis}ms")
  } yield ()

  final case class State(max: Int, curr: Int, waiter: List[Deferred[IO, Unit]])

  trait Semaphore {
    def acquire: IO[Unit]

    def release: IO[Unit]
  }
  object Semaphore {
    def apply(permits: Int): IO[Semaphore] =
      for {
        stateRef <- IO.ref(State(permits, 0, List.empty[Deferred[IO, Unit]]))
      } yield new Semaphore {
        override def acquire: IO[Unit] =
          for {
            latch <- IO.deferred[Unit]
            _ <- stateRef.flatModify(state =>
              if (state.max == state.curr)
                State(state.max, state.curr, state.waiter :+ latch) -> latch.get
              else state.copy(curr = state.curr + 1) -> IO.unit
            )
          } yield ()

        override def release: IO[Unit] = stateRef.flatModify {
          case State(max, now, xs) if xs.isEmpty =>
            State(max, now - 1, xs) -> IO.unit
          case State(max, now, xs) =>
            State(max, now - 1, xs.drop(1)) -> xs.headOption.fold(IO.unit)(_.complete(()))
        }.void
      }
  }
}
