package th.lim.dojo3.concurrent

import cats.effect.std.Semaphore
import cats.effect.{IO, IOApp, Ref}

object OddEvenConCounter extends IOApp.Simple {
  val max: Int = 20

  override def run: IO[Unit] = for {
    lock <- Semaphore[IO](1)
    counter <- Ref.of[IO, Int](1)
    f1 <- runMe(counter, lock).whileM_(counter.get.map(_ < max)).start
    f2 <- runMe(counter, lock).whileM_(counter.get.map(_ < max)).start
    _ <- f1.joinWithNever
    _ <- f2.joinWithNever
  } yield ()

  private def runMe(counter: Ref[IO, Int], lock: Semaphore[IO]): IO[Unit] = for {
    _ <- lock.acquire
    name <- IO(Thread.currentThread().getName)
    i <- counter.getAndUpdate(_ + 1)
    _ <- IO.println(s"$i ($name)")
    _ <- lock.release
  } yield ()
}
