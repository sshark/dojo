package th.lim.dojo3.concurrent

import cats.effect.{ExitCode, IO, IOApp}

import scala.concurrent.duration.DurationInt

object TimedConsoleInput extends IOApp {

  override def run(args: List[String]): IO[ExitCode] = for {
    _ <- IO.println("What's your name? ")
    fiber <- IO.readLine.timeout(3.second).start
    status <- fiber.joinWith(IO.pure("Cancelled")).attempt
    _ <- status.fold(
      _ => IO.println("Timeout. You didn't enter anything"),
      name => IO.println(s"Hi $name")
    )
  } yield ExitCode.Success
}
