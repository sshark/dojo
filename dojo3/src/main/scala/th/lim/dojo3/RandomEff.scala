package th.lim.dojo3

import cats.effect.std.{Console, Random, SecureRandom}
import cats.effect.{IO, IOApp}
import cats.syntax.all.{toFlatMapOps, toFunctorOps}
import cats.{FlatMap, Functor}

/** An example of using Random[F] in additional to
  * https://typelevel.org/cats-effect/docs/std/random#using-random
  */
object RandomEff extends IOApp.Simple {
  override def run: IO[Unit] = showMagicNumber3("rnd", Random.scalaUtilRandom[IO])
    *> showMagicNumber("sec-rnd", SecureRandom.javaSecuritySecureRandom)

  // Scala 2.x & 3.x
  def showMagicNumber[F[_]: Console: FlatMap](id: String, rnd: F[Random[F]]): F[Unit] =
    rnd.flatMap(implicit rnd => dieRoll[F].flatMap(i => Console[F].println(s"$id: $i")))

  // Scala 3.x only
  def showMagicNumber3[F[_]: Console: FlatMap](id: String, rnd: F[Random[F]]): F[Unit] =
    for {
      given Random[F] <- rnd
      i               <- dieRoll[F]
      _               <- Console[F].println(s"$id: $i")
    } yield ()

  def dieRoll[F[_]: Functor: Random]: F[Int] = Random[F].betweenInt(0, 6).map(_ + 1)
}
