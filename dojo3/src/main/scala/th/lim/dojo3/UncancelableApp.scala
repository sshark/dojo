package th.lim.dojo3

import cats.effect.{IO, IOApp}

import java.util.concurrent.TimeUnit
import scala.concurrent.duration.Duration

object UncancelableApp extends IOApp.Simple {
  override def run: IO[Unit] =
    for {
      fiber <- IO.uncancelable { poll =>
        for {
          _ <- IO.println("waiting...")
          _ <- IO.sleep(Duration(800, TimeUnit.MICROSECONDS))
          _ <- poll(IO.println("done"))
        } yield ()
      }.start
      _ <- IO.sleep(Duration(500, TimeUnit.MICROSECONDS)) *> fiber.cancel
    } yield ()
}
