package th.lim.dojo3

import cats.effect.{IO, IOApp}
import cats.implicits.*

object ApplicativeApp extends IOApp.Simple {

  type Func1[Y] = Int => Y

  val add1  = (x: Int) => x + 1
  val addXY = (x: Int) => (y: Int) => x + y

  val foobar: Func1[Int] = addXY.pure[[X] =>> Func1[X]] <*> add1 <*> add1

  override def run: IO[Unit] = IO.println(foobar(10))
}
