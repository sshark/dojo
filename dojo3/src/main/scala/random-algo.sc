def powerSet[A](xs: List[A]): List[List[A]] =
  xs.foldLeft(List(List.empty[A]))((ys, y) => ys.map(zs => y :: zs) ++ ys)

powerSet(List(1, 2, 3)).reverse
