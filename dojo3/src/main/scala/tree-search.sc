import scala.annotation.tailrec

trait Tree[A](val value: A)
case class Node[A](nodeValue: A, children: List[Tree[A]]) extends Tree[A](nodeValue)
case class Leaf[A](leafValue: A)                          extends Tree[A](leafValue)

val tree: Tree[Int] =
  Node(
    1,
    List(
      Node(2, List(Node(5, List(Leaf(12))))),
      Node(3, List(Leaf(6), Leaf(7))),
      Node(4, List(Leaf(8), Node(9, List(Leaf(11), Leaf(10)))))
    )
  )

def bfs[A](tree: Tree[A]): List[A] =
  @tailrec
  def _bfs(nodes: List[Tree[A]], acc: List[A]): List[A] =
    if (nodes.isEmpty) acc
    else
      val nextNodes = nodes.flatMap:
        case Node(_, children) => children
        case _                 => List.empty

      _bfs(nextNodes, acc ++ nodes.map(_.value))

  tree match
    case Node(n, children) => _bfs(children, List(n))
    case Leaf(n)           => List(n)

val bfsExpected = List(1, 2, 3, 4, 5, 6, 7, 8, 9, 12, 11, 10)
val bfsResult = bfs(tree)
if bfsResult == bfsExpected then
  println(s"BFS: ${bfsResult.mkString(",")}")
else println(s"BFS failed: ${bfsResult.mkString(",")}, expected: ${bfsExpected.mkString(",")}")

def dfs[A](tree: Tree[A]): List[A] =
  @tailrec
  def _dfs(nodes: List[Tree[A]], localAcc: List[A], acc: List[A]): List[A] =
    if nodes.isEmpty then acc
    else
      nodes.head match
        case Node(n, children) => _dfs(children ++ nodes.tail, localAcc ++ List(n), acc)
        case Leaf(n)           => _dfs(nodes.tail, List.empty, acc ++ localAcc ++ List(n))

  tree match
    case Node(n, children) => _dfs(children, List.empty[A], n :: List.empty[A])
    case Leaf(n)           => List(n)

val dfsExpected = List(1, 2, 5, 12, 3, 6, 7, 4, 8, 9, 11, 10)
val dfsResult = dfs(tree)
if dfsResult == dfsExpected then
  println(s"DFS: ${dfsResult.mkString(",")}")
else println(s"DFS failed, ${dfsResult.mkString(",")}, expected: ${dfsExpected.mkString(",")}")
