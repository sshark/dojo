// class Eval a where
//   eval :: a -> Int
trait Eval[A]:
  def eval(expr: A): Int

//////////////////////////////////////////
// HERE'S THE MAGIC
//
// data Expr = forall t. Eval t => Expr t
//
// This form of declaration is used because
// Eval and Expr has cyclic dependency and
// it is convenient for Expr to have type 'T'
// instead of type constructor
trait Expr:
  type T
  val t: T
  val evalInst: Eval[T]

object Expr:
  def apply[T0: Eval](t0: T0) =
    new Expr :
      type T = T0
      val t: T = t0
      val evalInst: Eval[T] = implicitly[Eval[T]]

// Default boxing is needed
given toExpr[T: Eval]: Conversion[T, Expr] with
  def apply(t: T): Expr = Expr(t)

// instance Eval Expr where
//   eval (Expr e) = eval e
given exprInstance: Eval[Expr] with
  def eval(e: Expr): Int = e.evalInst.eval(e.t)

// NOTE: Haskell's name lookup makes this unnecessary; however, in order to
// avoid explicitly specifying `exprInstance.eval` each time when we call it
// recursively, simply define an alias.
def evaluate[T]: Expr => Int = exprInstance.eval

// data BaseExpr = Const Int | Add Expr Expr | Mul Expr Expr
sealed abstract class BaseExpr
case class Const(c: Int) extends BaseExpr
case class Add(lhs: Expr, rhs: Expr) extends BaseExpr
case class Mul(lhs: Expr, rhs: Expr) extends BaseExpr

// instance Eval BaseExpr where
//   eval (Const n) = n
//   eval (Add a b) = eval a + eval b
//   eval (Mul a b) = eval a * eval b
given Eval[BaseExpr] with
  def eval(baseExpr: BaseExpr): Int =
    baseExpr match
      case Const(c) => c
      case Add(lhs, rhs) => evaluate(lhs) + evaluate(rhs)
      case Mul(lhs, rhs) => evaluate(lhs) * evaluate(rhs)

// Implicit conversions for all base expressions
//
// NOTE: This is only needed because we created a new hierarchy instead of
// enrolling each type to `Eval` on its own.  Otherwise, the boxing function
// would suffice.
given toBaseExprToExpr[T <: BaseExpr]: Conversion[T, Expr] with
  def apply(t: T) = Expr[BaseExpr](t)

///////////////////////////////////////////////
// Possibly somewhere else (other module/lib).
///////////////////////////////////////////////

// data SubExpr = Sub Expr Exp
case class SubExpr(lhs: Expr, rhs: Expr)

// instance Eval SubExpr where
//   eval (Sub a b) = eval a - eval b
given Eval[SubExpr] with
  def eval(subExpr: SubExpr): Int =
    evaluate(subExpr.lhs) - evaluate(subExpr.rhs)

// NOTE: We don't have to provide an implicit conversion to Expr as the
// default `box` one suffices.

object Test:
  val exprs: List[Expr] = List(
    SubExpr(Add(Const(2), Const(10)), Const(3)),
    SubExpr(Mul(Const(10), Const(10)), SubExpr(Const(1), Const(2)))
  )

Test.exprs.map(evaluate).foreach(println)
