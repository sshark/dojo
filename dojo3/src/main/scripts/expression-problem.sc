trait I[F[_], A] {
  def lit(value: Int): F[A]
}

trait B[F[_], A] {
  def lit(value: Boolean): F[A]
}

trait Or[F[_], A] {
  def or(a: F[A], b: F[A]): F[A]
}

trait And[F[_], A] {
  def and(a: F[A], b: F[A]): F[A]
}

trait Sum[F[_], A] {
  def sum(a: F[A], b: F[A]): F[A]
}

trait LEq[F[_], A, B] {
  def leq(a: F[B], b: F[B]): F[A]
}

case class Expr[A](value: A)

object Eval {
  given intEval: I[Expr, Int] with {
    override def lit(value: Int) = Expr(value)
  }

  given boolEval: B[Expr, Boolean] with {
    override def lit(value: Boolean) = Expr(value)
  }

  given orEval: Or[Expr, Boolean] with {
    override def or(a: Expr[Boolean], b: Expr[Boolean]) = Expr(
      a.value || b.value
    )
  }

  given andEval: And[Expr, Boolean] with {
    override def and(a: Expr[Boolean], b: Expr[Boolean]) = Expr(
      a.value && b.value
    )
  }

  given sumEval: Sum[Expr, Int] with {
    override def sum(a: Expr[Int], b: Expr[Int]) = Expr(a.value + b.value)
  }
}

object Show {
  given intShow: I[Expr, String] with {
    override def lit(value: Int) = Expr(value.toString)
  }

  given boolShow: B[Expr, String] with {
    override def lit(value: Boolean) = Expr(value.toString)
  }

  given orShow: Or[Expr, String] with {
    override def or(a: Expr[String], b: Expr[String]): Expr[String] = Expr(
      s"${a.value} || ${b.value}"
    )
  }

  given andShow: And[Expr, String] with {
    override def and(a: Expr[String], b: Expr[String]): Expr[String] = Expr(
      s"${a.value} && ${b.value}"
    )
  }

  given sumShow: Sum[Expr, String] with {
    override def sum(a: Expr[String], b: Expr[String]) = Expr(s"${a.value} + ${b.value}")
  }
}

object EvalLEq {
  given leqEval: LEq[Expr, Boolean, Int] with {
    override def leq(a: Expr[Int], b: Expr[Int]) = Expr(a.value <= b.value)
  }
}

object ShowLEq {
  given leqShow: LEq[Expr, String, Int] with {
    override def leq(a: Expr[Int], b: Expr[Int]) = Expr(s"${a.value} <= ${b.value}")
  }
}

trait Program[F[_], A] {
  def run: F[A]
}

def program0[F[_], A](using b: B[F, A], o: Or[F, A], a: And[F, A]) =
  new Program[F, A] {

    import a.*
    import b.*
    import o.*

    override val run: F[A] =
      or(b.lit(true), and(b.lit(true), b.lit(false)))
  }

import Eval.given
import Show.given

program0[Expr, Boolean].run.value
program0[Expr, String].run.value

def program1[F[_], A, C](using b: B[F, A], o: Or[F, A], a: And[F, A], l: LEq[F, A, C], i: I[F, C]) =
  new Program[F, A] {

    import a.*
    import l.*
    import o.*

    override val run: F[A] =
      or(b.lit(true), and(b.lit(true), leq(i.lit(11), i.lit(2))))
  }

import EvalLEq.given
import ShowLEq.given

program1[Expr, Boolean, Int].run.value
program1[Expr, String, Int].run.value
