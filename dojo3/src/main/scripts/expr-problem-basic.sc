// Using HKT, E[_], allows the use of Either, IO etc. to return error results
trait Algebra[E[_]] {
  def b(boolean: Boolean): E[Boolean]
  def i(int: Int): E[Int]
  def or(left: E[Boolean], right: E[Boolean]): E[Boolean]
  def and(left: E[Boolean], right: E[Boolean]): E[Boolean]
  def sum(left: E[Int], right: E[Int]): E[Int]
}

type Id[A] = A

object IdEvalAlg {
  given idAlg: Algebra[Id] with {
    override def b(value: Boolean) = value
    override def i(value: Int) = value
    override def or(left: Id[Boolean], right: Id[Boolean]) = left || right
    override def and(left: Id[Boolean], right: Id[Boolean]) = left && right
    override def sum(left: Id[Int], right: Id[Int]) = left + right
  }
}

import IdEvalAlg.given

def program1[E[_]](using alg: Algebra[E]): E[Boolean] = {
  import alg.*
  or(b(true), and(b(true), b(false)))
}

def program2[E[_]](using alg: Algebra[E]): E[Int] = {
  import alg.*
  sum(i(24), i(-3))
}

program1[Id]
program2[Id]

case class Expr[A](value: A)

object EvalAlg {
  given simpleExprAlg: Algebra[Expr] with {
    override def b(boolean: Boolean) = Expr(boolean)

    override def i(int: Int) = Expr(int)

    override def or(left: Expr[Boolean], right: Expr[Boolean]) =
      Expr(left.value || right.value)

    override def and(left: Expr[Boolean], right: Expr[Boolean]) =
      Expr(left.value && right.value)

    override def sum(left: Expr[Int], right: Expr[Int]) =
      Expr(
        left.value + right.value
      )
  }
}

import EvalAlg.given

program1[Expr].value
program2[Expr].value
