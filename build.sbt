ThisBuild / organization := "org.teckhooi"
ThisBuild / version      := "1.0-SNAPSHOT"
ThisBuild / name         := """Code Dojo"""
ThisBuild / Compile / compile / wartremoverErrors ++= Warts.unsafe

val scala2Version = "2.13.11"
val scala3Version = "3.5.1"

val catsEffectVersion = "3.5.2"
val configVersion     = "1.3.0"
val declineVersion    = "2.4.0"
val fastParseVersion  = "2.2.2"
val fs2Version        = "3.4.0"
val httpVersion       = "4.5.2"
val http4sVersion     = "0.23.29"
val javaSlangVersion  = "2.0.2"
val json4sVersion     = "3.6.7"
val junitInfVersion   = "0.11"
val junitTestVersion  = "4.13-beta-1"
val kyoVersion        = "0.14.0"
val logbackVersion    = "1.4.0"
val log4catsVersion   = "2.6.0"
val projectReactor    = "3.5.7"
val pureConfigVersion = "0.12.2"
val scalaTestVersion  = "3.2.16"
val shapelessVersion  = "2.3.3"
val uTestVersion      = "0.7.10"
val zioVersion        = "2.0.16"

lazy val makeNativeImage = taskKey[Unit]("Make native image using Graavl")

//enablePlugins(GraalVMNativeImagePlugin)

lazy val dojo = project
  .in(file("."))
  .aggregate(dojo2, dojo3)

/*
graalVMNativeImageCommand := {
if (System.getProperty("os.name").toLowerCase.startsWith("windows"))
"C:/Program Files/Java/graalvm/bin/native-image.cmd"
else
"native-image"
},
mainClass in GraalVMNativeImage := Some("org.teckhooi.fp.dojo2.sudoku.CatSudokuApp"),
name in GraalVMNativeImage := "pusheen-run",
target in GraalVMNativeImage := target.value / "pusheen-native-image",
graalVMNativeImageOptions := Seq(
"--no-fallback",
"--report-unsupported-elements-at-runtime",
"--initialize-at-build-time=scala.runtime.Statics$VM",
s"-H:ResourceConfigurationFiles=${(resourceDirectory in Compile).value}/resources-config.json"
),
 */

lazy val dojo2 = project
  .in(file("dojo2"))
  .settings(
    scalaVersion            := scala2Version,
    Compile / doc / sources := Seq.empty,
    logLevel                := util.Level.Info,
    addCompilerPlugin(("org.typelevel" %% "kind-projector" % "0.13.2").cross(CrossVersion.full)),
    scalacOptions ++= Seq("-unchecked", "-deprecation", "-feature"),
    libraryDependencies ++= Seq(
      "co.fs2"                   %% "fs2-core"        % fs2Version,
      "com.monovore"             %% "decline-effect"  % declineVersion,
      "com.lihaoyi"              %% "fastparse"       % fastParseVersion,
      "com.github.pureconfig"    %% "pureconfig"      % pureConfigVersion,
      "com.chuusai"              %% "shapeless"       % shapelessVersion,
      "org.typelevel"            %% "cats-effect"     % catsEffectVersion,
      "org.json4s"               %% "json4s-native"   % json4sVersion,
      "com.typesafe"              % "config"          % configVersion,
      "dev.zio"                  %% "zio-streams"     % zioVersion,
      "org.apache.httpcomponents" % "fluent-hc"       % httpVersion,
      "org.typelevel"            %% "log4cats-slf4j"  % log4catsVersion,
      "io.projectreactor"         % "reactor-core"    % projectReactor,
      "io.projectreactor"         % "reactor-test"    % projectReactor   % Test,
      "junit"                     % "junit"           % junitTestVersion % Test,
      "com.novocode"              % "junit-interface" % junitInfVersion  % Test,
      "org.scalactic"            %% "scalactic"       % scalaTestVersion % Test,
      "org.scalatest"            %% "scalatest"       % scalaTestVersion % Test
    )
  )

val dojo3 = project
  .in(file("dojo3"))
//  .enablePlugins(JmhPlugin)
  .settings(
    //    Compile / compile / wartremoverErrors ++= Warts.unsafe,
    scalacOptions ++= Seq("-unchecked", "-deprecation", "-feature", "-rewrite", "-source", "3.4-migration"),
    libraryDependencies ++= Seq(
      "ch.qos.logback" % "logback-classic"     % logbackVersion,
      "org.http4s"    %% "http4s-ember-server" % http4sVersion,
      "org.http4s"    %% "http4s-dsl"          % http4sVersion,
      "org.typelevel" %% "log4cats-slf4j"      % log4catsVersion,
      "org.typelevel" %% "cats-effect"         % catsEffectVersion,
      "io.getkyo"     %% "kyo-core"            % kyoVersion,
      "org.scalatest" %% "scalatest"           % scalaTestVersion % Test
    ),
    scalaVersion := scala3Version
  )

crossPaths := false // required to run jUnit test cases

console / initialCommands :=
  """
import cats._
import cats.data._
import cats.implicits._
import cats.effect._
import fs2.Stream

import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.global

implicit val cs = IO.contextShift(global)
"""
