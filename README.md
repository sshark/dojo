# Changelog

There are 4 Git tags that tract the progression of coding style from *Java++* using `Future` to FP to IO Monad abstraction. They are: -

* `with-future` implemented in *Java++* using `Future`
* `with-io-monad`  implemented in `cats.effect.IO`
* `with-io-monad-abstracted` uses `cats.effect.Aysnc` to abstract `cats.effect.IO` 
* `with-io-zio` uses `cats.effect.ConcurrentEffect` abstraction over `ZIO` and `cats.effect.IO`
