package org.teckhooi;

import org.junit.Test;

public class TrieTest {
    /*
        @Test
        public void addKeyValueToTrie() {
            Trie<Integer> trie = new Trie<>();
            trie.add("WANE", 29);
            Trie[] rootKeysMap = trie.getKeysMap();
            Trie wTrie = rootKeysMap['w' - 'a'];
            assertFalse(wTrie.isFinalSeq());
            Trie aTrie = wTrie.getKeysMap()[0];
            assertFalse(aTrie.isFinalSeq());
            Trie nTrie = aTrie.getKeysMap()['n' - 'a'];
            assertFalse(nTrie.isFinalSeq());
            Trie eTrie = nTrie.getKeysMap()['e' - 'a'];
            assertEquals(29, eTrie.getValue());
            assertTrue(eTrie.isFinalSeq());
        }

        @Test
        public void getNullFromTrie() {
            Trie<Integer> trie = new Trie<>();
            assertNull(trie.get("abc"));
        }

        @Test
        public void testKeyCaseInsensitivityToGetValue() {
            Trie<Integer> trie = new Trie<>();
            trie.add("WanE", 29);
            trie.add("wisp", 72);
            trie.add("wanT", 36);

            assertEquals(29, trie.get("WANE").intValue());
            assertEquals(36, trie.get("WANT").intValue());
            assertEquals(72, trie.get("WISP").intValue());
        }

        @Test
        public void getValueFromTrie() {
            Trie<Integer> trie = new Trie<>();
            trie.add("WANE", 29);
            trie.add("WISP", 72);
            trie.add("WANT", 36);

            assertEquals(29, trie.get("WANE").intValue());
            assertEquals(36, trie.get("WANT").intValue());
            assertEquals(72, trie.get("WISP").intValue());

            trie.add("WANTED", 99);
            assertEquals(36, trie.get("WANT").intValue());
            assertEquals(99, trie.get("WANTED").intValue());
            assertNull(trie.get("WANTE"));
        }
    */
    @Test
    protected void foo() {

    }
}
