package org.teckhooi.leetcode;

import org.junit.Assert;
import org.junit.Test;
import org.teckhooi.RottenOranges;

public class RottenOrangesTest {
    @Test
    public void whenThereAreBadOranges_thenTheyWillInfectFreshOranges() {
        Assert.assertEquals(4, RottenOranges.orangesRotting(new int[][]{{2, 1, 1}, {1, 1, 0}, {0, 1, 1}}));
    }

    @Test
    public void whenThereAreFreshOrangesCannotBeRotten_thenReturnNegativeOne() {
        Assert.assertEquals(-1, RottenOranges.orangesRotting(new int[][]{{2, 1, 1}, {0, 1, 1}, {1, 0, 1}}));
    }

    @Test
    public void whenThereAreBadOrangesInTheCenterOfTheGroup_thenFreshOrangesWillGetInfected() {
        Assert.assertEquals(2, RottenOranges.orangesRotting(new int[][]{{1, 1, 2, 0, 2, 0}}));
    }

    @Test
    public void whenThereAreBadOrangesAtDifferentPlaces_thenTheyWillInfectFreshOrangesSimultaneously() {
        Assert.assertEquals(2, RottenOranges.orangesRotting(new int[][]{{1, 2, 1, 1, 2, 1, 1}}));
    }

    @Test
    public void whenThereAreNoFreshOranges_thenReturnZero() {
        Assert.assertEquals(0, RottenOranges.orangesRotting(new int[][]{{0, 2}}));
    }
}
