package org.teckhooi.dojo2

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers._
import org.teckhooi.BowlingScore

import scala.util.{Failure, Success}

class BowlingSpec extends AnyFlatSpec {
  val strikes = "XXXXXXXXXXXX"
  val nines = "9-9-9-9-9-9-9-9-9-9-"
  val fiveSpares = "5/5/5/5/5/5/5/5/5/5/5"
  val mixSpares = "1/2/3/4/5/6/7/8/9/3/5"
  val alternateHalf = "-7-7-7-7-7-7-7-7-7-7"
  val mixFrames = "14456/5/X-17/6/X2/6"
  val invalidFramesWithSmallX = "14456/5/x-17/6/X2/6"
  val invalidFramesWithZero = "01234567890123456789"

  import Bowling._

  strikes should "have 12 rolls and score of 300" in {
    strikes should have length 12
    strikes.score shouldBe Success(300)

    // Java
//    new BowlingScore(strikes).score() shouldBe Try.success(300)
  }

  nines should "have 20 rolls and a score of 90" in {
    nines should have length 20
    nines.score shouldBe Success(90)

    // Java
//    new BowlingScore(nines).score() shouldBe Try.success(90)
  }

  alternateHalf should "have 20 rolls and score of 70" in {
    alternateHalf should have length 20
    alternateHalf.score shouldBe Success(70)

    // Java
//    new BowlingScore(alternateHalf).score() shouldBe Try.success(70)
  }


  fiveSpares should "have 21 rolls and score of 150" in {
    fiveSpares should have length 21
    fiveSpares.score shouldBe Success(150)

    // Java
//    new BowlingScore(fiveSpares).score() shouldBe Try.success(150)
  }

  mixSpares should "have 21 rolls and score of 152" in {
    mixSpares should have length 21
    mixSpares.score shouldBe Success(152)

    // Java
//    new BowlingScore(mixSpares).score() shouldBe Try.success(152)
  }

  mixFrames should "have 19 rolls and score of 133" in {
    mixFrames should have length 19
    mixFrames.score shouldBe Success(133)

    // Java
//    new BowlingScore(mixFrames).score() shouldBe Try.success(133)
  }

  invalidFramesWithSmallX should "fail because it contains 'x'" in {
    invalidFramesWithSmallX.score shouldBe a[Failure[_]]

    // Java
//    new BowlingScore(invalidFramesWithSmallX).score() shouldBe a[Try.Failure[_]]
  }

  invalidFramesWithZero should "fail because it contains zero" in {
    invalidFramesWithZero.score shouldBe a[Failure[_]]

    // Java
//    new BowlingScore(invalidFramesWithZero).score() shouldBe a[Try.Failure[_]]
  }
}
