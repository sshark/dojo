package org.teckhooi.fp

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers._
import org.teckhooi.dojo2.Permutation._

class PermutationSpec extends AnyFlatSpec {

  "abbcdd" should "have 180 permutations" in {
    permutation("abbcdd".toList).length should be(180)
  }

  """"abc" permutation""" should """yields "abc", "bca", "cba", "acb", "bac" and "cab" """ in {
    permutation("abc".toList).map(_.mkString) should contain theSameElementsAs
      List("bac", "bca", "cba", "cab", "acb", "abc")
  }

  it should "contain 6 elements" in {
    permutation("abc".toList).size should be(6)
  }

  """rotate "abcd"""" should """yields "abcd", "bcda", "cdab" & "dabc"""" in {
    rotate("abcd".toList).map(_.mkString) should contain theSameElementsAs List("abcd", "bcda", "cdab", "dabc")
  }
}
