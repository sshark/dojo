package org.teckhooi.dojo2.dynamic

import org.teckhooi.dojo2.dynamic.LargestSubarray.largestSubArray

/*
object LargestSubarrayTest extends TestSuite {
  override def tests: Tests =
    Tests {
      test("Given empty list") {
        List.empty[Int] ==> largestSubArray(List.empty[Int])
      }

      test("2 elements list with bigger head value") {
        List(-2) ==> largestSubArray(List(-2, -3))
      }

      test("2 elements list with bigger last value") {
        List(3) ==> largestSubArray(List(-2, 3))
      }

      test("A common list of integers") {
        List(4, -1, -2, 1, 5) ==> largestSubArray(List(-2, -3, 4, -1, -2, 1, 5, -3))
      }

      test("A common list of integers with values not contribute to the overall max sum") {
        List(4) ==> largestSubArray(List(-2, -3, 4, -1, -2, 1, -5, 3))
      }

      test("A single element list") {
        List(-2) ==> largestSubArray(List(-2))
      }
    }
}
 */
