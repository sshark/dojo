package org.teckhooi.dojo2.dynamic

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import org.teckhooi.dojo2.dynamic.CoinsChangeIntegral._

object SampleCoins {

  implicit object CoinIntegral extends Integral[Coin] {
    override def quot(x: Coin, y: Coin): Coin = Coin(x.v / y.v)

    override def rem(x: Coin, y: Coin): Coin = Coin(x.v % y.v)

    override def plus(x: Coin, y: Coin): Coin = Coin(x.v + y.v)

    override def minus(x: Coin, y: Coin): Coin = Coin(x.v - y.v)

    override def times(x: Coin, y: Coin): Coin = Coin(x.v * y.v)

    override def negate(x: Coin): Coin = x

    override def fromInt(x: Int): Coin = x match {
      case 1 => Cent
      case 5 => Five
      case 10 => Ten
      case 21 => Blackjack
      case 25 => Quarter
      case y => Coin(y)
    }

    override def toInt(x: Coin): Int = x.v

    override def toLong(x: Coin): Long = x.v.toLong

    override def toFloat(x: Coin): Float = x.v.toFloat

    override def toDouble(x: Coin): Double = x.v.toDouble

    override def compare(x: Coin, y: Coin): Int = x.v.compare(y.v)

    override def parseString(str: String): Option[Coin] = Some(Cent)
  }

  sealed case class Coin(v: Int)
  object Cent extends Coin(1)
  object Five extends Coin(5)
  object Ten extends Coin(10)
  object Blackjack extends Coin(21)
  object Quarter extends Coin(25)
}

class CoinChangeSpec extends AnyFlatSpec with Matchers {

  private val TypicalCoinsChangeSet =
    change(List(1, 5, 10, 21, 25)) _

  "A set of 1, 5, 10 and 25-cent coins" should "return four quarters" in {
    pick(TypicalCoinsChangeSet(100)) should be(List(25, 25, 25, 25))
  }

  it should "return three 21-cent" in {
    pick(TypicalCoinsChangeSet(63)) should be(List(21, 21, 21))
  }

  it should "return a 5-cent" in {
    pick(TypicalCoinsChangeSet(5)) should be(List(5))
  }

  it should "return two 1-cent and a 10-cent coins for 12 cents change" in {
    pick(TypicalCoinsChangeSet(12)) should contain allElementsOf List(1, 1, 10)
  }

  it should "return a 5-cent and a quarter coins for 30 cents change" in {
    pick(TypicalCoinsChangeSet(30)) should contain allElementsOf List(5, 25)
  }

  it should "return two 21-cent coins for 42 cents change" in {
    pick(TypicalCoinsChangeSet(42)) should be(List(21, 21))
  }

  it should "return three 21-cent coins for 63 cents change" in {
    pick(TypicalCoinsChangeSet(63)) should be(List(21, 21, 21))
  }

  it should "return a 1-cent, two 21-cent and five quaters for 168 cents change" in {
    pick(TypicalCoinsChangeSet(168)) should contain allElementsOf List(1, 21,
      21, 25, 25, 25, 25, 25)
  }

  it should "return a 10-cent, four 21-cent and three quarters for 169 cents change" in {
    pick(TypicalCoinsChangeSet(169)) should contain allElementsOf List(10, 21,
      21, 21, 21, 25, 25, 25)
  }

  it should "return two ten-cents and six quarters for 170 cents change" in {
    pick(TypicalCoinsChangeSet(170)) should contain allElementsOf List(10, 10,
      25, 25, 25, 25, 25, 25)
  }

  private val OddCoinsChangeSet = change(List(5, 10, 21)) _

  "A set of 5, 10 and 21-cent coins" should "return nothing for 3 cents change" in {
    pick(OddCoinsChangeSet(3)) shouldBe empty
  }

  it should "return nothing for 16 cents change" in {
    pick(OddCoinsChangeSet(16)) shouldBe empty
  }

  private val NoOneCentChangeSet = change(List(2, 5, 10, 21)) _

  "A set of 2, 5, 10 and 21-cent coins" should "return 2 and 5-cent coins for 7 cents change" in {
    pick(NoOneCentChangeSet(7)) should contain allElementsOf List(2, 5)
  }

  it should "return a 10-cent coin for 10 cents change" in {
    pick(NoOneCentChangeSet(10)) should contain allElementsOf List(10)
  }

  private val BigIntsCoinSet: List[BigInt] =
    List(BigInt(1), BigInt(5), BigInt(10), BigInt(21), BigInt(25))

  "A set of 1, 5 10, 21 and 25 BigInts" should "return three 21-BigInt for 63-BigInt change" in {
    val integral = implicitly[Integral[BigInt]]

    //    change(BigIntsCoinSet)(BigInt(63)) should be(List(BigInt(21), BigInt(21), BigInt(21)))
  }

  private val LongsCoinSet = List(1L, 5L, 10L, 21L, 21L, 25L)

  "A set of 1, 5, 10, 21 and 25 Longs" should "return three 21-Long for 63-Long change" in {
    pick(change(LongsCoinSet)(63L)) should contain allElementsOf List(21L, 21L, 21L)
  }

  private val EvenIntsCoinSet = change(List(2, 4, 6, 8)) _

  "A set of even Ints" should "return 4 and 8 Ints for the 12 Int change" in {
    pick(EvenIntsCoinSet(12)) should contain allElementsOf List(4, 8)
  }

  it should "return nothing for the 11 Int change" in {
    pick(EvenIntsCoinSet(11)) shouldBe empty
  }

  private val RandomLongsCoinSet =
    change(List(1L, 21L, 25L, 5L, 10L)) _

  "A set of random Longs" should "return 1, two 21 and five 25 Longs for 168 Long change" in {
    pick(RandomLongsCoinSet(168L)) should contain allElementsOf List(1, 21, 21, 25, 25, 25, 25, 25)
  }

    import SampleCoins._

  private val SampleCoinsCoinSet = change(List(Cent, Five, Ten, Quarter, Blackjack)) _

    "A set of sample coins" should "return a Cent, two Blackjacls and five Quarters for Coin of 168 change" in {
      pick(SampleCoinsCoinSet(Coin(168))) should contain allElementsOf List(Cent, Blackjack, Blackjack, Quarter, Quarter, Quarter, Quarter, Quarter)
    }
}
