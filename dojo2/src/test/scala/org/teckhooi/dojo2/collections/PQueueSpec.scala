package org.teckhooi.fp.collections

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers._
import org.teckhooi.dojo2.collections.PQueue
import org.teckhooi.dojo2.collections.PQueue._

class PQueueSpec extends AnyFlatSpec {

  "A Priority Queue" should "pop values in ascending order" in {
    val pq = enqueue(2, enqueue(99, enqueue(1, PQueue.empty[Int])))
    top(pq) should be(1)

    val dequeuedOncePQ = dequeue(pq)
    top(dequeuedOncePQ) should be(2)

    val dequeuedTwicePQ = dequeue(dequeuedOncePQ)
    top(dequeuedTwicePQ) should be(99)
  }

  it should "throw NoSuchElementException if an empty queue is dequeued" in {
    a[NoSuchElementException] should be thrownBy {
      dequeue(PQueue.empty[Int])
    }
  }
}
