package org.teckhooi.fp.collections

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers._
import org.teckhooi.dojo2.collections.Stack
import org.teckhooi.dojo2.collections.Stack._

class StackSpec extends AnyFlatSpec {

  "A Stack" should "pop values in LIFO order" in {
    val stack = push(3, push(2, push(1, Stack.empty)))
    top(stack) should be(3)

    val poppedOnceStack = pop(stack)
    top(poppedOnceStack) should be(2)

    val poppedTwiceStack = pop(poppedOnceStack)
    top(poppedTwiceStack) should be(1)
  }

  it should "throw NoSuchElementException if an empty stack is popped" in {
    a[NoSuchElementException] should be thrownBy {
      pop(Stack.empty)
    }
  }
}
