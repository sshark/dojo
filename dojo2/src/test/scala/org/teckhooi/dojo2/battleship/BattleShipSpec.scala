package org.teckhooi.fp.battleship

import org.scalatest.flatspec._
import org.scalatest.matchers._
import org.teckhooi.dojo2.battleship.BattleShip

/**
  * Created by sshark on 2016/10/08.
  */

class BattleShipSpec extends AnyFlatSpec with should.Matchers {

  "Ships 1B 2C, 2D 4D" should "have sink/hit ratio of 1/1 with targets 2B 2D 3D 4D 4A" in {
    BattleShip.solution(4, "1B 2C, 2D 4D", "2B 2D 3D 4D 4A") should be("1,1")
  }

  "Ships 2B 2D, 3C 4D" should "have sink/hit ratio of 2/0 with targets 2B 2D 3D 4D 3C 4C 4A 2C" in {
    BattleShip.solution(4, "2B 2D, 3C 4D", "2B 2D 3D 4D 3C 4C 4A 2C") should be("2,0")
  }

  it should "have sink/hit ratio of 2/0 with targets 2B 2D 3D 4D 3C 4C 2C" in {
    BattleShip.solution(4, "2B 2D, 3C 4D", "2B 2D 3D 4D 3C 4C 2C") should be("2,0")
  }

  it should "have sink/hit ratio of 1/1 with targets 2B 2D 3D 4D 4C 4A 2C" in {
    BattleShip.solution(4, "2B 2D, 3C 4D", "2B 2D 3D 4D 4C 4A 2C") should be("1,1")
  }

  "Ship 1A 26Z" should "have sink/hit ratio of 0/1 with targets 26C" in {
    BattleShip.solution(2, "1A 26Z", "26C") should be("0,1")
  }
}
