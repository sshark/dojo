package org.teckhooi.fp

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.must.Matchers.contain
import org.scalatest.matchers.should.Matchers.convertToAnyShouldWrapper
import org.teckhooi.dojo2.Steps

class StepsSpec extends AnyFlatSpec {

  "Given a stair height of 4 and the constraint set of 1, 2 and 3 steps" should "return 7 steps combinations" in {

    val expected: List[List[Int]] = List(List(1, 1, 1, 1), List(2, 1, 1), List(1, 2, 1), List(3, 1), List(1, 1, 2), List(2, 2), List(1, 3))

    Steps.stepsSet(4, List(1, 2, 3)) should contain theSameElementsAs expected
  }
}
