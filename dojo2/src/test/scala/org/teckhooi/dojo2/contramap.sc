import scala.language.higherKinds

trait ContraMap[F[_]] {
  def contramap[A, B](fa: F[A])(f: B => A): F[B]
}

trait Ordering[A] {
  def compare(a: A, b: A): Int

  def on[B](f: B => A): Ordering[B] = (a: B, b: B) => compare(f(a), f(b))
}

object Ordering {
  def apply[T](implicit ev: Ordering[T]) = ev
}

object IntOrdering {
  implicit def intOrdering = new Ordering[Int] {
    override def compare(a: Int, b: Int): Int = a compare b
  }
}

def max[A: Ordering](a: A, b: A): A =
  implicitly[Ordering[A]].compare(a, b) match {
    case x if x > 0 => a
    case x if x < 0 => b
    case _ => a
  }

import IntOrdering._

case class Person(name: String, age: Int)

implicit def personOrdering = Ordering[Int].on((p: Person) => p.age)
max(Person("andrew", 50), Person("lim", 40))


trait DoorState
sealed trait Close extends DoorState
sealed trait Open extends DoorState

class Door[S <: DoorState] {
  def open[T >: S <: Close]: Door[Open] = new Door

  def close[T >: S <: Open]: Door[Close] = new Door
}

class Door[S <: DoorState] {
  def open[S <: Close]: Door[Open] = new Door

  def close[S <: Open]: Door[Close] = new Door
}

/* Alternatively,

class Door[S <: DoorState] {
  def open(implicit ev: Close =:= S): Door[Open] = new Door
  def close(implicit ev: Open =:= S): Door[Close] = new Door
}
*/

val door = new Door[Close]
door.open.open

trait Functor[F[_]] {
  def map[A, B](f: A => B): F[A] => F[B]
}

class Foo[E] {
  type AB[C] = Either[E, C]

  def apply(ev: Functor[AB]): Foo[E] = new Foo[E]()
}

def foo[E] = new Foo[E]

def eitherF[C]: Functor[Lambda[x => Either[C, x]]] = new Functor[Lambda[x => Either[C, x]]] {
  override def map[A, B](f: A => B): Either[C, A] => Either[C, B] = {
    case l: Left[C, A] => Left[C, B](l.value)
    case r: Right[C, A] => Right(f(r.value))
  }
}

class Bar[A[_, _], B] {
  type AB[C] = A[B, C]

  def apply[C](functor: Functor[AB]) = new Bar[A, B]
}

