package org.teckhooi.fp

object TrumpCards extends App {
  val inputs = List("AD 2H | H", "KD KH | C", "JH 10S | C",
    "3H 8D | S", "3D 9C | S")

  val results = inputs.map(Cards.hand).map(Cards.winner)

  results.foreach(println)
}

case class Card(numStr: String, suite: String, value: Int) {
  override def toString = numStr + suite
}

object Cards {
  val re = "(10|[AKQJ(2-9)])([HCDS])".r

  def winner(round: (Card, Card, String)) = duel(round._1, round._2, round._3) match {
    case (Some(x), None) => x.toString
    case (None, Some(x)) => x.toString
    case (Some(x), Some(y)) => s"${x.toString} ${y.toString}"
    case (_, _) => ""
  }

  def hand(s: String) = s.split(" ") match {
    case Array(a, b, _, c) => (Cards.apply(a), Cards.apply(b), c)
  }

  val numToValueMap = Map(
    "2" -> 2,
    "3" -> 3,
    "4" -> 4,
    "5" -> 5,
    "6" -> 6,
    "7" -> 7,
    "8" -> 8,
    "9" -> 9,
    "10" -> 10,
    "J" -> 11,
    "Q" -> 12,
    "K" -> 13,
    "A" -> 14
  )

  def apply(s: String) = {
    val re(numStr, suite) = s
    Card(numStr, suite, numToValueMap(numStr))
  }

  def duel(fst: Card, snd: Card, trump: String): (Option[Card], Option[Card]) =
    (fst, snd) match {
      case (x, _) if x.suite == trump => (Some(x), None)
      case (_, y) if y.suite == trump => (None, Some(y))
      case (x, y) if x.value > y.value => (Some(x), None)
      case (x, y) if x.value < y.value => (None, Some(y))
      case (x, y) => (Some(x), Some(y))
    }
}
