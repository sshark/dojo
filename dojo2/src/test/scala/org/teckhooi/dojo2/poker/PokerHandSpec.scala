package org.teckhooi.dojo2.poker

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import org.teckhooi.dojo2.poker.PokerHand._
import org.teckhooi.dojo2.poker.Ranks.RankOrdering

/**
  *
  * @author Lim, Teck Hooi
  *
  */

class PokerHandSpec extends AnyFlatSpec with Matchers {
  "AS QS KS 10S JS" should "match Straight Flush with high card Ace" in {
    PokerHand("AS QS KS 10S JS") should be(StraightFlush(List(Ace)))
  }

  "10S 10C 10D JS 10H" should "match Four of A Kind with high card Ten" in {
    PokerHand("10S 10C 10D JS 10H") should be(FourOfAKind(List(Ten)))
  }

  "JD 3D JS JH 3S" should "match Full House with high card Three" in {
    PokerHand("JD 3D JS JH 3S") should be(FullHouse(List(Jack)))
  }

  "10D AD 2D 8D 7D" should "match Flush with high cards 10D AD 2D 8D 7D" in {
    PokerHand("10D AD 2D 8D 7D") should be(Flush(List(Ten, Ace, Two, Eight, Seven).sorted))
  }

  "2D 4S 5C 6S 3D" should "match Straight with high card Six" in {
    PokerHand("2D 4S 5C 6S 3D") should be(Straight(List(Six)))
  }

  "2D 4S 5C AS 3D" should "match Straight with high card Five" in {
    PokerHand("2D 4S 5C AS 3D") should be(Straight(List(Five)))
  }

  "AD AC 2C 5D 2D" should "match Two Pairs with high cards Ace and Two" in {
    PokerHand("AD AC 2C 5D 2D") should be(TwoPairs(List(Two, Ace)))
  }

  "2D 3C 9S 8S 9C" should "match One Pair with high card Nine" in {
    PokerHand("2D 3C 9S 8S 9C") should be(OnePair(List(Nine)))
  }

  "3D 7C KD QS 10C" should "match High Card with List(Three, Seven, Ten, Queen, King)" in {
    PokerHand("3D 7C KD QS 10C") should be(HighCard(List(King, Queen, Ten, Seven, Three)))
  }
}
