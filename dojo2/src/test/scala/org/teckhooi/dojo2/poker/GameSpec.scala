package org.teckhooi.dojo2.poker

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import org.teckhooi.dojo2.poker.Game._
import org.teckhooi.dojo2.poker.PokerHand._
import org.teckhooi.dojo2.poker.Ranks.RankOrdering

/**
  *
  * @author Lim, Teck Hooi
  *
  */
class GameSpec extends AnyFlatSpec with Matchers {
  "AS QS KS10S JS" should "throw match error" in {
    intercept[MatchError](PokerHand("AS QS KS10S JS"))
  }

  "AS 5D 8C JH" should "complain illegal argument" in {
    val thrown = the[IllegalArgumentException] thrownBy PokerHand("AS 5D 8C JH")
    thrown.getMessage should be("Each player must be dealt exactly 5 cards with no duplicates.")
  }

  "AS 5D 8C JH 10C 2C" should "complain illegal argument" in {
    val thrown = the[IllegalArgumentException] thrownBy PokerHand("AS 5D 8C JH 10C 2C")
    thrown.getMessage should be("Each player must be dealt exactly 5 cards with no duplicates.")
  }

  "5D JH 10C 2C 10C" should "complain illegal argument" in {
    val thrown = the[IllegalArgumentException] thrownBy PokerHand("5D JH 10C 2C 10C")
    thrown.getMessage should be("Each player must be dealt exactly 5 cards with no duplicates.")
  }

  "White: 4D 8H 7H 10S 9C" should "complain the game allows exactly 2 players" in {
    val thrown = the[IllegalArgumentException] thrownBy Game("White with 4D 8H 7H 10S 9C")
    thrown.getMessage should be("This game allows exactly 2 players.")
  }

  "White: 4D 8H 7H 10S 9C  Black: 2H  3D 5S 9C KD  Green: 2C 3H 4S 8C AH" should "complain the game allows exactly 2 players" in {
    val thrown = the[IllegalArgumentException] thrownBy Game(
      "White: 4D 8H 7H 10S 9C  Black: 2H  3D 5S 9C KD  Green: 2C 3H 4S 8C AH")
    thrown.getMessage should be("This game allows exactly 2 players.")
  }

  "AS QS    KS 10S   JS" should "tie with Straight Flush with high card Ace" in {
    duel(Player("White", PokerHand("AS QS    KS 10S   JS")), Player("Black", StraightFlush(List(Ace)))) should be(Tie)
  }

  it should "win Four of A Kind" in {
    duel(Player("Black", PokerHand("AS QS KS 10S JS")), Player("White", FourOfAKind(List(Five)))) should be(
      Win(Player("Black", StraightFlush(List(Ace))), "with StraightFlush"))
  }

  "AS 3C 3D 3S 3H" should "lose to Straight Flush" in {
    duel(Player("Black", PokerHand("AS 3C 3D 3S 3H")), Player("White", StraightFlush(List(Ace)))) should be(
      Win(Player("White", StraightFlush(List(Ace))), "with StraightFlush"))
  }

  it should "tie with Four of A Kind of the same high card (hypothetically)" in {
    duel(Player("Black", PokerHand("AS 3C 3D 3S 3H")), Player("White", FourOfAKind(List(Three)))) should be(Tie)
  }

  it should "win Three of A Kind with high card Six" in {
    duel(Game(Player("Black", PokerHand("AS 3C 3D 3S 3H")), Player("White", PokerHand("6D 3C 6C 6S AS")))) should be(
      Win(Player("Black", FourOfAKind(List(Three))), "with FourOfAKind"))
  }

  "White: 4D 8H 7H 10S 9C" should "win Black: Four, Eight, Seven, Ten and Nine with high card Ten" in {
    duel(Player("White", PokerHand("4D 8H 7H 10S 9C")),
      Player("Black", HighCard(List(Four, Eight, Seven, Two, Nine).sorted.reverse))) should be(
      Win(Player("White", HighCard(List(Four, Eight, Seven, Ten, Nine).sorted.reverse)), "with high card: 10"))
  }

  it should "lose to Black: Eight, Five, Seven, Ten and Nine with high card Five" in {
    duel(Player("White", PokerHand("4D 8H 7H 10S 9C")),
      Player("Black", HighCard(List(Eight, Five, Seven, Ten, Nine).sorted.reverse))) should be(
      Win(Player("Black", HighCard(List(Eight, Five, Seven, Ten, Nine).sorted.reverse)), "with high card: 5"))
  }

  it should "win Black: Ten, Three, Seven, Eight and Nine with high card 4" in {
    duel(Player("White", PokerHand("4D 8H 7H 10S 9C")),
      Player("Black", HighCard(List(Ten, Three, Seven, Eight, Nine).sorted.reverse))) should be(
      Win(Player("White", HighCard(List(Four, Eight, Seven, Ten, Nine).sorted.reverse)), "with high card: 4"))
  }

  it should "lose to Black: Queen, Four, Jack, Six and Two with high card Queen" in {
    duel(Player("White", PokerHand("4D 8H 7H 10S 9C")), Player("Black", HighCard(List(Queen, Four, Jack, Six, Two)))) should be(
      Win(Player("Black", HighCard(List(Queen, Four, Jack, Six, Two))), "with high card: Queen"))
  }

  it should "tie with Black: Four, Ten, Eight, Nine and Seven" in {
    duel(Player("White", PokerHand("4D 8H 7H 10S 9C")),
      Player("Black", HighCard(List(Four, Ten, Eight, Nine, Seven).sorted.reverse))) should be(Tie)
  }

  "Black: 2H  3D 5S    9C KD     White: 2C   3H 4S 8C AH   " should
    "match players \"Black\" with HighCard(List((K, 9, 5, 3, 2))) and \"White\" with HighCard(List(A, 8, 4, 3, 2)))" in {

    Game("Black: 2H  3D 5S    9C KD     White: 2C   3H 4S 8C AH   ") should be(
      Game(Player("Black", HighCard(List(Two, Five, Three, Nine, King).sorted.reverse)),
        Player("White", HighCard(List(Ace, Eight, Four, Two, Three).sorted.reverse))))
  }

  it should "lose to White: 2C 3H 4S 8C AH with high card: Ace" in {
    duel(Game("Black: 2H 3D 5S 9C KD  White: 2C 3H 4S 8C AH")).toString should be("White wins. - with high card: Ace")
  }

  "Black: 2H 4S 4C 2D 4H" should "win White: 2S 8S AS QS 3S with Full House" in {
    duel(Game("Black: 2H 4S 4C 2D 4H  White: 2S 8S AS QS 3S")).toString should be("Black wins. - with FullHouse")
  }

  "Black: 2D 6C 4S 2C 6D " should "win White: 3C 6D 4C 7H 8S with Two Pairs" in {
    duel(Game("Black: 2D 6C 4S 2C 6D  White: 3C 6D 4C 7H 8S")).toString should be("Black wins. - with TwoPairs")
  }

  "Black: 2H 3D 5S 9C KD" should "win White: 2C 3H 4S 8C KH - with high card: 9" in {
    duel(Game("Black: 2H 3D 5S 9C KD  White: 2C 3H 4S 8C KH")).toString should be("Black wins. - with high card: 9")
  }

  "Black: 2H 3D 5S 9C KD  White: 3H 2D 5C 9S KH" should "Tie" in {
    duel(Game("Black: 2H 3D 5S 9C KD  White: 2D 3H 5C 9S KH")).toString should be("Tie")
  }

  "Black: 2D 6C 4S 3C 5D" should "win White: AC 3D 2C 4H 5S with high card 6 " in {
    duel(Game("Black: 2D 6C 4S 3C 5D  White: AC 3D 2C 4H 5S")).toString should be("Black wins. - with high card: 6")
  }

  it should "lose to White: KS JD 10D AC QS with high card Ace" in {
    duel(Game("Black: 2D 6C 4S 3C 5D  White: KS JD 10D AC QS")).toString should be("White wins. - with high card: Ace")
  }
}
