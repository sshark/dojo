package org.teckhooi.dojo2.poker

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import org.teckhooi.dojo2.poker.Cards.CardOrdering
import org.teckhooi.dojo2.poker.Ranks.RichRank

class CardSpec extends AnyFlatSpec with Matchers {
  val ord = implicitly[Ordering[Card]]

  import ord.mkOrderingOps

  "Cards constructed with 'of' or 'Card'" should "be the same" in {
    val suits = List(Diamond, Heart, Spade, Club)
    val ranks = (2 to 10).map(x => Card.toRank(x.toString)).toList
    val higherRanks = "JQKA".split("").map(Card.toRank)
    val cards = for {
      suit <- suits
      rank <- ranks ++ higherRanks
    } yield (rank of suit, Card(rank, suit))
    cards.forall(card => card._1 == card._2) should be(true)
  }

  "QD" should "not match Queen of Heart" in {
    Card("QD") should not be (Queen of Heart)
  }

  "AS" should "match Ace of Spade" in {
    Card("AS") should be(Ace of Spade)
  }

  "QQ" should "throw match error" in {
    intercept[MatchError](Card("QQ"))
  }

  "Card(Queen, Club)" should "be lower rank than Ace of Spade" in {
    Card(Queen, Club) should be < (Ace of Spade)
  }

  it should "be higher rank than Jack of Spade" in {
    Card(Queen, Club) should be > (Jack of Spade)
  }

  it must "be same rank with Queen of Diamond" in {
    Card(Queen, Club) equiv (Queen of Diamond) should be(true)
  }

  "2C 3H 4S 8C AH" should "match a List(Two of Club, Three of Heart, Four of Spade, Eight of Club and Ace of Heart)" in {
    Cards("2C 3H 4S 8C AH") should
      be(List(Two of Club, Three of Heart, Four of Spade, Eight of Club, Ace of Heart).sorted.reverse)
  }
}
