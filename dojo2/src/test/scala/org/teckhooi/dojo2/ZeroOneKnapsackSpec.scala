package org.teckhooi.fp

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers._
import org.teckhooi.dojo2.ZeroOneKnapsack._

class ZeroOneKnapsackSpec extends AnyFlatSpec {

  case class GoldNugget(value: Int, weight: Int)

  implicit object GoldNuggetValueWeightProps extends ValueWeightProps[GoldNugget] {
    override def value(a: GoldNugget): Int = a.value

    override def weight(a: GoldNugget): Int = a.weight
  }

  implicit object GoldNuggetOrdering extends Ordering[GoldNugget] {
    override def compare(x: GoldNugget, y: GoldNugget): Int = x.value.compare(y.value)
  }

  implicit object GoldNuggetsOrdering extends Ordering[List[GoldNugget]] {
    override def compare(x: List[GoldNugget], y: List[GoldNugget]): Int =
      x.map(_.value).sum.compare(y.map(_.value).sum)
  }

  "The best fit for GoldNuggets (10, 5), (40, 4), (30, 6) and (50, 3) for a knapsack that takes 10 weight units" should "be 90" in {
    val goldNuggets = List(GoldNugget(10, 5), GoldNugget(40, 4), GoldNugget(30, 6), GoldNugget(50, 3))
    val chart = bestFitChart(10, goldNuggets)

    chart(goldNuggets.size).last.map(_.value).sum should be(90)
  }


  "The best fit for GoldNuggets (100, 3), (20, 2), (60, 4) and (40, 1) for a knapsack that takes 5 weight units" should "be 140" in {
    val goldNuggets = List(GoldNugget(100, 3), GoldNugget(20, 2), GoldNugget(60, 4), GoldNugget(40, 1))
    val chart = bestFitChart(5, goldNuggets)

    chart(goldNuggets.size).last.map(_.value).sum should be(140)
  }

}
