sealed trait HList {}

final case class HCons[H, T <: HList](head: H, tail: T) extends HList {
  def ::[U](v: U) = HCons(v, this)

  override def toString = head + " :: " + tail
}
class HNil extends HList {
  def ::[T](v: T) = HCons(v, this)

  override def toString = "HNil"
}

object HList {
  type ::[H, T <: HList] = HCons[H, T]
  val :: = HCons
  val HNil = new HNil
}

import HList._

val x: (String :: Int :: Boolean :: HNil) = "Hi" :: 5 :: false :: HNil

val one :: two :: three :: HNil = x
