val l = List('a, 'a, 'a, 'a, 'b, 'n, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e)

def rl(xs: List[Symbol], acc: List[(Symbol, Int)], counter: Option[(Symbol, Int)] = None): List[(Symbol, Int)] =
  if (xs.isEmpty) counter.fold(acc)(x => acc :+ x)
  else counter match {
    case None => rl(xs.tail, acc, Some((xs.head, 1)))
    case Some((sym, count)) =>
      if (xs.head == sym) rl(xs.tail, acc, Some((sym, count + 1)))
      else rl(xs, acc :+ (sym, count), None)
  }

rl(l, List.empty[(Symbol, Int)], None)
rl(Nil, List.empty[(Symbol, Int)], None)
rl(List('a), List.empty[(Symbol, Int)], None)


