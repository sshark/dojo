package org.teckhooi.fp

import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers._
import org.teckhooi.dojo2.ClusterFinder.findClusters

class ClusterFinderTest extends AnyFunSuite {
  test("3 clusters orientation") {
    val clusters: List[List[Boolean]] = List(
      "     # ",
      "     ##",
      "      #",
      "#   #  ").map(_.toList.map(c => if (c == ' ') {
      false
    } else {
      true
    }))

    findClusters(clusters).size should be(3)

  }

  test("No cluster orientation") {
    val singleBlob: List[List[Boolean]] = List(
      "     ",
      "     ",
      "     ",
      "     ").map(_.toList.map(c => if (c == ' ') false else true))

    findClusters(singleBlob) shouldBe empty
  }

  test("Single blob orientation") {
    val singleBlob: List[List[Boolean]] = List(
      "#####",
      "#####",
      "#####",
      "#####").map(_.toList.map(c => if (c == ' ') false else true))

    findClusters(singleBlob).size should be(1)
  }

  test("Non-uniform orientation") {
    val broken: List[List[Boolean]] = List(
      "     # ",
      "     #",
      "      #",
      "#   #  ").map(_.toList.map(c => if (c == ' ') false else true))

    findClusters(broken).size should be(4)
  }

  test("Special case orientation") {
    val broken: List[List[Boolean]] = List(
      " # # ",
      "# #  ",
      "   # ",
      "#### ",
      "#  # ").map(_.toList.map(c => if (c == ' ') false else true))

    println(findClusters(broken))
    findClusters(broken).size should be(5)
  }
}
