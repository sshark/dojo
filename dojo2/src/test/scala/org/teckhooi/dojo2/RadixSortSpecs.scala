package org.teckhooi.fp

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers._
import org.teckhooi.dojo2.RadixSort

import scala.util.Random

class RadixSortSpecs extends AnyFlatSpec {
  "When 1, 6, 4, 0, 2 are sorted in ascending order" should "be 0, 1, 2, 4, 6" in {
    RadixSort.sort(Seq(1, 6, 4, 0, 2)) shouldBe Seq(0, 1, 2, 4, 6)
  }

  "When 100, 2000, 10, 1 are sorted in ascending order" should "be 1, 10, 100, 2000" in {
    RadixSort.sort(Seq(100, 2000, 10, 1)) shouldBe Seq(1, 10, 100, 2000)
  }

  "When 2, 1, 1, 2, 3 are sorted in ascending order" should "be 1, 1, 2, 2, 3" in {
    RadixSort.sort(Seq(2, 1, 1, 2, 3)) shouldBe Seq(1, 1, 2, 2, 3)
  }

  "When 2, -3, 0, 1, 1, -2, 3 are sorted in ascending order" should "be -3, -2, 0, 1, 1, 2, 3" in {
    RadixSort.sort(Seq(2, -3, 0, 1, 1, -2, 3)) shouldBe Seq(-3, -2, 0, 1, 1, 2, 3)
  }

  "When 1000 random integer numbers are radix sorted" should "be in the same order as using a different sort" in {
    val numbers = Seq.fill(1000)(Random.nextInt(1000000000))
    RadixSort.sort(numbers) shouldBe numbers.sorted
  }
}
