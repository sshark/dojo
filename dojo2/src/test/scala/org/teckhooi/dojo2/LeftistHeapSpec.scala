package org.teckhooi.fp

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers._
import org.teckhooi.dojo2.LeftistHeap

class LeftistHeapSpec extends AnyFlatSpec {

  "Add [4, 5, 7, 6, 2, 1, 3] into an empty heap" should "yield [1, 4, 2, 6, 5, 7, 3, 9]" in {
    val xs = List(4, 5, 7, 6, 2, 1, 3, 9)
    val heap = new LeftistHeap(xs)
    heap.list should contain theSameElementsInOrderAs List(1, 4, 2, 6, 5, 7, 3, 9)
  }

  "Add 100 to a [9, 7, 5, 6, 2, 1, 3, 4] heap" should "yield [1, 4, 2, 6, 5, 7, 3, 9, 100]" in {
    val xs = List(4, 5, 7, 6, 2, 1, 3, 9)
    val heap = new LeftistHeap(xs)
    heap.add(100)
    heap.list should contain theSameElementsInOrderAs List(1, 4, 2, 6, 5, 7, 3, 9, 100)
  }

  "Remove top from [100, 9, 5, 7, 2, 1, 3, 4, 6]" should "yield 100" in {
    val xs = List(4, 5, 7, 6, 2, 1, 3, 9)
    val heap = new LeftistHeap(xs)
    heap.add(100)
    heap.deleteMin() should equal(Some(1))
  }

  "Sorting [4, 5, 7, 6, 2, 1, 3] using heap" should "yield []" in {

    val xs = List(4, 5, 7, 6, 2, 1, 3, 9)
    val heap = new LeftistHeap(xs)

    heap.ordered should contain theSameElementsInOrderAs xs.sorted
  }
}
