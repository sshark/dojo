package org.teckhooi.fp

import org.scalatest.flatspec.AnyFlatSpec
//import scalaz.Scalaz._

/**
 *
 * @author thlim
 *
 */

class ElevatorStopsSpec extends AnyFlatSpec {

  val maxWeight = 200
  val maxPerson = 2

  /*
    "An elevator with 3 persons weighting more than 200 going to stories 2, 3 and 5" should "return failure" in {
      ElevatorStops.stops(Seq(260, 280, 240), Seq(2, 3, 5), 5, maxPerson, maxWeight) should be("One or more allowable weights found".failureNel)
    }

    "An elevator with 3 persons weighting 60, 80 and 40 going to stories 2, 3 and 5" should "made 5 trips" in {
      ElevatorStops.stops(Seq(60, 80, 40), Seq(2, 3, 5), 5, maxPerson, maxWeight) should be(5.successNel)
    }

    "An elevator with 3 persons weighting 60, 80 and 40 going to stories 2, 3 and 2" should "made 5 trips" in {
      ElevatorStops.stops(Seq(60, 80, 40), Seq(2, 3, 2), 5, maxPerson, maxWeight) should be(5.successNel)
    }

    "An elevator with 3 persons weighting 60, 80 and 40 going to stories 2, 2 and 3" should "made 4 trips" in {
      ElevatorStops.stops(Seq(60, 80, 40), Seq(2, 2, 3), 5, maxPerson, maxWeight) should be(4.successNel)
    }

    "Different number of weights and floors given" should "return failure" in {
      ElevatorStops.stops(Seq(60, 80, 40), Seq(2, 3), 5, maxPerson, maxWeight) should be("Weights and floors not match".failureNel)
    }

    "No person taking the elevator" should "made zero trip" in {
      ElevatorStops.stops(Seq.empty[Int], Seq.empty[Int], 5, maxPerson, maxWeight) should be(0.successNel)
    }
  */
}
