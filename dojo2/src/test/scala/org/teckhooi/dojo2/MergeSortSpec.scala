package org.teckhooi.fp

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers._
import org.teckhooi.dojo2.MergeSort

class MergeSortSpec extends AnyFlatSpec {
  "Sorted Seq(5, 3, 1)" should "be Seq(1, 3, 5)" in {
    MergeSort.sort(Seq(5, 3, 1)) shouldBe Seq(1, 3, 5)
  }

  "Sorted Seq(5, 3, 1, 10, 20, 2)" should "be Seq(1, 2, 3, 5, 10, 20" in {
    MergeSort.sort(Seq(5, 3, 1, 10, 20, 2)) shouldBe Seq(1, 2, 3, 5, 10, 20)
  }
}
