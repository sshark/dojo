package org.teckhooi;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class PowerSet {

    // recursive solution
    public static <A> List<List<A>> powerSet(List<A> xs, List<List<A>> acc) {
        if (xs.isEmpty()) {
            return acc;
        }

        List<List<A>> previousElems = new ArrayList<>();
        for (List<A> single : acc) {
            previousElems.add(new ArrayList<>(single));
        }

        A head = xs.get(0);
        for (List<A> ys : previousElems) {
            ys.add(head);
        }

        acc.addAll(previousElems);

        return powerSet(xs.subList(1, xs.size()), acc);
    }

    //  for-loop solution
    /*
    public static <A> List<List<A>> powerSet(List<A> xs) {
        if (xs.isEmpty()) {
            List<List<A>> empty = Collections.EMPTY_LIST;
            empty.add(new ArrayList<>());
            return empty;
        }

        List<List<A>> acc = new ArrayList<>();
        acc.add(new ArrayList<>());

        for (A a : xs) {
            List<List<A>> previousElems = new ArrayList<>();
            for (List<A> single : acc) {
                previousElems.add(new ArrayList<>(single));
            }

            for (List<A> ys : previousElems) {
                ys.add(a);
            }
            acc.addAll(previousElems);
        }
        return acc;
    }
    */

    private static int sum(List<Integer> ints) {
        int x = 0;
        for (int i : ints) {
            x += i;
        }
        return x;
    }

    public static void main(String[] args) {
        List<String> strings = Arrays.asList("4", "6", "3");
        List<Integer> ints = strings.stream().map(Integer::parseInt).collect(Collectors.toList());

        int max = 8;
        List<List<Integer>> emptyInts = new ArrayList<>();
        emptyInts.add(new ArrayList<>());

        List<List<Integer>> schedule = powerSet(ints, emptyInts)
            .stream().filter(xs -> xs.stream().mapToInt(i -> i.intValue()).sum() <= max)
            .collect(Collectors.toList());

        System.out.println("Slots fit within " + max + " hours");
        schedule.forEach(System.out::println);

        System.out.println("Best slot within " + max + " hours");
        schedule.stream().max(Comparator.comparing(PowerSet::sum)).ifPresent(System.out::println);
    }
}
