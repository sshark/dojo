package org.teckhooi.existential;

/**
 * Created by sshark on 01-Sep-16.
 */

public class DriedAppleJug implements Jug<AppleJug> {
    @Override
    public String topUp(AppleJug fruitJug) {
        return fruitJug.topUp(fruitJug);
    }
}
