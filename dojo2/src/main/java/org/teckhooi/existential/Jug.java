package org.teckhooi.existential;

/**
 * Created by sshark on 01-Sep-16.
 */

public interface Jug<T extends Jug<T>> {
    String topUp(T jug);
}
