package org.teckhooi.existential;

/**
 * Created by sshark on 01-Sep-16.
 */
public class AppleJug implements Jug<AppleJug> {
    @Override
    public String topUp(AppleJug fruit) {
        return "top up with apple juice";
    }
}
