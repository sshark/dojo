package org.teckhooi.existential;

/**
 * Created by sshark on 01-Sep-16.
 */

public class JuiceStore {
    public static void main(String[] args) {
        DriedAppleJug driedAppleJug = new DriedAppleJug();
        AppleJug quarterAppleJug = new AppleJug();

        // should not be allowed to proceed
        driedAppleJug.topUp(quarterAppleJug);  // dried apple "juice" should not be filled with apple juice
    }
}
