package org.teckhooi;

import java.util.PriorityQueue;

public class MergeKLists {
    public static void main(String[] args) {

    }

    /**
     * Definition for singly-linked list.
     */
    class Solution {
        public ListNode mergeKLists(ListNode[] lists) {
            PriorityQueue<Integer> minHeap = new PriorityQueue<Integer>();
            for (ListNode node : lists) {
                while (node != null) {
                    minHeap.add(node.val);
                    node = node.next;
                }
            }

            ListNode node = new ListNode(-1);
            ListNode dummy = node;
            while (!minHeap.isEmpty()) {
                dummy.next = new ListNode(minHeap.remove());
                dummy = dummy.next;
            }

            return node.next;
        }
    }
}

