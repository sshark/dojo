package org.teckhooi;

import java.util.HashMap;
import java.util.Map;

public class SubArraySum {
    public static void main(String[] args) {
        System.out.println(new SubArraySum().subarraySum(new int[]{1, 1, 1}, 2)); // 2
        System.out.println(new SubArraySum().subarraySum(new int[]{1, 2, 3}, 3)); // 2
        System.out.println(new SubArraySum().subarraySum(new int[]{1, 2, 1, 2, 1}, 3)); // 4
        System.out.println(new SubArraySum().subarraySum(new int[]{28, 54, 7, -70, 22, 65, -6}, 100)); // 1
    }

    public int subarraySum(int[] nums, int k) {
        Map<Integer, Integer> history = new HashMap<>();
        history.put(0, 1);
        int sum = 0;
        int count = 0;

        for (int i = 0; i < nums.length; i++) {
            sum += nums[i];
            if (history.containsKey(sum - k)) {
                count += history.get(sum - k);
            }

            history.put(sum, history.getOrDefault(sum, 0) + 1);
        }

        return count;
    }
}
