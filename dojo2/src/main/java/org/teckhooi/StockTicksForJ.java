package org.teckhooi;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StockTicksForJ {
    public static void main(String[] args) {
        String ticks1 = "9.20 8.03 10.02 8.08 8.14 8.10 8.31 8.28 8.35 8.34 8.39 8.45 8.38 8.38 8.32 8.36 8.28 8.28 8.38 8.48 8.49 8.54 8.73 8.72 8.76 8.74 8.87 8.82 8.81 8.82 8.85 8.85 8.86 8.63 8.70 8.68 8.72 8.77 8.69 8.65 8.70 8.98 8.98 8.87 8.71 9.17 9.34 9.28 8.98 9.02 9.16 9.15 9.07 9.14 9.13 9.10 9.16 9.06 9.10 9.15 9.11 8.72 8.86 8.83 8.70 8.69 8.73 8.73 8.67 8.70 8.69 8.81 8.82 8.83 8.91 8.80 8.97 8.86 8.81 8.87 8.82 8.78 8.82 8.77 8.54 8.32 8.33 8.32 8.51 8.53 8.52 8.41 8.55 8.31 8.38 8.34 8.34 8.19 8.17 7.16";
        String ticks2 = "9.20 8.03 10.02 5.00 8.08 8.14 8.10 8.31 8.28 8.35 8.34 8.39 8.45 8.38 8.38 4.20 8.32 8.36 8.28 8.28 8.38 8.48 8.49 8.54 8.73 8.72 8.76 8.74 8.87 8.82 8.81 8.82 8.85 8.85 8.86 8.63 8.70 8.68 8.72 8.77 8.69 8.65 8.70 8.98 8.98 8.87 8.71 9.17 9.34 9.28 8.98 9.02 9.16 9.15 9.07 9.14 9.13 9.10 9.16 9.06 9.10 9.15 9.11 8.72 8.86 8.83 8.70 8.69 8.73 8.73 8.67 8.70 8.69 8.81 8.82 8.83 8.91 8.80 8.97 8.86 8.81 8.87 8.82 8.78 8.82 8.77 8.54 8.32 8.33 8.32 8.51 8.53 8.52 8.41 8.55 8.31 8.38 8.34 8.34 8.19 8.17 10.00 4.16";
        String ticks3 = "9.20 8.03 7.00 4.16  ";
        String ticks4 = "9.20 10.03 11.00 12.16 1.0";
        List<Double> prices = Stream.of(ticks1.split(" "))
            .map(Double::parseDouble)
            .collect(Collectors.toList());


        List<Double> result = buysLowSellsHigh(prices.toArray(new Double[0]));
        System.out.println(result);
        assert result.equals(Arrays.asList(8.03, 10.02));   // add VM option, -ea, to enable assertion

        prices = Stream.of(ticks2.split(" "))
            .map(Double::parseDouble)
            .collect(Collectors.toList());

        result = buysLowSellsHigh(prices.toArray(new Double[0]));
        System.out.println(result);
        assert result.equals(Arrays.asList(4.2, 10.0));   // add VM option, -ea, to enable assertion

        prices = Stream.of(ticks3.split(" "))
            .map(Double::parseDouble)
            .collect(Collectors.toList());

        result = buysLowSellsHigh(prices.toArray(new Double[0]));
        System.out.println(result);
        assert result.isEmpty();   // add VM option, -ea, to enable assertion

        prices = Stream.of(ticks4.split(" "))
            .map(Double::parseDouble)
            .collect(Collectors.toList());

        result = buysLowSellsHigh(prices.toArray(new Double[0]));
        System.out.println(result);
        assert result.equals(Arrays.asList(9.2, 12.16));   // add VM option, -ea, to enable assertion
    }

    static public List<Double> buysLowSellsHigh(Double[] prices) {
        if (prices.length <= 1) {
            new ArrayList<>();
        }

        double bestBuy = prices[0];
        double bestSell = Double.MIN_VALUE;
        double bestProfit = Double.MIN_VALUE;

        double buy = prices[0];

        boolean tx = false;

        for (int i = 1; i < prices.length; i++) {
            if (prices[i] < buy) {
                buy = prices[i];
            } else if (prices[i] - buy > bestProfit) {
                bestProfit = prices[i] - buy;
                bestSell = prices[i];
                bestBuy = buy;
                tx = true;
            }
        }

        return tx ? Arrays.asList(bestBuy, bestSell) : Collections.emptyList();
    }
}