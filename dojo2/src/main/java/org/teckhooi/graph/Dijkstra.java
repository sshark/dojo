package org.teckhooi.graph;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class Dijkstra {

  public static Map<Integer, Integer> dijkstra(int[][] graph, int source, int target) {
    int count = graph.length;
    boolean[] visitedVertex = new boolean[count];
    int[] distance = new int[count];
    for (int i = 0; i < count; i++) {
      visitedVertex[i] = false;
      distance[i] = Integer.MAX_VALUE;
    }

    Map<Integer, Integer> acc = new HashMap<>();

    // Distance of self loop is zero
    distance[source] = 0;
    for (int i = 0; i < count; i++) {

      // Update the distance between neighbouring vertex and source vertex
      int u = findMinDistance(distance, visitedVertex);
      visitedVertex[u] = true;

      // Update all the neighbouring vertex distances
      for (int v = 0; v < count; v++) {
        if (!visitedVertex[v] && graph[u][v] != 0 && (distance[u] + graph[u][v] < distance[v])) {
          distance[v] = distance[u] + graph[u][v];
          acc.put(v, u);
        }
      }

    }
    for (int i = 0; i < distance.length; i++) {
      System.out.printf("Distance from %s to %s is %s%n", source, i, distance[i]);
    }

    return acc;
  }

  // Finding the minimum distance
  private static int findMinDistance(int[] distance, boolean[] visitedVertex) {
    int minDistance = Integer.MAX_VALUE;
    int minDistanceVertex = -1;
    for (int i = 0; i < distance.length; i++) {
      if (!visitedVertex[i] && distance[i] < minDistance) {
        minDistance = distance[i];
        minDistanceVertex = i;
      }
    }
    return minDistanceVertex;
  }

  public static void main(String[] args) {
    int[][] graph = new int[][]{
        {0, 0, 1, 2, 0, 0, 0},
        {0, 0, 2, 0, 0, 3, 0},
        {1, 2, 0, 1, 3, 0, 0},
        {2, 0, 1, 0, 0, 0, 1},
        {0, 0, 3, 0, 0, 2, 0},
        {0, 3, 0, 0, 2, 0, 1},
        {0, 0, 0, 1, 0, 5, 0}
    };
    int source = 1;
    int target = 6;

    Map<Integer, Integer> revPathMap = dijkstra(graph, source, target);
    System.out.printf("The fastest route from %s to %s is via %s%n", source, target,
        tracePath(revPathMap, source, target));
  }

  static String tracePath(Map<Integer, Integer> reversePathMap, int source, int target) {
    int current = target;
    LinkedList<String> path = new LinkedList<>();

    path.add(Integer.toString(target));
    while (current != source) {
      current = reversePathMap.get(current);
      path.add(0, Integer.toString(current));
    }

    return String.join(" -> ", path);
  }
}

/* @formatter:off
 * Example graph used
 * ------------------
 *
 * (0)-1-(2)-2-(1)
 *  |2   / \    \
 *  |  1/   \    \
 *  | /      \    \
 * (3)        \    \
 *  \          \    \
 *   \ 1      (4)-2-(5)
 *    \             /
 *    (6)----1-----/
 */
