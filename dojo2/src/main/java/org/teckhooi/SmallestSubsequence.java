package org.teckhooi;

import java.util.Stack;

public class SmallestSubsequence {
    public static void main(String[] args) {

        SmallestSubsequence smallestSubsequence = new SmallestSubsequence();
        System.out.println(smallestSubsequence.smallestSubsequence("cdadabcc"));
//        System.out.println(smallestSubsequence.smallestSubsequence("leetcode"));
    }

    public String smallestSubsequence(String text) {
        int[] last = new int[26];
        for (int i = 0; i < text.length(); i++) {
            last[text.charAt(i) - 'a'] = i;
        }

        Stack<Character> stk = new Stack<>();
        boolean[] used = new boolean[26];
        for (int i = 0; i < text.length(); i++) {
            char c = text.charAt(i);
            if (used[c - 'a']) {
                continue;
            }

            while (!stk.isEmpty() && stk.peek() > c && last[stk.peek() - 'a'] > i) {
                char top = stk.pop();
                used[top - 'a'] = false;
            }

            stk.push(c);
            used[c - 'a'] = true;
        }

        StringBuilder sb = new StringBuilder();
        for (char c : stk) {
            sb.append(c);
        }

        return sb.toString();
    }
}
