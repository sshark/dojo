package org.teckhooi;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Bar {
    /**
     * @param source  : A string
     * @param target: A string
     * @return: A string denote the minimum window, return "" if there is no such a string
     */
    public String minWindow(String source, String target) {
        // map Character -> Frequency
        int[] map = new int[128];
        // initialize map with target
        for (char c : target.toCharArray()) {
            map[c]++;
        }
        // used for check whether the searching is done
        int counter = target.length();
        int min = Integer.MAX_VALUE;
        // mark the starting point for minium window
        int start = 0;
        // j -> taker(front), i -> giver(back)
        for (int i = 0, j = 0; j < source.length(); j++) {
            char cur = source.charAt(j);
            // if current char in our map, reduce counter
            if (map[cur] > 0) {
                counter--;
            }
            map[cur]--;
            // if searching is done
            while (counter == 0) {
                // if current window < min, update
                if (j - i + 1 < min) {
                    min = j - i + 1;
                    start = i;
                }
                char pre = source.charAt(i);
                // if the char is from source
                if (map[pre] == 0) {
                    counter++;
                }
                map[pre]++;
                // squeeze the current window
                i++;
            }
        }
        return min == Integer.MAX_VALUE ? "" : source.substring(start, start + min);
    }

    public static void main(String[] args) {
        Bar bar = new Bar();
        System.out.println(bar.minWindow("ADOBEOCDEBANC", "ABC"));


        System.out.println(count("aaabeebbcac"));

        Set<Integer> ints = new HashSet<>(Arrays.asList(1, 2, 3, 1));
        System.out.println(ints);

    }

    public static String count(String s) {
        char[] xs = s.toCharArray();
        char c = xs[0];
        int count = 1;

        StringBuilder buffer = new StringBuilder();

        for (int i = 1; i < xs.length; i++) {
            if (xs[i] == c) {
                count++;
            } else {
                buffer.append(c).append(Integer.toString(count));
                c = xs[i];
                count = 1;
            }
        }
        buffer.append(c).append(Integer.toString(count));

        return buffer.toString();
    }
}