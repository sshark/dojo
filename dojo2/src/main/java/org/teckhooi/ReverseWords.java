package org.teckhooi;

import java.util.Arrays;

public class ReverseWords {
    public static void reverseWord(int i, int j, char[] word) {
        while (i < j) {
            swap(word, i, j);
            i++;
            j--;
        }
    }

    static void swap(char[] xs, int i, int j) {
        char c = xs[i];
        xs[i] = xs[j];
        xs[j] = c;
    }

    static void reverseWords(char[] words) {
        reverseWord(0, words.length - 1, words);
        int end = 0;
        int start = 0;
        for (int i = 0; i < words.length; i++) {
            if (words[i] == ' ') {
                end = i - 1;
                reverseWord(start, end, words);
                start = i + 1;
            }
        }
        reverseWord(start, words.length - 1, words);
    }

    public static void main(String[] args) {
        char[] strings = "prefect makes practice".toCharArray();
        reverseWords(strings);
        System.out.println(Arrays.toString(strings));

        char[] freedom = "free financially be to going am i".toCharArray();
        reverseWords(freedom);
        System.out.println(Arrays.toString(freedom));
    }
}
