package org.teckhooi;

/*
import javaslang.collection.List;
import javaslang.control.Try;

import java.util.function.BiFunction;

import static javaslang.API.$;
import static javaslang.API.Case;
import static javaslang.API.Match;
*/

public class BowlingScore {
/*
    private List<Integer> frames;
    private String scoreLine;

    public BowlingScore(String scoreLine) {
        this.scoreLine = scoreLine;
    }

    @Override
    public String toString() {
        return score().map(x -> "The sum of \"" + scoreLine + "\" is " + x).getOrElseGet(t -> t.getMessage());
    }

    public Try<Integer> score() {
        List<Character> chars = List.ofAll(scoreLine.toCharArray());

        BiFunction<List<Integer>, Character, List<Integer>> f = (List<Integer> l, Character c) -> Match(c).of(
            Case($('/'), () -> l.append(10 - l.last())),
            Case($('-'), () -> l.append(0)),
            Case($('X'), () -> l.append(10)),
            Case($(), x -> Character.isDigit(x) && x != '0' ? l.append(x - 0x30) : l.append(-1))
        );

        frames = chars.scanLeft(List.of(), f).last();
        if (frames.contains(-1))
            return Try.failure(new IllegalArgumentException(scoreLine + " contains invalid characters"));

        return Try.success(_score(frames, 0, 0));
    }

    private int _score(List<Integer> frames, int acc, int turn) {
        if (turn < 10) {
            if (frames.head() == 10) {
                return _score(frames.tail(), acc + 10 + frames.get(1) + frames.get(2), turn + 1);
            } else if (frames.head() + frames.get(1) == 10) {
                return _score(frames.drop(2), acc + 10 + frames.get(2), turn + 1);
            } else {
                return _score(frames.drop(2), acc + frames.head() + frames.get(1), turn + 1);
            }
        } else return acc;
    }

    public static void main(String[] args) {
        BowlingScore mixScore = new BowlingScore("14456/5/X-17/6/X2/6");
        BowlingScore invalidScoreLine = new BowlingScore("14456/5/x-17/6/X2/6");
        BowlingScore strikes = new BowlingScore("XXXXXXXXXXXX");

        // assert keyword must be standalone; include VM option -ea to enable assert
        assert (mixScore.score().get() == 133);
        assert (strikes.score().get() == 300);

        System.out.println(mixScore);
        System.out.println(invalidScoreLine);
        System.out.println(strikes);
    }
*/
}
