package org.teckhooi;

public class SuperUglyNumber {
    public static void main(String[] args) {
        SuperUglyNumber superUglyNumber = new SuperUglyNumber();
        System.out.println(superUglyNumber.nthSuperUglyNumber(12, new int[]{2, 7, 13, 19}));
    }

    public int nthSuperUglyNumber(int n, int[] primes) {
        int[] ndx = new int[primes.length];
        int[] result = new int[n];
        result[0] = 1;

        for (int i = 1; i < n; ++i) {
            long prev = result[i - 1];

            for (int j = 0; j < ndx.length; j++) {
                if (result[ndx[j]] * primes[j] <= prev) {
                    ++ndx[j];
                }
            }

            int[] values = new int[primes.length];

            for (int k = 0; k < ndx.length; k++) {
                values[k] = result[ndx[k]] * primes[k];
            }

            result[i] = min(values);
        }

        return result[n - 1];

    }

    public int min(int[] values) {
        int min = Integer.MAX_VALUE;


        for (int i = 0; i < values.length; i++) {
            if (min > values[i]) {
                min = values[i];
            }
        }
        return min;
    }
}
