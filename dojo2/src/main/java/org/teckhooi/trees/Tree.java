package org.teckhooi.trees;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public abstract class Tree<A extends Comparable<A>> {
    @SuppressWarnings("rawtypes")
    private static Tree EMPTY = new Empty();

    abstract Tree<A> right();

    abstract Tree<A> left();

    abstract Tree<A> merge(Tree<A> ta);

    public abstract A value();

    public abstract Tree<A> insert(A a);

    public abstract Tree<A> remove(A a);

    public abstract boolean member(A a);

    public abstract boolean isEmpty();

    public abstract int size();

    public abstract int height();

    public static <A extends Comparable<A>> Tree<A> tree(A... args) {
        return _tree(empty(), Arrays.asList(args));
    }

    private static <A extends Comparable<A>> Tree<A> _tree(Tree<A> acc, List<A> xs) {
        if (xs.isEmpty()) {
            return acc;
        } else {
            return _tree(acc.insert(xs.get(0)), tail(xs));
        }
    }

    private static <B> List<B> tail(List<B> xs) {
        if (xs.size() <= 1) {
            return Collections.emptyList();
        }

        return xs.subList(1, xs.size());
    }

    private static class Empty<A extends Comparable<A>> extends Tree<A> {

        @Override
        public boolean isEmpty() {
            return true;
        }

        @Override
        public A value() {
            throw new IllegalArgumentException("value() cannot be called on empty");
        }

        @Override
        public Tree<A> right() {
            throw new IllegalStateException("right() cannot be called on empty");
        }

        @Override
        public Tree<A> left() {
            throw new IllegalStateException("left() cannot be called on empty");
        }

        @Override
        public boolean member(A a) {
            throw new IllegalStateException("member(...) cannot be called on empty");
        }

        @Override
        public Tree<A> insert(A a) {
            return new T(empty(), a, empty());
        }

        @Override
        public Tree<A> remove(A a) {
            return this;
        }

        @Override
        Tree<A> merge(Tree<A> ta) {
            return ta;
        }

        @Override
        public int size() {
            return 0;
        }

        @Override
        public int height() {
            return 0;
        }

        @Override
        public String toString() {
            return "E";
        }
    }


    private static class T<A extends Comparable<A>> extends Tree<A> {
        private final A value;
        private final Tree<A> left;
        private final Tree<A> right;

        public T(Tree<A> left, A value, Tree<A> right) {
            this.value = value;
            this.left = left;
            this.right = right;
        }

        @Override
        public boolean isEmpty() {
            return false;
        }

        @Override
        public A value() {
            return value;
        }

        @Override
        public Tree<A> right() {
            return right;
        }

        @Override
        public Tree<A> left() {
            return left;
        }

        @Override
        public Tree<A> insert(A a) {
            return a.compareTo(value) > 0 ? new T(left(), value, right().insert(a)) :
                a.compareTo(value) < 0 ? new T(left().insert(a), value, right()) :
                    new T(left(), a, right());
        }

        @Override
        public int size() {
            return 1 + left.size() + right.size();
        }

        @Override
        public int height() {
            return 1 + Math.max(left.height(), right.height());
        }

        @Override
        public boolean member(A a) {
            return value.equals(a) || right.member(a) || left.member(a);
        }

        @Override
        public Tree<A> remove(A a) {
            return value.compareTo(a) > 0 ? new T(left().remove(a), value, right()) :
                value.compareTo(a) < 0 ? new T(left(), value, right().remove(a)) :
                    left().merge(right());
        }

        Tree<A> merge(Tree<A> ta) {
            if (ta.isEmpty()) {
                return this;
            }

            if (value.compareTo(ta.value()) > 0) {
                return new T(left().merge(new T(ta.left(), ta.value(), empty())), value, right())
                    .merge(ta.right());
            }

            if (value.compareTo(ta.value()) < 0) {
                return new T(left(), value, right().merge(new T(empty(), ta.value(), ta.right()))
                    .merge(ta.left()));
            }

            return new T(left().merge(ta.left()), value, right().merge(ta.right()));
        }

        @Override
        public String toString() {
            return String.format("(T %s %s %s)", left, value, right);
        }
    }

    @SuppressWarnings("unchecked")
    public static <A extends Comparable<A>> Tree<A> empty() {
        return EMPTY;
    }
}
