package org.teckhooi;

import java.util.Arrays;
import java.util.List;
import java.util.Stack;

class StockSpanner {

    public static void main(String[] args) {
        StockSpanner spanner = new StockSpanner();
//        List<Integer> prices = Arrays.asList(28, 14, 28, 35, 46, 53, 66, 80, 87, 88); // 1, 1, 3, 4,5 ,6 7, 8, 9, 10
//        List<Integer> prices = Arrays.asList(100, 80, 60, 70, 60, 75, 85); // 1, 1, 1, 2, 1, 4, 6
        List<Integer> prices = Arrays.asList(31, 41, 48, 59, 79);   // 1, 2, 3, 4, 5
        for (int price : prices) {
            System.out.println(spanner.next(price));
        }
    }

    class Pair {
        public int ndx;
        public int price;

        Pair(int ndx, int price) {
            this.ndx = ndx;
            this.price = price;
        }
    }

    Stack<Pair> history = new Stack<>();
    int count = 0;

    public StockSpanner() {

    }

    public int next(int price) {
        count++;

        if (history.isEmpty()) {
            history.push(new Pair(count, price));
            return 1;
        }

        while (!history.isEmpty() && price >= history.peek().price) {
            history.pop();
        }

        int res = history.isEmpty() ? count : count - history.peek().ndx;
        history.push(new Pair(count, price));

        return res;
    }
}