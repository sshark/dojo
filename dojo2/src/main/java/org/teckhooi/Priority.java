package org.teckhooi;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;

public class Priority {
    public static void main(String[] args) {
        PriorityQueue<Integer> queue = new PriorityQueue<>(3, new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o1 - o2;
            }
        });

        List<Integer> ints = Arrays.asList(5, 10, 3, 2, 5);

        for (int i : ints) {
            queue.add(i);
        }

/*
        while(!queue.isEmpty()) {
            System.out.println(queue.remove());
        }
*/
    }

}
