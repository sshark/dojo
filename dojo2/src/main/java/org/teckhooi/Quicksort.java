package org.teckhooi;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Quicksort {
    private static int partition(int[] arr, int l, int r, boolean reverse) {
        int i = l;
        int pivotNum = arr[r];

        for (int j = l; j <= r - 1; j++) {
            if (arr[j] <= pivotNum && !reverse) {
                swap(arr, i, j);
                i++;
            }
        }
        swap(arr, i, r);

        return i;
    }

    private static void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    public static void main(String[] args) {
        int[] unsorted = {10, 3, 1, 2, 7, 5, 12};
        printlnArr(unsorted);
        sort(unsorted, 0, unsorted.length - 1);
        printlnArr(unsorted);
    }

    static private void printlnArr(int[] arr) {
        System.out.println(Arrays.toString(arr));
    }

    public static void sort(int[] arr, int l, int r) {
        if (l < r) {
            int pivot = partition(arr, l, r, false);
            sort(arr, l, pivot - 1);
            sort(arr, pivot + 1, r);
        }
    }

    public static class RegexFoo {
        public static void main(String[] args) {
            Pattern p = Pattern.compile("(a+?).*");
            Matcher m = p.matcher("aaaabbcc");
            int n = m.groupCount();
            System.out.println(n);
            m.results().forEach(r -> System.out.println(r.group(1)));
        }
    }
}
