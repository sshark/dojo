package org.teckhooi;

import java.util.Arrays;
import java.util.Stack;

public class NextGreaterNodeInLinkedList {
    static public class ListNode {
        int val;
        ListNode next;

        ListNode() {
        }

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }
    }

    public static void main(String[] args) {
//        int[] ints = new int[]{1, 7, 5, 1, 9, 2, 5, 1};
//        int[] ints = new int[]{5, 2, 5};
//        int[] ints = new int[]{2, 2, 5};
//        int[] ints = new int[]{4, 3, 2, 5, 1, 8, 10};
//        int[] ints = new int[]{2, 2, 2};
        int[] ints = new int[]{9, 7, 6, 7, 6, 9};

        ListNode head = new ListNode();
        ListNode node = head;
        ListNode prev = null;

        for (int i : ints) {
            node.val = i;
            node.next = new ListNode();
            prev = node;
            node = node.next;
        }

        prev.next = null;

        System.out.println(Arrays.toString(new NextGreaterNodeInLinkedList().nextLargerNodes(head)));
    }

    public int[] nextLargerNodes(ListNode head) {

        Stack<Integer> stack = new Stack<>();

        ListNode next = head;
        while (next.next != null) {
            stack.push(next.val);
            next = next.next;
        }
        stack.push(next.val);

        int[] result = new int[stack.size()];

        int max = 0;
        int prev = 0;

        for (int i = result.length - 1; i >= 0; i--) {
            int peek = stack.peek();

            if (peek == max) {
                stack.pop();
            } else if (peek < max && peek < prev) {
                result[i] = Math.min(max, prev);
                max = result[i];
                stack.pop();
            } else if (peek > max) {
                result[i] = 0;
                max = stack.pop();
            } else {
                result[i] = max;
                stack.pop();
            }
            prev = peek;
        }

        return result;
    }
}
