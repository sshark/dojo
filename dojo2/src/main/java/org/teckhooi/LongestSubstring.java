package org.teckhooi;

import java.util.HashSet;
import java.util.Set;

public class LongestSubstring {
    public static void main(String[] args) {
/*
        System.out.println(lengthOfLongestSubstring("abcadcbb"));
        System.out.println(lengthOfLongestSubstring("abcabcbb"));
        System.out.println(lengthOfLongestSubstring("bbbbb"));
        System.out.println(lengthOfLongestSubstring("pwwkew"));
        System.out.println(lengthOfLongestSubstring("dvdf"));
*/


    }

    static public int lengthOfLongestSubstring(String s) {
        Set<Character> chars = new HashSet<>();
        int a = 0;
        int b = 0;
        char[] xs = s.toCharArray();
        int max = 0;

        while (b < xs.length) {
            if (chars.contains(xs[b])) {
                chars.remove(xs[a]);
                a++;
            } else {
                chars.add(xs[b]);
                b++;
                max = Math.max(max, chars.size());
            }
        }

        return max;
    }
}
