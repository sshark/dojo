package org.teckhooi;

import java.util.Arrays;
import java.util.Objects;

public class Trie<A> {
    private String remainingKeys;
    private A value;
    private char key;
    private Trie[] keysMap = new Trie[26];

    public Trie() {
        this.key = Character.MIN_VALUE;
    }

    private Trie(char key) {
        this.key = key;
    }

    public Trie(char key, A value) {
        this.value = value;
        this.key = key;
    }

    public A getValue() {
        return value;
    }

    public char getKey() {
        return key;
    }

    public boolean isFinalSeq() {
        return Arrays.stream(keysMap).allMatch(Objects::isNull);
    }

    public Trie[] getKeysMap() {
        Trie[] keysMapCopy = new Trie[26];
        System.arraycopy(keysMap, 0, keysMapCopy, 0, 26);
        return keysMapCopy;
    }

    public A get(String key) {
        if (key == null || key.isEmpty()) {
            return null;
        }

        A value = null;

        String lowerKey = key.toLowerCase();
        for (int i = 0; i < lowerKey.length(); i++) {
            int k = lowerKey.charAt(0) - 'a';
            Trie<A> nextTrie = keysMap[k];
            if (nextTrie != null) {
                if (lowerKey.length() == 1) {
                    value = nextTrie.getValue();
                    break;
                } else {
                    value = nextTrie.get(lowerKey.substring(1));
                }
            } else {
                break;
            }

        }
        return value;
    }

    public Trie<A> add(String key, A value) {
        if (key == null || key.isEmpty()) {
            return this;
        }

        String lowerKey = key.toLowerCase();
        int k = lowerKey.charAt(0) - 'a';
        Trie<A> nextTrie = keysMap[k];
        if (nextTrie != null) {
            nextTrie.add(key.substring(1), value);
        } else {
            Trie<A> newTrie;
            if (lowerKey.length() == 1) {
                newTrie = new Trie<>(lowerKey.charAt(0), value);
            } else {
                newTrie = new Trie<>(lowerKey.charAt(0));
                newTrie.add(key.substring(1), value);
            }
            keysMap[k] = newTrie;
        }
        return this;
    }

    @Override
    public String toString() {
        return "Trie{" +
            "remainingKeys='" + remainingKeys + '\'' +
            ", value=" + value +
            ", key=" + key +
            ", keysMap=" + Arrays.toString(keysMap) +
            ", endSeq=" + isFinalSeq() +
            '}';
    }
}
