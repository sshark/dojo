package org.teckhooi;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.ReentrantLock;

public class LocksPackagePractice {
    private int i = 0;
    ReentrantLock lock = new ReentrantLock();
    Runnable r = () -> {
        printValue();
    };

    public static void main(String[] args) {
        ExecutorService service = Executors.newFixedThreadPool(2);

        new LocksPackagePractice().trigger(service);

        service.shutdown();

    }

    void trigger(ExecutorService service) {
        service.submit(r);
        for (int i = 0; i < 20; i++) {
            service.execute(r);
        }
    }

    void printValue() {
        lock.lock();
        try {
            System.out.println(Thread.currentThread().getName() + " and value is = " + i++);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }
}
