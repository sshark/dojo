package org.teckhooi;

public class LongestPalindromicSubString {
    public static void main(String[] args) {

    }

    public static String longestString(String s) {
        int left = 0;
        int right = 0;
        int len = s.length();
        StringBuilder maxBuffer = new StringBuilder();
        StringBuilder buffer = new StringBuilder();

        for (int i = 0; i < len; i++) {
            left = i;
            right = i + 1;
            if (right == len) {
                break;
            }
            while (s.charAt(left) == s.charAt(right)) {
                if (right == len) {
                    break;
                }
                buffer.append(s.charAt(left)).append(s.charAt(right));
                left--;
                right++;
            }
            if (buffer.length() > maxBuffer.length()) {
                maxBuffer = new StringBuilder(buffer.toString());
            }
        }

        return buffer.toString();
    }
}
