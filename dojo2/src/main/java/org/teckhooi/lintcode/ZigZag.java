package org.teckhooi.lintcode;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;

public class ZigZag {
    public static void main(String[] args) {
        String[] strings = {"3", "9", "20", "#", "#", "15", "7", "11", "12", "13", "14", "#", "#", "#", "#"};

        Stack<String> stack = new Stack<>();
        Queue<String> queue = new ArrayDeque<>();

        List<String> acc = new ArrayList<>();
        acc.add(("[" + strings[0] + "]"));

        zigZag(strings, stack, queue, 1, acc).forEach(System.out::println);
    }

    private static List<String> zigZag(String[] strings, Stack<String> stack, Queue<String> queue, int level, List<String> acc) {
        int count = 1 << level;
        int base = (1 << level) - 1;

        if (base >= strings.length) {
            return acc;
        }

        if (level % 2 == 1) {
            for (int i = base; i < count + base; i++) {
                String s = strings[i];
                if (!s.equals("#")) {
                    stack.push(s);
                }
            }

            StringBuilder buffer = new StringBuilder();
            while (!stack.empty()) {
                buffer.append("[").append(stack.pop()).append(",").append(stack.pop()).append("]");
            }
            acc.add(buffer.toString());
        } else {
            for (int i = base; i < count + base; i++) {
                String s = strings[i];
                if (!s.equals("#")) {
                    queue.offer(s);
                }
            }
            StringBuilder buffer = new StringBuilder();
            while (!queue.isEmpty()) {
                buffer.append("[").append(queue.remove()).append(",").append(queue.remove()).append("]");
            }
            acc.add(buffer.toString());
        }

        return zigZag(strings, stack, queue, level + 1, acc);
    }
}
