package org.teckhooi;

import java.util.Arrays;

public class NthSmallest {
    private static int partition(int[] arr, int l, int r) {
        int i = l;
        int pivotNum = arr[r];

        for (int j = l; j <= r - 1; j++) {
            if (arr[j] <= pivotNum) {
                swap(arr, i, j);
                i++;
            }
        }
        swap(arr, i, r);

        return i;
    }

    private static void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    public static void main(String[] args) {
        int[] ints = {3, 7, 1, 2, 8, 10, 4, 5, 6, 9};

        printlnArr(ints);
        System.out.println(nthSmallest(ints, 0, ints.length - 1, 3, true));
        printlnArr(ints);
    }

    static private void printlnArr(int[] arr) {
        System.out.println(Arrays.toString(arr));
    }

    public static int nthSmallest(int[] arr, int l, int r, int k, boolean reverse) {
        return reverse ?
            nthSmallest(arr, l, r, arr.length - k) :
            nthSmallest(arr, l, r, k);
    }

    public static int nthSmallest(int[] arr, int l, int r, int k) {
        int pivot = partition(arr, l, r);
        if (k == pivot - l) {
            return arr[pivot];
        } else if (pivot - l > k - 1) {
            return nthSmallest(arr, l, pivot - 1, k);
        } else {
            // normalize 'k' with k + l - pivot - 1 at the initial call
            return nthSmallest(arr, pivot + 1, r, k + l - pivot - 1);
        }
    }
}
