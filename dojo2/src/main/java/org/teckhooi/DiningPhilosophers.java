package org.teckhooi;

import java.util.concurrent.Semaphore;

import static java.lang.Integer.max;
import static java.lang.Integer.min;

public class DiningPhilosophers {
    private Semaphore forks[];
    private static final int NUM_OF_DINERS = 5;
    private static final int NUM_OF_ROUNDS = 10;

    public static void main(String[] args) {
        DiningPhilosophers diningPhilosophers = new DiningPhilosophers(NUM_OF_DINERS);

        Thread[] diners = new Thread[NUM_OF_DINERS];
        for (int i = 0; i < diners.length; i++) {
            final int j = i;
            diners[i] = new Thread(() -> {
                for (int k = 0; k < NUM_OF_ROUNDS; k++) {
                    diningPhilosophers.wantsToEat(j);
                }
            });
        }

        for (Thread diner : diners) {
            diner.start();
        }
    }

    public DiningPhilosophers(int diners) {
        forks = new Semaphore[diners];
        for (int i = 0; i < diners; i++) {
            forks[i] = new Semaphore(1);
        }
    }

    public void eat(int id) {
        System.out.println(String.format("%s is eating", id));
    }

    public void pickFork(int id, int fork) {
        if (id != fork) {
            System.out.println(String.format("%s picks left fork %d", id, fork));
        } else {
            System.out.println(String.format("%s picks right fork %d", id, fork));
        }
    }

    public void putFork(int id, int fork) {
        if (id != fork) System.out.println(String.format("%s puts right fork %d", id, fork));
        else System.out.println(String.format("%s puts left fork %d", id, fork));

    }

    public void wantsToEat(int id) {
        int leftFork = min(id, (id + 1) % NUM_OF_DINERS);
        int rightFork = max(id, (id + 1) % NUM_OF_DINERS);

        forks[leftFork].acquireUninterruptibly();
        pickFork(id, leftFork);
        forks[rightFork].acquireUninterruptibly();
        pickFork(id, rightFork);
        eat(id);
        forks[rightFork].release();
        putFork(id, rightFork);
        forks[leftFork].release();
        putFork(id, leftFork);
    }
}
