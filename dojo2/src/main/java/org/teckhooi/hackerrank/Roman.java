package org.teckhooi.hackerrank;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Roman {
    static private Map<Integer, String> int2Roman = new TreeMap<Integer, String>() {{
        put(1, "I");
        put(4, "IV");
        put(2, "II");
        put(10, "X");
        put(3, "III");
        put(5, "V");
    }};

    static private List<Integer> numTokens = new ArrayList<>(int2Roman.keySet());

    public static class Pair {
        private String s;
        private Integer i;

        public Pair(String s, Integer i) {
            this.s = s;
            this.i = i;
        }

        @Override
        public String toString() {
            return "(" + s + ", " + i + ")";
        }
    }

    public static void main(String[] args) {
        System.out.println(numTokens);
        System.out.println(findLargestRoman(0, 10, numTokens.iterator()));
        System.out.println(findRoman(17));
        System.out.println(findRoman(20));
        System.out.println(findRoman(25));
        System.out.println(findRoman(15));
        System.out.println(findRoman(9));
        System.out.println(findRoman(5));
        System.out.println(findRoman(3));
        System.out.println(findRoman(8));

    }

    static public String findRoman(int num) {
        int remainder = num;
        StringBuffer buffer = new StringBuffer();
        while (remainder > 0) {
            Pair result = findLargestRoman(0, remainder, numTokens.iterator());
            buffer.append(result.s);
            remainder -= result.i;
        }
        return buffer.toString();
    }

    public static Pair findLargestRoman(int previous, int num, Iterator<Integer> tokens) {
        if (!tokens.hasNext()) {
            return new Pair(int2Roman.get(previous), previous);
        } else {
            int current = tokens.next();
            if (current > num) {
                return new Pair(int2Roman.get(previous), previous);
            } else if (current == num) {
                return new Pair(int2Roman.get(current), current);
            } else {
                return findLargestRoman(current, num, tokens);
            }
        }
    }
}
