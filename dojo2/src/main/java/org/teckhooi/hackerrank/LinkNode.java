package org.teckhooi.hackerrank;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class LinkNode {
    private int value;
    private LinkNode next = null;

    public LinkNode(int value) {
        this.value = value;
    }

    public LinkNode(int value, LinkNode next) {
        this.value = value;
        this.next = next;
    }

    public String toString() {
        StringBuffer buffer = new StringBuffer(value);
        LinkNode node = this;
        while (node.next != null) {
            buffer.append(node.value)
                .append(" -> ");
            node = node.next;
        }
        buffer.append(node.value);
        return buffer.toString();
    }

    public static LinkNode unique(LinkNode nodes) {
        LinkNode uniqueNode = new LinkNode(nodes.value, null);
        LinkNode startNode = uniqueNode;
        LinkNode nextNode = nodes.next;
        while (nextNode != null) {
            if (!contains(startNode, nextNode.value)) {
                uniqueNode.next = new LinkNode(nextNode.value, null);
                uniqueNode = uniqueNode.next;
            }
            nextNode = nextNode.next;
        }
        return startNode;
    }

    public static boolean contains(LinkNode node, int num) {
        while (node != null) {
            if (node.value == num) {
                return true;
            } else {
                node = node.next;
            }
        }

        return false;
    }

    public static LinkNode createNodes(List<Integer> nums) {
        LinkNode startNode = null;
        LinkNode currentNode = null;
        Iterator<Integer> numIterator = nums.iterator();
        while (numIterator.hasNext()) {
            if (startNode == null) {
                startNode = new LinkNode(numIterator.next());
                currentNode = startNode;
            } else {
                LinkNode nextNode = new LinkNode(numIterator.next());
                currentNode.next = nextNode;
                currentNode = nextNode;
            }
        }
        return startNode;
    }

    public static void main(String[] args) {
        LinkNode nodes = createNodes(Arrays.asList(1, 4, 3, 1, 2, 5, 3));
        System.out.println(nodes);
        System.out.println(unique(nodes));
    }
}
