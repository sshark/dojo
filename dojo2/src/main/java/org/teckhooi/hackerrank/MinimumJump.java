package org.teckhooi.hackerrank;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MinimumJump {

    public static void main(String[] args) {
        int[] ints = {2, 13, 1, 1, 2, 4, 2, 0, 1, 1};
        int[] steps = new int[ints.length];
        int[] minCount = new int[ints.length];
        Arrays.fill(minCount, Integer.MAX_VALUE);

        minCount[0] = 0;
        minJump(ints, steps, minCount);

        System.out.println(Arrays.toString(minCount));
        System.out.println(Arrays.toString(steps));
        System.out.println(traceSteps(steps, steps.length - 1, minCount[minCount.length - 1], new ArrayList<>()));

    }

    static List<Integer> traceSteps(int[] steps, int j, int total, List<Integer> acc) {
        for (int i = 0; i < total - 1; i++) {
            int nextStep = steps[j];
            acc.add(0, steps[j]);
            j = nextStep;

        }
        acc.add(0, 0);
        acc.add(steps.length - 1);
        return acc;
    }

    static void minJump(int[] ints, int[] steps, int[] minCount) {
        for (int i = 1; i < ints.length; i++) {
            for (int j = 0; j < i; j++) {
                if (ints[j] + j >= i) {
                    if (minCount[j] + 1 < minCount[i]) {
                        minCount[i] = minCount[j] + 1;
                        steps[i] = j;
                    }
                }
            }
        }
    }
}
