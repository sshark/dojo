package org.teckhooi.hackerrank;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Stack;

public class ValidBST {
    static private Stack<Integer> s = new Stack<>();

    static public boolean validBST(List<Integer> xs) {
        int root = Integer.MIN_VALUE;
        int size = xs.size();

        for (int i = 0; i < size; i++) {
            if (xs.get(i) < root) {
                return false;
            }

            while (!s.empty() && s.peek() < xs.get(i)) {
                root = s.peek();
                s.pop();
            }
            s.push(xs.get(i));
        }
        return true;
    }

    public static void main(String[] args) {
        System.out.println(validBST(Arrays.asList(2, 1, 3)));
        System.out.println(validBST(Arrays.asList(1, 2, 3)));
        System.out.println(validBST(Arrays.asList(4, 2, 3)));
        System.out.println(validBST(Arrays.asList(3, 2, 1, 5, 4, 6)));
        System.out.println(validBST(Arrays.asList(1, 3, 4, 2)));
        System.out.println(validBST(Arrays.asList(3, 4, 5, 1, 2)));
        System.out.println(validBST(Collections.EMPTY_LIST));
    }


}
