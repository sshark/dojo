package org.teckhooi;

import java.util.ArrayList;
import java.util.List;

public class Fib {
    public List<Integer> series(int maxNum) {
        List<Integer> fibs = new ArrayList<>(1);
        fibs.add(1);
        return series(maxNum, 1, 1, fibs);
    }

    private List<Integer> series(int maxNum, int first, int second, List<Integer> fibs) {
        if (second > maxNum) {
            return fibs;
        }

        int next = first + second;
        fibs.add(second);
        return series(maxNum, second, next, fibs);
    }
}