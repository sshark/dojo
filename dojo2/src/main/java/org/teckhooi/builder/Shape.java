package org.teckhooi.builder;

public class Shape {
    private final double opacity;

    public static class Builder<T extends Builder<T>> {
        private double opacity;

        public T opacity(double opacity) {
            this.opacity = opacity;
            return self();
        }

        protected T self() {
            return (T) this;
        }

        public Shape build() {
            return new Shape(this);
        }
    }

    public double getOpacity() {
        return opacity;
    }

    public static Builder<?> builder() {
        return new Builder<>();
    }

    protected Shape(Builder<?> builder) {
        this.opacity = builder.opacity;
    }
}
