package org.teckhooi.builder;

public class Rectangle extends Shape {
    private final double height;
    private final double length;

    public static class Builder extends Shape.Builder<Builder> {
        private double height;
        private double length;

        public Builder height(double height) {
            this.height = height;
            return self();
        }

        public Builder length(double length) {
            this.length = length;
            return self();
        }

        public Rectangle build() {
            return new Rectangle(this);
        }
    }

    @Override
    public String toString() {
        return String.format("{%s: {height: %.2f, length: %.2f, area: %.2f, opacity: %.2f}}", getClass().getName(), height, length, getArea(), getOpacity());
    }

    public static Builder builder() {
        return new Builder();
    }

    protected Rectangle(Builder builder) {
        super(builder);
        this.height = builder.height;
        this.length = builder.length;
    }

    public double getHeight() {
        return height;
    }

    public double getLength() {
        return length;
    }

    public double getArea() {
        return height * length;
    }
}
