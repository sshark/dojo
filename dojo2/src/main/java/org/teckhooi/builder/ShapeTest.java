package org.teckhooi.builder;

public class ShapeTest {
    public static void main(String[] args) {
        Rectangle.Builder rectBuilder = Rectangle.builder();
        Rectangle rect = rectBuilder
            .length(10)
            .opacity(0.5)
            .height(25)
            .build();
        System.out.println(rect.toString());
    }
}
