package org.teckhooi;

public class RottenOranges {

    public static void main(String[] args) {
        System.out.println(orangesRotting(new int[][]{{2, 1, 1}, {1, 1, 0}, {0, 1, 1}}));
        System.out.println(orangesRotting(new int[][]{{2, 1, 1}, {0, 1, 1}, {1, 0, 1}}));
        System.out.println(orangesRotting(new int[][]{{1, 1, 2, 0, 2, 0}}));
        System.out.println(orangesRotting(new int[][]{{1, 2, 1, 1, 2, 1, 1}}));
        System.out.println(orangesRotting(new int[][]{{0, 2}}));
    }

    static public int orangesRotting(int[][] grid) {
        int days = 0;
        boolean isProgress = true;

        while (isProgress) {
            isProgress = false;

            for (int i = 0; i < grid.length; i++) {
                for (int j = 0; j < grid[i].length; j++) {
                    if (grid[i][j] == 2) {
                        if (update(grid, i + 1, j) | update(grid, i, j + 1) | update(grid, i - 1, j) | update(grid, i, j - 1)) {
                            isProgress = true;
                        }
                    }
                }
            }

            if (isProgress) {
                days++;
                commit(grid);
            }
        }

        for (int[] cols : grid) {
            for (int orange : cols) {
                if (orange == 1) {
                    return -1;
                }
            }
        }

        return days;
    }

    static private int[][] commit(int[][] grid) {
        int[][] cloneGrid = new int[grid.length][grid[0].length];
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length; j++) {
                if (grid[i][j] == 4) {
                    grid[i][j] = 2;
                }
            }
        }

        return cloneGrid;
    }

    static public boolean update(int[][] grid, int y, int x) {
        if (y >= grid.length || x >= grid[0].length || y < 0 || x < 0) {
            return false;
        }

        if (grid[y][x] == 1) {
            grid[y][x] = 4;
            return true;
        }

        return false;
    }
}
