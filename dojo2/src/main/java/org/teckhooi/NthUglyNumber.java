package org.teckhooi;

public class NthUglyNumber {
    public static void main(String[] args) {
        NthUglyNumber uglyNumber = new NthUglyNumber();
        assert (15L == uglyNumber.nthUglyNumber(11));
        assert (12L == uglyNumber.nthUglyNumber(10));
        assert (373248L == uglyNumber.nthUglyNumber(415));

        long current = System.currentTimeMillis();
        System.out.println(uglyNumber.nthUglyNumber(1352));
        System.out.println(System.currentTimeMillis() - current);
    }


    public int nthUglyNumber(int n) {
        if (n > 1690) {
            return -1;
        } else {
            int last2 = 0;
            int last3 = 0;
            int last5 = 0;

            long[] result = new long[n];
            result[0] = 1;
            for (int i = 1; i < n; ++i) {
                long prev = result[i - 1];

                if (result[last2] * 2 <= prev) {
                    ++last2;
                }
                if (result[last3] * 3 <= prev) {
                    ++last3;
                }
                if (result[last5] * 5 <= prev) {
                    ++last5;
                }

                long candidate1 = result[last2] * 2;
                long candidate2 = result[last3] * 3;
                long candidate3 = result[last5] * 5;

                result[i] = Math.min(candidate1, Math.min(candidate2, candidate3));
            }

            return (int) result[n - 1];
        }
    }
}
