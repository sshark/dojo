package org.teckhooi;

import java.util.PriorityQueue;

public class MDAQ {

    public static void main(String[] args) {

/*
        System.out.println(lightbulbs(new int[] {2,3,4,1,5}));
        System.out.println(lightbulbs(new int[] {1,3,4,2,5}));
        System.out.println(lightbulbs(new int[] {3,4,2,5,1}));
        System.out.println(lightbulbs(new int[] {1,2,3,4,5}));
        System.out.println(lightbulbs(new int[] {1,2,4,5,3}));
        System.out.println(lightbulbs(new int[] {4,5,2,3,1}));

        System.out.println(casino(8, 0));
        System.out.println(casino(18, 2));
        System.out.println(casino(10, 10));
*/

        System.out.println(happyString(6, 1, 1));
        System.out.println(happyString(1, 3, 1));
        System.out.println(happyString(0, 1, 8));
        System.out.printf("'%s'\n", happyString(10, 0, 0));
        System.out.printf("'%s'\n", happyString(10, 10, 10));
    }

    static String happyString(int a, int b, int c) {
        PriorityQueue<Count> q = new PriorityQueue<>((x, y) -> y.count - x.count);

        StringBuilder sb = new StringBuilder();

        q.offer(new Count('a', a));
        q.offer(new Count('b', b));
        q.offer(new Count('c', c));

        int cnt = 0;
        char re = ' ';

        while (!q.isEmpty()) {
            Count p = q.poll();

            char ch = p.c;
            int count = p.count;
            int N = sb.length();

            if (count <= 0) continue;

            if (N <= 1) {
                sb.append(ch);
                q.offer(new Count(ch, count - 1));
            } else if (sb.charAt(N - 1) == ch && sb.charAt(N - 2) == ch) {
                re = ch;
                cnt = count;
            } else {
                sb.append(ch);
                q.offer(new Count(ch, count - 1));
                if (cnt > 0) {
                    q.offer(new Count(re, cnt));
                    cnt = 0;
                }
            }
        }
        return sb.toString();
    }

    static String diverseString(int A, int B, int C) {
        StringBuilder buffer = new StringBuilder();

        PriorityQueue<Count> queue = new PriorityQueue<>((a, b) -> b.count - a.count);
        queue.offer(new Count('a', A));
        queue.offer(new Count('b', B));
        queue.offer(new Count('c', C));

        Count first = queue.poll();
        Count second = queue.poll();

        while (first.count > 0 && second.count > 0) {
            if (first.count >= 2) {
                buffer.append(first.c).append(first.c);
                first.count -= 2;
            } else if (first.count == 1) {
                buffer.append(first.c);
                first.count--;
            }

            if (second.count >= 2) {
                buffer.append(second.c).append(second.c);
                second.count -= 2;
            } else if (second.count == 1) {
                buffer.append(second.c);
                second.count--;
            }

            queue.offer(first);
            queue.offer(second);

            first = queue.poll();
            second = queue.poll();
        }

        if (first.count >= 2) {
            buffer.append(first.c).append(first.c);
        } else if (first.count == 1) {
            buffer.append(first.c);
        }

        return buffer.toString();
    }

    static class Count {
        char c;
        int count;

        public Count(char c, int count) {
            this.c = c;
            this.count = count;
        }
    }

    static int casino(int N, int K) {
        int rounds = 0;
        int allIns = K;
        int curr = N;

        while (curr > 0) {
            if (curr % 2 == 0 && allIns > 0) {
                rounds++;
                curr >>= 1;
                allIns--;
            } else {
                curr--;
                rounds++;
            }
        }

        return rounds - 1;
    }

    static int lightbulbs(int[] A) {
        int count = 0;
        boolean[] lights = new boolean[A.length];

        for (int i = 0; i < A.length; i++) {
            boolean all = true;
            lights[A[i] - 1] = true;
            for (int j = 0; j < A[i] - 1; j++) {
                if (!lights[j]) {
                    all = false;
                    break;
                }
            }
            if (all) {
                count++;
            }
        }

        return count;
    }
}
