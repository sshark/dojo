package org.teckhooi.concurrent;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ConParTasks {
    static class DelayTask implements Runnable {
        final private long millis;
        final private String name;

        public DelayTask(long millis) {
            this.millis = millis;
            name = Thread.currentThread().getName();
        }

        public DelayTask(String name, long millis) {
            this.millis = millis;
            this.name = name;
        }

        @Override
        public void run() {
            System.out.format("%s is delayed for %d...\n", name, millis);
            try {
                Thread.sleep(millis);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            System.out.format("%s is done.\n", name);
        }
    }

    public static void main(String[] args) throws Exception {
        List<DelayTask> tasks = new ArrayList<>();
        int numOfTasks = 6;

        for (int i = 0; i < numOfTasks; i++) {
            tasks.add(new DelayTask("Task " + i, 1000));
        }

        ConParTasks mainTask = new ConParTasks();

        ExecutorService es = Executors.newFixedThreadPool(6);

        long millis = mainTask.timeTaken(() -> {
            for (DelayTask task : tasks) {
                es.submit(task);
            }
            es.shutdown();
            try {
                es.awaitTermination(10, TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        });

        System.out.format("Time taken %dms\n", millis);
    }

    public long timeTaken(Runnable runnable) {
        long start = System.currentTimeMillis();
        runnable.run();
        return System.currentTimeMillis() - start;
    }
}
