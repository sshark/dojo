package org.teckhooi.concurrent;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class RunWild {
    static int foo = 0;


    public static void main(String[] args) throws Exception {
        final int MAX_SERVICE = 3;
        final CountDownLatch latch = new CountDownLatch(3);
        final Lock lock = new ReentrantLock();

        Runnable inc = () -> {
            for (int i = 0; i < 10; i++) {
                lock.lock();
                foo++;
                lock.unlock();
            }
            latch.countDown();
        };

        ExecutorService service = Executors.newCachedThreadPool();
        for (int i = 0; i < MAX_SERVICE; i++) {
            service.submit(inc);
        }

        latch.await();
        service.shutdown();
        System.out.println("$$$: " + foo);
    }
}
