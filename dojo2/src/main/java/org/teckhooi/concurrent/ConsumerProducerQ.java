package org.teckhooi.concurrent;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ConsumerProducerQ {
    final private int capacity;
    final private Object lock = new Object();
    volatile private boolean alive = true;
    volatile private boolean producerAlive = true;

    final private Queue<String> bin = new LinkedList<>();
    final private Queue<String> queue = new LinkedList<>();

    public Queue<String> dump() {
        return bin;
    }

    public boolean isEmpty() {
        return queue.isEmpty();
    }

    public boolean isAlive() {
        return alive;
    }

    public void kill() {
        this.alive = false;
    }

    public boolean isProducerAlive() {
        return producerAlive;
    }

    public void killProducer() {
        this.producerAlive = false;
    }

    public ConsumerProducerQ(int capacity) {
        this.capacity = capacity;
    }

    public ConsumerProducerQ() {
        this(Integer.MAX_VALUE);
    }

    public void consume() throws Exception {
        synchronized (lock) {
            if (queue.isEmpty()) {
                lock.wait(200);
                if (!isProducerAlive()) {
                    return;
                }
            }
            String data = queue.remove();
            bin.add(data);
            lock.notifyAll();
        }
    }

    public void produce() throws Exception {
        synchronized (lock) {
            if (queue.size() >= capacity) {
                lock.wait();
            }

            String data = UUID.randomUUID().toString();
            queue.offer(data);
            bin.add(data);
            lock.notifyAll();
        }
    }

    public static void main(String[] args) throws Exception {
        final ConsumerProducerQ cpQ = new ConsumerProducerQ(5);
        final CountDownLatch latch = new CountDownLatch(1);

        final Runnable producer = () -> {
            while (cpQ.isAlive()) {
                try {
                    cpQ.produce();
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        };

        final Runnable consumer = () -> {
            while (cpQ.isAlive() || !cpQ.isEmpty()) {
                try {
                    cpQ.consume();
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
            latch.countDown();
        };

        ExecutorService es = Executors.newVirtualThreadPerTaskExecutor();
        es.submit(consumer);
        es.submit(producer);

        Thread.sleep(1000);
        cpQ.kill();

        latch.await();
        es.shutdown();

        var result = cpQ.dump().toArray(new String[0]);
        if (result.length % 2 == 1) {
            throw new RuntimeException("Rows count should be even");
        }

        Arrays.sort(result);

        try (PrintWriter writer = new PrintWriter(new FileWriter("results.txt"))) {
            Arrays.stream(result).forEach(writer::println);
        }

        for (int i = 0; i < result.length; i = i + 2) {
            if (!result[i].equals(result[i + 1])) {
                throw new RuntimeException(String.format("Bad values at %d, %s vs %s", i, result[i], result[i + 1]));
            }
        }
    }
}