package org.teckhooi.concurrent;

public class OddEvenCounter {
    private int value = 1;
    private final int max;

    public OddEvenCounter(int max) {
        this.max = max;
    }

    synchronized public static void oddEven(OddEvenCounter lock) throws RuntimeException {
        while (lock.value < lock.max) {
            System.out.format("%4s: %02d\n", Thread.currentThread().getName(), lock.value);
            lock.value++;

            try {
                if (lock.value % 2 == 0) {
                    OddEvenCounter.class.wait();
                    OddEvenCounter.class.notify();
                } else {
                    OddEvenCounter.class.notify();
                    OddEvenCounter.class.wait();
                }
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        OddEvenCounter.class.notify();
    }

    public static void main(String[] args) throws Exception {
        OddEvenCounter lock = new OddEvenCounter(21);
        new Thread(() -> oddEven(lock), "Odd").start();
        new Thread(() -> oddEven(lock), "Even").start();
    }
}
