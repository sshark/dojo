package org.teckhooi.concurrent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;
import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class SleepingBarberShop {
    final static private Logger LOGGER = LoggerFactory.getLogger(SleepingBarberShop.class);

    final public long POISON_PILL = -1;
    final int MAX_CHAIRS = 5;
    final int BARBERS = 50;
    final int CLOSING_SECONDS = 3;
    final BlockingQueue<Long> waitingRoom = new LinkedBlockingQueue<>(MAX_CHAIRS);

    public static void main(String[] args) {
        int tests2Run = 100;
        Thread[] tests = new Thread[Runtime.getRuntime().availableProcessors()];
        for (int i = 0; i < Math.min(tests2Run, tests.length); i++) {
            new Thread(() -> new SleepingBarberShop().run()).start();
        }
    }

    public void run() {
        final boolean[] isWorking = new boolean[]{true};

        final SleepingBarberShop barberShop = new SleepingBarberShop();
        final Random rnd = new Random();
        final long[] left = new long[]{0};
        final long[] serviced = new long[]{0};
        final long[] waited = new long[]{0};
        final long[] total = new long[]{0};
        final boolean[] shopIsOpen = new boolean[]{true};
        final CountDownLatch latch = new CountDownLatch(BARBERS);
        final Lock barberLock = new ReentrantLock();

        Runnable barber = () -> {
            while (isWorking[0]) {
                barberShop.customer().ifPresent(customer -> {
                    if (customer < 0) {
                        isWorking[0] = false;
                    } else {
                        LOGGER.debug("Barber {} cutting customer {} hair", Thread.currentThread().getId(), customer);
                        barberLock.lock();
                        serviced[0]++;
                        barberLock.unlock();

                        try {
                            Thread.sleep(50 + rnd.nextInt(100));
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
            latch.countDown();
        };

        Runnable customers = () -> {
            while (shopIsOpen[0]) {
                total[0]++;
                if (!barberShop.fullHouse()) {
                    barberShop.walkIn(total[0]);
                    waited[0]++;
                    LOGGER.debug("Customer {} walked in", total[0]);
                } else {
                    LOGGER.debug("Customer {} LEFT", total[0]);
                    left[0]++;
                }

                try {
                    Thread.sleep(rnd.nextInt(5));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            barberShop.close();
        };

        ScheduledExecutorService scheduled = Executors.newSingleThreadScheduledExecutor();
        scheduled.schedule(() -> {
            shopIsOpen[0] = false;
            LOGGER.debug("Shop is CLOSED");
        }, CLOSING_SECONDS, TimeUnit.SECONDS);

        ExecutorService services = Executors.newCachedThreadPool();

        for (int i = 0; i < BARBERS; i++) {
            services.submit(barber);
        }

        services.submit(customers);
        services.shutdown();
        scheduled.shutdown();

        try {
            latch.await();
            LOGGER.info("Total: {}, waited: {}, serviced: {}, left: {}",
                total[0], waited[0], serviced[0], left[0]);

            assert (total[0] == serviced[0] + left[0]);
            assert (waited[0] == serviced[0]);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public boolean fullHouse() {
        return !waitingRoom.isEmpty();
    }

    public void walkIn(long id) {
        try {
            waitingRoom.put(id);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void close() {
        try {
            waitingRoom.put(POISON_PILL);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public Optional<Long> customer() {
        try {
            return Optional.ofNullable(waitingRoom.poll(100, TimeUnit.MILLISECONDS));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }
}
