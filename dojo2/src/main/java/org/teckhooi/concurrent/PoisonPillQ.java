package org.teckhooi.concurrent;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.Optional;
import java.util.Queue;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class PoisonPillQ {
    final private int capacity;
    final private Object lock = new Object();
    volatile private boolean alive = true;

    final private Queue<String> bin = new LinkedList<>();
    final private Queue<Optional<String>> queue = new LinkedList<>();

    public Queue<String> dump() {
        return bin;
    }

    public boolean isAlive() {
        return alive;
    }

    public void kill() {
        this.alive = false;
    }

    public void killProducer() {
        try {
            produce(Optional.empty());  // poison pill
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public PoisonPillQ(int capacity) {
        this.capacity = capacity;
    }

    public PoisonPillQ() {
        this(Integer.MAX_VALUE);
    }

    public boolean consume() throws Exception {
        synchronized (lock) {
            while (queue.isEmpty()) {
                lock.wait();
            }

            Optional<String> data = queue.remove();
            data.ifPresent(d -> {
                bin.add(d);
            });

            boolean exit = data.isEmpty();
            lock.notifyAll();
            return exit;
        }
    }

    public void produce() throws Exception {
        produce(Optional.of(UUID.randomUUID().toString()));
    }

    public void produce(Optional<String> data) throws Exception {
        synchronized (lock) {
            if (queue.size() >= capacity) {
                lock.wait();
            }

            queue.offer(data);
            data.ifPresent(bin::add);
            lock.notifyAll();
        }
    }

    public static void main(String[] args) throws Exception {
        final PoisonPillQ cpQ = new PoisonPillQ(5);
        final CountDownLatch latch = new CountDownLatch(1);

        final Runnable producer = () -> {
            while (cpQ.isAlive()) {
                try {
                    cpQ.produce();
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
            cpQ.killProducer();
        };

        final Runnable consumer = () -> {
            boolean exit = false;
            while (!exit) {
                try {
                    exit = cpQ.consume();
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
            latch.countDown();
        };

        ExecutorService es = Executors.newCachedThreadPool();
        es.submit(consumer);
        es.submit(producer);

        Thread.sleep(300);
        cpQ.kill();

        latch.await();
        es.shutdown();

        var sortedResult = cpQ.dump().stream().sorted().toList().toArray(new String[0]);
        if (sortedResult.length % 2 != 0) {
            throw new RuntimeException("Result count should be even");
        }

        try (PrintWriter writer = new PrintWriter(new FileWriter("results.txt"))) {
            cpQ.dump().forEach(writer::println);

//            Future.firstCompletedOf()
        }

        for (int i = 0; i < sortedResult.length; i = i + 2) {
            if (!sortedResult[i].equals(sortedResult[i + 1])) {
                throw new RuntimeException(String.format("The results does not tally, %s, %s", sortedResult[i], sortedResult[i + 1]));
            }
        }
    }
}