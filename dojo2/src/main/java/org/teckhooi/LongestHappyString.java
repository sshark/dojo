package org.teckhooi;


import java.util.PriorityQueue;

public class LongestHappyString {

    public static void main(String[] args) {
        System.out.println(longestDiverseString(2, 2, 7));
    }

    static public String longestDiverseString(int a, int b, int c) {
        PriorityQueue<Freq> q = new PriorityQueue<>(
            (x, y) -> y.count - x.count);
        q.offer(new Freq('a', a));
        q.offer(new Freq('b', b));
        q.offer(new Freq('c', c));
        StringBuilder sb = new StringBuilder();
        while (q.size() >= 2) {
            Freq f1 = q.poll();
            Freq f2 = q.poll();
            if (sb.length() == 0 || sb.charAt(sb.length() - 1) != f1.c) {
                sb.append(f1.c);
                f1.count--;
            } else {
                if (sb.length() == 1) {
                    sb.append(f1.c);
                    f1.count--;
                } else if (sb.charAt(sb.length() - 1) != sb.charAt(sb.length() - 2)) {
                    sb.append(f1.c);
                    f1.count--;
                } else {
                    sb.append(f2.c);
                    f2.count--;
                }
            }
            if (f1.count > 0) q.offer(f1);
            if (f2.count > 0) q.offer(f2);
        }
        if (q.size() == 1) {
            Freq last = q.poll();
            if (sb.charAt(sb.length() - 1) != last.c) {
                if (last.count >= 2) {
                    sb.append(last.c);
                    sb.append(last.c);
                } else {
                    sb.append(last.c);
                }
            } else {
                if (sb.charAt(sb.length() - 1) != sb.charAt(sb.length() - 2)) {
                    sb.append(last.c);
                }
            }
        }
        return sb.toString();
    }

    static class Freq {
        public char c;
        public int count;

        public Freq(char ch, int count) {
            this.c = ch;
            this.count = count;
        }
    }
}

