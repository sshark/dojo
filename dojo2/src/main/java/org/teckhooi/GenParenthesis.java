package org.teckhooi;

import java.util.ArrayList;
import java.util.List;

public class GenParenthesis {
    public static void main(String[] args) {
        System.out.println(generateParenthesis(3));
    }

    static public List<String> generateParenthesis(int n) {
        List<String> arr = new ArrayList<>();
        genParenthesis(n - 1, n, "(", arr);
        return arr;
    }

    static void genParenthesis(int b, int c, String s, List<String> arr) {
        if (b == 0 && c == 0) arr.add(s);
        else {
            if (b > 0 && b <= c) {
                genParenthesis(b - 1, c, s + "(", arr);
            }

            if (c > 0) {
                genParenthesis(b, c - 1, s + ")", arr);
            }
        }
    }
}
