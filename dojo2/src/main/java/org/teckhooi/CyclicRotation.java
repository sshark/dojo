package org.teckhooi;

import java.util.Arrays;

public class CyclicRotation {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(solution(new int[]{3, 8, 9, 7, 6}, 11)));
    }

    static public int[] solution(int[] A, int K) {
        boolean isSame = true;
        for (int i = 1; i < A.length; i++) {
            if (A[i] != A[0]) {
                isSame = false;
                break;
            }
        }

        if (isSame) {
            return A;
        }

        int rotation = K % A.length;
        if (rotation == 0) {
            return A;
        }

        int[] arr = new int[A.length];
        System.arraycopy(A, 0, arr, rotation, A.length - rotation);
        System.arraycopy(A, A.length - rotation, arr, 0, rotation);

        return arr;
    }
}
