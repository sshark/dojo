package org.teckhooi;

public class CountDiv {
    public static void main(String[] args) {
        System.out.println(solution(101, 123_451_000, 10_000));
        System.out.println(solution(0, 2_000_000_000, 1));


    }


    static public int solution2(int A, int B, int K) {
        int count = 0;
        for (int i = A; i <= B; i++) {
            if (i % K == 0) {
                count++;
            }
        }
        return count;
    }

    static public int solution(int A, int B, int K) {
        int count = 0;
        int start = 0;
        int remainder = A % K;
        start = A + remainder;

        while (start <= B) {
            if (B - start >= K) {
                count++;
            }
            start += K;
        }

        return count;
    }
}
