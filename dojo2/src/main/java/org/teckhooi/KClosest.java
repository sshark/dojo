package org.teckhooi;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class KClosest {

    public static void main(String[] args) {
        int[][] points = new int[][]{
            new int[]{1, 3}, new int[]{-2, 2}
        };

        int[][] result = kClosest(points, 1);

        List<String> strs = new ArrayList<>();
        for (int[] point : result) {
            strs.add(Arrays.toString(point));
        }

        System.out.printf("[%s]%n", String.join(",", strs));
    }

    static public int[][] kClosest(int[][] points, int K) {
        Pair[] pairs = new Pair[points.length];
        for (int i = 0; i < points.length; i++) {
            pairs[i] = new Pair(points[i][0], points[i][1]);
        }

        Arrays.sort(pairs, Comparator.comparingInt(p -> (p.x * p.x + p.y * p.y)));

        int[][] selected = new int[K][2];

        for (int i = 0; i < K; i++) {
            selected[i] = new int[]{pairs[i].x, pairs[i].y};
        }
        return selected;
    }

    static class Pair {
        int x;
        int y;

        Pair(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }
}
