package org.teckhooi;

public class FindKthLargest {

    public static void main(String[] args) {
        Integer[] arr = new Integer[]{11, 1, 4, 5, 18, 3, 10, 8, 9, 6, 17, 16, 7};
        int k = 2;  // 0 < k <= arr.length
        int kthLargest = arr.length - k;
        System.out.println(new FindKthLargest().findKthElementByQuickSelect(arr, 0, arr.length - 1, kthLargest));
    }

    public int findKthElementByQuickSelect(Integer[] arr, int left, int right, int k) {
        if (k >= 0 && k <= right - left + 1) {
            int pos = partition(arr, left, right);
            if (pos - left == k) {
                return arr[pos];
            }
            if (pos - left > k) {
                return findKthElementByQuickSelect(arr, left, pos - 1, k);
            }
            return findKthElementByQuickSelect(arr, pos + 1, right, k - pos + left - 1);
        }
        return 0;
    }

    private int partition(Integer[] arr, int left, int right) {
        int pivot = arr[right], i = left;
        for (int j = left; j <= right - 1; j++) {
            if (arr[j] <= pivot) {
                swap(arr, i, j);
                i++;
            }
        }
        swap(arr, i, right);
        return i;
    }

    private void swap(Integer[] arr, int n1, int n2) {
        int temp = arr[n2];
        arr[n2] = arr[n1];
        arr[n1] = temp;
    }
}

