package org.teckhooi.dojo2.state

/**
  *
  * @author thlim
  *
  */

case class State[S, +A](run: S => (A, S)) {
  def flatMap[B](f: A => State[S, B]): State[S, B] = State(s => {
    val (a, rng2) = run(s)
    f(a).run(rng2)
  })

  def map[B](f: A => B): State[S, B] = flatMap(a => State(x => (f(a), x)))

  def map2[B, C](rb: State[S, B])(f: (A, B) => C): State[S, C] = flatMap(a => rb.map(b => f(a, b)))
}

object State {
  def unit[S, A](a: A): State[S, A] = State(s => (a, s))

  def sequence[S, A](ls: List[State[S, A]]): State[S, List[A]] =
    ls.reverse.foldLeft(unit[S, List[A]](List()))((acc, s) => s.map2(acc)(_ :: _))

  def modify[S](f: S => S): State[S, Unit] = for {
    s <- get // Gets the current state and assigns it to `s`.
    _ <- set(f(s)) // Sets the new state to `f` applied to `s`.
  } yield ()

  def get[S]: State[S, S] = State(s => (s, s))

  def set[S](s: S): State[S, Unit] = State(_ => ((), s))
}
