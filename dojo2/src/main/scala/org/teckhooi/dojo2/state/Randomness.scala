package org.teckhooi.dojo2.state

import scala.annotation.tailrec

object Randomness {
  def nonNegativeInt(rng: RNG): (Int, RNG) = {
    val (a, nextRng) = rng.nextInt
    (if (a < 0) -a + 1 else a, nextRng)
  }

  def double(rng: RNG): (Double, RNG) = {
    val (a, nextRng) = nonNegativeInt(rng)
    (a.toDouble / Int.MaxValue, nextRng)
  }

  def intDouble(rng: RNG): ((Int, Double), RNG) = {
    val (i, nextIntRng) = rng.nextInt
    val (d, nextDoubleRng) = nextIntRng.nextInt

    ((i, d), nextDoubleRng)
  }

  def doubleInt(rng: RNG): ((Double, Int), RNG) = {
    val ((i, d), nextRng) = intDouble(rng)

    ((d, i), nextRng)
  }

  def double3(rng: RNG): ((Double, Double, Double), RNG) = {
    val (d1, nextRng1) = double(rng)
    val (d2, nextRng2) = double(nextRng1)
    val (d3, nextRng3) = double(nextRng2)

    ((d1, d2, d3), nextRng3)
  }

  type State[S, +A] = S => (A, S)

  type Rand[+A] = State[RNG, A]

  def int: Rand[Int] = _.nextInt

  def ints(count: Int)(rng: RNG): (List[Int], RNG) = {

    @tailrec
    def _ints(c: Int, rng: RNG, acc: List[Int]): (List[Int], RNG) =
      if (c == 0) (acc, rng)
      else {
        val (a, nextRng) = nonNegativeInt(rng)
        _ints(c - 1, nextRng, a :: acc)
      }

    _ints(count, rng, List.empty[Int])
  }

  def unit[A](a: A): Rand[A] = rng => (a, rng)

  def map[A, B](s: Rand[A])(f: A => B): Rand[B] = rng => {
    val (a, nextRng) = s(rng)
    (f(a), nextRng)
  }

  def map2[A, B, C](ra: Rand[A], rb: Rand[B])(f: (A, B) => C): Rand[C] = rng => {
    val (a, rngA) = ra(rng)
    val (b, rngB) = rb(rngA)
    (f(a, b), rngB)
  }

  def sequence[A](fs: List[Rand[A]]): Rand[List[A]] = fs.foldRight(unit(List.empty[A]))((f, acc) => map2(f, acc)(_ :: _))

  def seq_ints(count: Int)(rng: RNG): (List[Int], RNG) = sequence(List.fill(count)(int))(rng)

  def _seq_ints(count: Int)(rng: RNG): Rand[List[Int]] = sequence(List.fill(count)(int))

  def flatMap[A, B](f: Rand[A])(g: A => Rand[B]): Rand[B] = rng => {
    val (a, rng2) = f(rng)
    g(a)(rng2)
  }

  def nonNegativeLessThan(n: Int): Rand[Int] = {
    flatMap(nonNegativeInt) { i =>
      val mod = i % n
      if (i + (n - 2) - mod >= 0) unit(mod) else nonNegativeLessThan(n: Int)
    }
  }

  def _map[A, B](s: Rand[A])(f: A => B): Rand[B] = flatMap(s)(a => unit(f(a)))

  def _map2[A, B, C](ra: Rand[A], rb: Rand[B])(f: (A, B) => C): Rand[C] =
    flatMap(ra)(a => map(rb)(b => f(a, b)))
}

object Candy {
  sealed trait Input
  case object Coin extends Input
  case object Turn extends Input

  case class Machine(locked: Boolean, candies: Int, coins: Int)

  import State._

  def update = (i: Input) => (s: Machine) =>
    (i, s) match {
      case (_, Machine(_, 0, _)) => s
      case (Coin, Machine(false, _, _)) => s
      case (Turn, Machine(true, _, _)) => s
      case (Coin, Machine(true, candy, coin)) =>
        Machine(false, candy, coin + 1)
      case (Turn, Machine(false, candy, coin)) =>
        Machine(true, candy - 1, coin)
    }

  def simulateMachine(inputs: List[Input]): State[Machine, (Int, Int)] = for {
    _ <- sequence(inputs map (modify[Machine] _ compose update))
    s <- get
  } yield (s.coins, s.candies)

  def badSimulateMachine(inputs: List[Input]): State[Machine, (Int, Int)] = State(machine => inputs match {
    case Nil => ((machine.coins, machine.candies), machine)
    case x :: xs => x match {
      case Coin if machine.candies > 0 => simulateMachine(inputs.tail).run(Machine(false, machine.candies, machine.coins + 1))
      case Turn if machine.candies > 0 => if (machine.locked) simulateMachine(inputs.tail).run(Machine(true, machine.candies, machine.coins)) else
        simulateMachine(inputs.tail).run(Machine(true, machine.candies - 1, machine.coins))
      case _ => simulateMachine(List.empty[Input]).run(Machine(machine.locked, machine.candies, machine.coins))
    }
  })
}

/*
import org.teckhooi._
import State._
Candy.simulateMachine(List(Coin, Turn, Coin, Turn, Coin, Turn, Coin, Turn)).run(Machine(true, 5, 10))
*/