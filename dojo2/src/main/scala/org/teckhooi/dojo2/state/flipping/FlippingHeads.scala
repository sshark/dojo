package org.teckhooi.dojo2.state.flipping

import org.json4s.JsonAST.JValue

import java.io.File
import scala.concurrent.Future
import scala.language.postfixOps
import scala.util.Try

/*
object FlippingHeads extends App {

  val randomAPI = "https://api.random.org/json-rpc/1/invoke"

  def randNumsReq(apiKey: String, n: Int, min: Int, max: Int, replacement: Boolean, id: Int): JObject =
    ("jsonrpc" -> "2.0") ~
      ("method" -> "generateIntegers") ~
      ("params" -> ("apiKey" -> apiKey) ~
        ("n" -> n) ~
        ("min" -> min) ~
        ("max" -> max) ~
        ("replacement" -> replacement)
        ) ~
      ("id" -> id)

  implicit val f = org.json4s.DefaultFormats

  import org.teckhooi.fp.MonadUtil._

  val node = Try(LocalFile(new File(getClass.getResource("/numbers.json").getPath)))
    .flatMap(localFile => JSONSourceLoaderUtil.jsonFrom[Try, DataSource[Try]](localFile)(JSONSourceLoaderUtil.extractFrom[Try]))

  /* An alternative OO solution
    val node = Try(LocalFile(new File(getClass.getResource("/numbers.json").getPath)))
      .flatMap(localFile => jsonFrom[Try, DataSource[Try]](localFile)(_.extract))
 */

  printResult(retrieveNums(node, 1, Seq.empty[Int]), "Bad local request")

  val reqJSON = randNumsReq("2b61a76d-846a-4169-8f6c-55b49adb7811", 2000, -1000, 999, false, 99)

  val remoteNode = Future(RemoteResource(randomAPI, reqJSON))
    .flatMap(remote => JSONSourceLoaderUtil.jsonFrom[Future, DataSource[Future]](remote)(JSONSourceLoaderUtil.extractFrom[Future]))

  /* An alternative OO solution
    val remoteNode = Future(RemoteResource(randomAPI, reqJSON))
      .flatMap(remote => jsonFrom[Future, DataSource[Future]](remote)(_.extract))
 */

  val numbers: Future[Seq[Int]] = retrieveNums(remoteNode, 5, Seq.empty[Int])

  Await.ready(numbers, 10 second).onComplete(result => printResult(result, "Bad remote request"))

  def retrieveNums[F[_]](loader: => F[JValue], segments: Int, acc: Seq[Int])(implicit ev: Monad[F]): F[Seq[Int]] = {
    if (segments <= 0) ev.point(acc)
    else {
      ev.bind(RetrieveRandomNumsUtil.loadNums(loader))(l => {
        retrieveNums(loader, segments - 1, l ++ acc)
      })
    }
  }

  def trackHeads(nums: Seq[Int], markers: Seq[Int]): Map[Int, Double] = {
    def _trackHeads(currNums: Seq[Int], i: Int, heads: Int = 0, currMarkers: Seq[Int], acc: Map[Int, Double]): Map[Int, Double] = {
      val (_markers, _acc) = records(i, heads, currMarkers, acc)
      currNums match {
        case Nil => acc + (nums.size -> (heads.toDouble / nums.length))
        case x :: xs if x > 0 => _trackHeads(xs, i + 1, heads + 1, _markers, _acc)
        case x :: xs => _trackHeads(xs, i + 1, heads, _markers, _acc)
      }
    }

    _trackHeads(nums, 0, 0, markers, Map.empty[Int, Double])
  }

  def records(i: Int, heads: Int, markers: Seq[Int], acc: Map[Int, Double]): (Seq[Int], Map[Int, Double]) = markers match {
    case Nil => (Nil, acc)
    case x :: xs if x == i => (xs, acc + (i -> (heads.toDouble / i)))
    case _ => (markers, acc)
  }

  private def printResult(result: Try[Seq[Int]], errMessage: String): Unit = result match {
    case Success(nums) => println(trackHeads(nums, List(10, 100, 1000, 10000)))
    case Failure(t) => println(s"$errMessage => $t")
  }
}
 */

trait DataSource[F[_]]
case class LocalFile(input: File) extends DataSource[Try]
case class RemoteResource(url: String, reqJSON: JValue)
  extends DataSource[Future]

/* An alternative OO solution. This solution does not require JSONSourceLoaderUtil.
trait DataSource[F[_]] {
  def extract: F[JValue]
}

case class LocalFile(input: File) extends DataSource[Try] {
  override def extract: Try[JValue] = Parser.parseFromFile(input)
}

case class RemoteResource(url: String, reqJSON: JValue) extends DataSource[Future] {
  override def extract: Future[JValue] = Future(Request.Post(url)
    .setHeader("Accept", "application/json")
    .bodyString(compact(render(reqJSON)), ContentType.APPLICATION_JSON)
    .connectTimeout(10000)
    .socketTimeout(10000)
    .execute().returnContent().asStream())
    .flatMap(input => Future.fromTry(Parser.parseFromChannel(Channels.newChannel(input))))

object JSONSourceLoaderUtil {

  import jawn.support.json4s.Parser

  def jsonFrom[F[_], S <: DataSource[F]](source: S)(f: S => F[JValue])(implicit ev: Monad[F]): F[JValue] = ev.bind(ev.point(source))(f)

  // this method definition is invalid for Lightbend Scala 2.11.8. It is fixed in
  // TypeLevel Scala 2.11.8 and Lightbend Scala 2.12.x
  def extractFrom[F[_]](source: DataSource[F]): F[JValue] = source match {
    case LocalFile(input) => Parser.parseFromFile(input)
    case RemoteResource(url, reqJSON) => Future(Request.Post(url)
      .setHeader("Accept", "application/json")
      .bodyString(compact(render(reqJSON)), ContentType.APPLICATION_JSON)
      .connectTimeout(10000)
      .socketTimeout(10000)
      .execute().returnContent().asStream())
      .flatMap(input => Future.fromTry(Parser.parseFromChannel(Channels.newChannel(input))))
  }
}
 */
