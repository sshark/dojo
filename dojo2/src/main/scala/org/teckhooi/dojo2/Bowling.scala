package org.teckhooi.dojo2

import scala.annotation.tailrec
import scala.util.{Failure, Success, Try}

object Bowling {
  implicit class BowlingFrame(scoreLine: String) {
    def score: Try[Int] = {
      @tailrec
      def _score(frames: Seq[Int], acc: Int = 0): Int = frames match {
        case 10 +: a +: b +: Nil               => _score(Nil, acc + 10 + a + b)
        case 10 +: a +: b +: xs                => _score(a +: b +: xs, acc + 10 + a + b)
        case a +: b +: c +: Nil if a + b == 10 => _score(Nil, acc + 10 + c)
        case a +: b +: xs if a + b == 10       => _score(xs, acc + 10 + xs.head)
        case a +: b +: xs                      => _score(xs, acc + a + b)
        case Nil                               => acc
      }

      buildFrames(scoreLine).map(_score(_))
    }

    private def buildFrames(frames: String): Try[Seq[Int]] = {
      val scores = frames.foldLeft(Seq.empty[Int]) {
        case (l, '/')                        => l :+ (10 - l.last)
        case (l, '-')                        => l :+ 0
        case (l, 'X')                        => l :+ 10
        case (l, c) if c.isDigit && c != '0' => l :+ c - 0x30
        case (l, _)                          => l :+ -1
      }

      if (scores.contains(-1))
        Failure(new IllegalArgumentException(s"${frames} contains invalid characters"))
      else Success(scores)
    }
  }
}
