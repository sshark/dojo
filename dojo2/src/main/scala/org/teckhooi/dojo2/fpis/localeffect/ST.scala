package org.teckhooi.dojo2.fpis.localeffect

import scala.collection.mutable

trait RunnableST[A] {
  def apply[S]: ST[S, A]
}

sealed trait ST[S, A] {
  self =>
  protected def run(s: S): (A, S)

  def map[B](f: A => B): ST[S, B] = new ST[S, B] {
    override def run(s: S): (B, S) = {
      val (a, s1) = self.run(s)
      (f(a), s1)
    }
  }

  def flatMap[B](f: A => ST[S, B]): ST[S, B] = new ST[S, B] {
    override def run(s: S): (B, S) = {
      val (a, s1) = self.run(s)
      f(a).run(s1)
    }
  }
}

object ST {
  def apply[S, A](a: => A): ST[S, A] = {
    lazy val memo = a
    new ST[S, A] {
      def run(s: S): (A, S) = (memo, s)
    }
  }

  def runST[A](st: RunnableST[A]): A = st.apply.run(())._1
}

sealed trait STRef[S, A] {
  protected var cell: A

  def read: ST[S, A] = ST(cell)

  def write(a: A): ST[S, Unit] = new ST[S, Unit] {
    override protected def run(s: S): (Unit, S) = {
      cell = a
      ((), s)
    }
  }
}
object STRef {
  def apply[S, A](a: A): ST[S, STRef[S, A]] = ST(new STRef[S, A] {
    var cell: A = a
  })
}

sealed abstract class STArray[S, A](implicit ev: Manifest[A]) {
  protected val value: Array[A]

  def size: ST[S, Int] = ST(value.length)

  def write(i: Int, v: A): ST[S, Unit] = new ST[S, Unit] {
    override protected def run(s: S): (Unit, S) = {
      value(i) = v
      ((), s)
    }
  }

  def read(i: Int): ST[S, A] = ST(value(i))

  def freeze: ST[S, List[A]] = ST(value.toList)

  def fill(xs: Map[Int, A]): ST[S, Unit] = new ST[S, Unit] {
    override protected def run(s: S): (Unit, S) = {
      xs.filter { case (ndx, v) => value.length > ndx }.foreach { case (ndx, v) => value(ndx) = v }
      ((), s)
    }
  }

  def fill2(xs: Map[Int, A]): ST[S, Unit] =
    xs.filter { case (ndx, v) => value.length > ndx }.foldRight(ST[S, Unit](())) {
      case ((ndx, v), s) =>
        s.flatMap(_ => write(ndx, v))
    }

  def swap(i: Int, j: Int): ST[S, Unit] = for {
    x <- read(i)
    y <- read(j)
    _ <- write(i, y)
    _ <- write(j, x)
  } yield ()
}

sealed abstract class STMap[S, K, V](implicit ev: Manifest[V]) {
  protected val value: mutable.Map[K, V]

  def size: ST[S, Int] = ST(value.size)

  def write(k: K, v: V): ST[S, Unit] = new ST[S, Unit] {
    override protected def run(s: S): (Unit, S) = {
      value(k) = v
      ((), s)
    }
  }

  def read(k: K): ST[S, V] = ST(value(k))

  def read(k: K, default: V): ST[S, V] = ST(value.get(k).fold({
    value(k) = default
    default
  })(identity))

  def freeze: ST[S, Map[K, V]] = ST(value.toMap)
}

object STArray {
  def apply[S, A: Manifest](sz: Int, a: A): ST[S, STArray[S, A]] = ST(new STArray[S, A] {
    override protected val value: Array[A] = Array.fill(sz)(a)
  })

  def fromList[S, A: Manifest](xs: List[A]): ST[S, STArray[S, A]] = ST(new STArray[S, A]() {
    override protected val value: Array[A] = xs.toArray
  })
}

object STMap {
  def apply[S, K, V: Manifest]: ST[S, STMap[S, K, V]] = ST(new STMap[S, K, V] {
    override protected val value: mutable.Map[K, V] = mutable.Map.empty
  })

  def fromMap[S, K, V: Manifest](xs: Map[K, V]): ST[S, STMap[S, K, V]] = ST(new STMap[S, K, V]() {
    override protected val value: mutable.Map[K, V] = mutable.Map.from(xs)
  })
}

object Quicksort {
  def partition[S](arr: STArray[S, Int], l: Int, r: Int, pivot: Int): ST[S, Int] =
    for {
      p_num <- arr.read(pivot)
      _ <- arr.swap(r, pivot)
      j <- STRef(l)
      _ <- (l until r).foldLeft(ST[S, Unit](()))((s, i) =>
        for {
          _ <- s // it forwards the state although it looks like NOP
          iv <- arr.read(i)
          _ <-
            if (iv < p_num) {
              for {
                jv <- j.read
                _ <- arr.swap(i, jv)
                _ <- j.write(jv + 1)
              } yield ()
            } else ST[S, Unit](())
        } yield ()
      )
      new_pv <- j.read
      _ <- arr.swap(new_pv, r)
    } yield new_pv

  def qs[S](arr: STArray[S, Int], l: Int, r: Int): ST[S, Unit] = if (l < r)
    for {
      pivot <- partition(arr, l, r, l + (r - l) / 2)
      _ <- qs(arr, l, pivot - 1)
      _ <- qs(arr, pivot + 1, r)
    } yield ()
  else ST(())

  def sort(xs: List[Int]): List[Int] = if (xs.isEmpty) xs
  else
    ST.runST(new RunnableST[List[Int]] {
      override def apply[S]: ST[S, List[Int]] =
        for {
          arr <- STArray.fromList(xs)
          size <- arr.size
          _ <- qs(arr, 0, size - 1)
          sorted <- arr.freeze
        } yield sorted
    })
}
