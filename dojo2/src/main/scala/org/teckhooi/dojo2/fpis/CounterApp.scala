package org.teckhooi.dojo2.fpis

import cats.Functor
import cats.effect.kernel.Ref
import cats.effect.{ExitCode, IO, IOApp}
import cats.syntax.functor._

trait Counter[F[_]] {
  def inc: F[Unit]

  def get: F[Int]
}

object Counter {
  def make[F[_] : Functor : Ref.Make]: F[Counter[F]] =
    Ref.of[F, Int](0).map { ref =>
      new Counter[F] {
        override def inc: F[Unit] = ref.update(_ + 1)

        override def get: F[Int] = ref.get
      }
    }
}

object CounterApp extends IOApp {
  override def run(args: List[String]): IO[ExitCode] = {
    def program(c: Counter[IO]): IO[ExitCode] = {
      for {
        _ <- c.get.flatMap(IO.println)
        _ <- c.inc
        _ <- c.get.flatMap(IO.println)
        _ <- c.inc.replicateA_(5)
        _ <- c.get.flatMap(IO.println)
      } yield ExitCode.Success

    }

    Counter.make[IO].flatMap(program)
  }
}
