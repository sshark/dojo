package org.teckhooi.dojo2.fpis.iostream

sealed trait Process[I, O] {

  import Process._

  def apply(s: LazyList[I]): LazyList[O] = this match {
    case Halt() => LazyList()
    case Emit(head, tail) =>
      log(s"Emit at apply, ${this.getClass}")
      head #:: tail.apply(s)
    case Await(recv) =>
      log("Await at apply")
      s match {
        case h #:: t => recv(Some(h))(t)
        case xs => recv(None)(xs)
      }
  }

  def repeat: Process[I, O] = {
    def go(p: Process[I, O]): Process[I, O] = p match {
      case Halt() =>
        log(s"Halt at repeat, ${this.getClass}")
        go(this)
      case Emit(head, tail) =>
        log(s"Emit at repeat, ${tail.getClass}, this: ${this.getClass}")
        Emit(head, go(tail))
      case Await(recv) =>
        log(s"Await at repeat, ${this.getClass}")
        Await {
          case None =>
            log("None at repeat")
            recv(None)
          case x => go(recv(x))
        }
    }

    go(this)
  }

  def |>[O2](p2: Process[O, O2]): Process[I, O2] = p2 match {
    case Halt() => Halt()
    case Emit(head, tail) => Emit(head, this |> tail)
    case Await(r) =>
      this match {
        case Halt() => this |> r(None)
        case Emit(head, tail) => tail |> r(Some(head))
        case Await(n) => Await((i: Option[I]) => n(i) |> p2)
      }
  }

  def map[O2](f: O => O2): Process[I, O2] = this |> lift(f)

  def ++[O2](p2: Process[I, O]): Process[I, O] = this match {
    case Halt() => p2
    case Emit(head, tail) => Emit(head, tail ++ p2)
    case Await(r) => Await(r.andThen(_ ++ p2))
  }
}

object Process {
  val debug = false

  def log(s: String): Unit = if (debug) println(s)

  case class Emit[I, O](head: O, tail: Process[I, O] = Halt[I, O]()) extends Process[I, O]
  case class Await[I, O](recv: Option[I] => Process[I, O]) extends Process[I, O]
  case class Halt[I, O]() extends Process[I, O]

  def liftOne[I, O](f: I => O): Process[I, O] =
    Await {
      case Some(x) => Emit(f(x))
      case _ => Halt()
    }

  def lift[I, O](f: I => O): Process[I, O] = liftOne(f).repeat

  def take[I](n: Int): Process[I, I] = {
    def go(i: Int): Process[I, I] = if (i < n) Await {
      case Some(d) => Emit(d, go(i + 1))
      case _ => Halt()
    }
    else Halt()

    go(0)
  }

  def drop[I](n: Int): Process[I, I] = {
    def go(i: Int): Process[I, I] = if (i < n) Await { _ =>
      Process.log(s"Dropping index $i < $n")
      go(i + 1)
    }
    else {
      Await {
        case Some(d) => Emit(d, go(i + 1))
        case _ => Halt()
      }
    }

    go(0)
  }

  def takeWhile[I](f: I => Boolean): Process[I, I] = {
    def go(check: Boolean): Process[I, I] =
      if (check) Await[I, I] {
        case Some(d) => if (f(d)) Emit(d, go(true)) else go(false)
        case _ =>
          Process.log("takeWhile None")
          Halt()
      }
      else
        Await[I, I] { case _ =>
          Process.log("takeWhile None")
          Halt()
        }

    go(true)
  }

  def dropWhile[I](f: I => Boolean): Process[I, I] = {
    def go(check: Boolean): Process[I, I] =
      if (check) Await[I, I] {
        case Some(d) => if (f(d)) Halt[I, I]() else Emit(d, go(false))
        case _ =>
          Process.log("dropWhile None")
          Halt()
      }.repeat
      else
        Await[I, I] {
          case Some(d) => Emit(d)
          case _ =>
            Process.log("dropWhile None")
            Halt()
        }.repeat

    go(true)
  }

  def await[I, O](f: I => Process[I, O], fallback: Process[I, O] = Halt[I, O]()): Process[I, O] =
    Await[I, O] {
      case Some(i) => f(i)
      case None => fallback
    }

  def loop[S, I, O](z: S)(f: (I, S) => (O, S)): Process[I, O] = await { (i: I) =>
    f(i, z) match {
      case (o, s2) => Emit(o, loop(s2)(f))
    }
  }

  def sum: Process[Int, Int] = loop(0)((i, s) => (i + s, i + s))

  def count[I]: Process[I, Int] = {
    def go(i: Int): Process[I, Int] = Await {
      case Some(_) => Emit(i, go(i + 1))
      case _ => Halt()
    }

    go(1)
  }

  def count_[I]: Process[I, Int] = loop(0)((_, s) => (s + 1, s + 1))

  def mean: Process[Double, Double] = {
    def go(sum: Double, i: Int): Process[Double, Double] = Await {
      case Some(d) => Emit((sum + d) / i, go(d + sum, i + 1))
      case _ => Halt()
    }

    go(0d, 1)
  }

  def filter[I](f: I => Boolean): Process[I, I] = Await {
    case Some(d) => if (f(d)) Emit(d) else Halt()
    case _ => Halt()
  }
}

object RunProcess extends App {

  import Process._

  val i = lift((i: Int) => i * 2)(LazyList(1, 2, 3)).toList
  assert(i == List(2, 4, 6))

  private val xs = LazyList(1, 2, 3, 4, 5, 6, 1, 8, 9, 10)

  assert(take(3)(LazyList.from(0)).toList == List(0, 1, 2))
  assert(drop(3)(xs).toList == xs.drop(3).toList)

  private val takeWhileList = takeWhile((_: Int) < 5)(xs).toList
  private val dropWhileList = dropWhile((_: Int) < 5)(xs).toList

  assert(xs.toList.takeWhile(_ < 5) == takeWhileList)
  assert(xs.toList.dropWhile(_ < 5) == dropWhileList)
  assert(count(xs).toList.last == xs.length) // 10

  private val ys = LazyList(1d, 2d, 1d, 4d)
  assert(mean(ys) == List(1.0, 1.5, 1.3333333333333333, 2.0))

  assert(sum(LazyList.from(1).take(10)).last == 55)
  assert(count_(LazyList.from(1).take(100)).last == 100)

  private val evenPlus1: Process[Int, Int] = filter((_: Int) % 2 == 0).repeat |> lift((_: Int) + 1)
  assert(evenPlus1(LazyList.range(1, 11)).toList == List(3, 5, 7, 9, 11))
}
