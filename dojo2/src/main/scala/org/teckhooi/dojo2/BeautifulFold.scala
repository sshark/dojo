package org.teckhooi.dojo2

import cats.{Functor, Monoid}

trait Fold[I, O] {
  type M

  def m: Monoid[M]

  def tally: I => M

  def summarize: M => O
}

object Fold {
  def apply[I, O, _M](_m: Monoid[_M])(_tally: I => _M, _summarize: _M => O): Fold[I, O] = new Fold[I, O] {
    override type M = _M

    override def m = _m

    override def tally = _tally

    override def summarize = _summarize
  }
}

object BeautifulFold extends App {
  def fold[I, O](input: Seq[I])(f: Fold[I, O]): O =
    f.summarize(input.foldLeft(f.m.empty)((x, y) => f.m.combine(x, f.tally(y))))

  import cats.implicits._

  def sum[M](implicit ev: Monoid[M]): Fold[M, M] = Fold.apply(ev)(identity, identity)

  fold(List(1, 2, 3))(sum)

  implicit def foldFunctor[I]: Functor[Lambda[X => Fold[I, X]]] = new Functor[Lambda[X => Fold[I, X]]] {
    override def map[A, B](fa: Fold[I, A])(f: A => B): Fold[I, B] =
      Fold(fa.m)(fa.tally, fa.summarize.andThen(f))
  }

  fold(List(1, 2, 3))(sum[Int].map(s => s"Sum is $s"))
}

