package org.teckhooi.dojo2

import zio.{Console, Ref, Scope, UIO, ZIO, ZIOAppArgs, ZIOAppDefault}

object TimedZIOConsole extends ZIOAppDefault {

  val makeRef: UIO[Ref[Int]] = Ref.make(0)

  val makeRef1 = makeRef
  val makeRef2 = makeRef
  override def run: ZIO[Any with ZIOAppArgs with Scope, Any, Any] = for {
    ref1 <- makeRef1
    ref2 <- makeRef2
    _    <- ref1.update(_ + 1)
    _    <- ref2.update(_ + 1)
    r1   <- ref1.get
    r2   <- ref2.get
    _    <- Console.printLine(r1, r2)
  } yield ()

  /*  override def run: ZIO[Any with ZIOAppArgs with Scope, Any, Any] = for {
    _     <- Console.printLine("What's your name?")
    fiber <- Console.readLine.timeout(3.second).fork
    message <- fiber.join.mapAttempt {
      case Some(name) => s"Hello, $name!"
      case _          => "Timeout. You didn't enter anything in time"
    }
    _ <- Console.printLine(message)
  } yield ()
   */
}
