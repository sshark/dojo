package org.teckhooi.dojo2

import scala.math.Ordering.Implicits.infixOrderingOps

object MergeSort {

  def sort[A: Ordering](a: Seq[A]): Seq[A] = {
    def split(l: Seq[A], r: Seq[A]): Seq[A] = (l, r) match {
      case (xs, Nil)        => xs
      case (Nil, xs)        => xs
      case (Seq(x), Seq(y)) => if (x > y) Seq(y, x) else Seq(x, y)
      case (xs, ys) =>
        merge(
          (split _).tupled(xs.splitAt(xs.length / 2)),
          (split _).tupled(ys.splitAt(ys.length / 2))
        )

      /*
                Alternate version of the line above,

                val firstPair = xs.splitAt(xs.length / 2)
                val secondPair = ys.splitAt(ys.length / 2)
                mergeSort(sort(firstPair._1, firstPair._2), sort(secondPair._1, secondPair._2))

                Compilation fails when sort is converted to a function via (sort _) to apply tupled(...)
                because the result of the conversion would be Function2[Nothing, Nothing, Nothing] which is pretty much
                useless.

                For completeness sake, to properly convert sort(...) method to a function would be,

                  val f = (l:Seq[Int]) => MergeSort.sort(l)
       */
    }

    (split _).tupled(a.splitAt(a.length / 2))
  }

  def merge[A: Ordering](a: Seq[A], b: Seq[A]): Seq[A] = (a, b) match {
    case (Nil, xs) => xs
    case (xs, Nil) => xs
    case (x +: xs, y +: ys) =>
      if (x > y) y +: merge(a, ys) else x +: merge(xs, b)
  }
}
