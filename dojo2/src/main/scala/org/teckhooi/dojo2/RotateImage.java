package org.teckhooi.dojo2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RotateImage {
    public static void main(String[] args) {
        int[][] image = new int[][]{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        rotate(image);
        List<String> arrString = new ArrayList<>();
        for (int[] row : image) {
            arrString.add(Arrays.toString(row));
        }
        System.out.printf("[%s]", String.join("\n", arrString));

    }

    static void rotate(int[][] image) {
        int size = image.length;

        // todo
        // step 1 transpose
        // step 2 horizontal swap
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                int temp = image[i][j];
                image[i][j] = image[j][size - i - 1];
                image[j][size - i - 1] = temp;
            }
        }
    }
}
