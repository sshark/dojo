package org.teckhooi.dojo2.poker

import org.teckhooi.dojo2.poker.PokerHand._
import org.teckhooi.dojo2.poker.Ranks.RankOrdering

import scala.math.Ordering.Implicits._

object Game {
  def duel(game: Game): Result = duel(game.firstPlayer, game.secondPlayer)

  def duel(first: Player, second: Player): Result = checks(first, second)

  val straightFlushCheck: PartialFunction[(Player, Player), Result] = {
    case (playerOne@Player(nameOne, handOne), playerTwo@Player(nameTwo, handTwo))
      if handOne.rank == straightFlushRanking && (handOne.rank == handTwo.rank) =>
      higherCards(playerOne, playerTwo, "with high card:")
  }

  val fourOfAKindCheck: PartialFunction[(Player, Player), Result] = {
    case (playerOne@Player(nameOne, handOne), playerTwo@Player(nameTwo, handTwo))
      if handOne.rank == fourOfAKindRanking && (handOne.rank == handTwo.rank) =>
      higherCards(playerOne, playerTwo, "with high card:")
  }

  val fullHouseCheck: PartialFunction[(Player, Player), Result] = {
    case (playerOne@Player(nameOne, handOne), playerTwo@Player(nameTwo, handTwo))
      if handOne.rank == fullHouseRanking && (handOne.rank == handTwo.rank) =>
      higherCards(playerOne, playerTwo, "with high card:")
  }

  val flushCheck: PartialFunction[(Player, Player), Result] = {
    case (playerOne@Player(nameOne, handOne), playerTwo@Player(nameTwo, handTwo))
      if handOne.rank == flushRanking && (handOne.rank == handTwo.rank) =>
      higherCards(playerOne, playerTwo, "with high card:")
  }

  val straightCheck: PartialFunction[(Player, Player), Result] = {
    case (playerOne@Player(nameOne, handOne), playerTwo@Player(nameTwo, handTwo))
      if handOne.rank == straightRanking && (handOne.rank == handTwo.rank) =>
      higherCards(playerOne, playerTwo, "with high card:")
  }

  val threeOfAKindCheck: PartialFunction[(Player, Player), Result] = {
    case (playerOne@Player(nameOne, handOne), playerTwo@Player(nameTwo, handTwo))
      if handOne.rank == threeOfAKindRanking && (handOne.rank == handTwo.rank) =>
      higherCards(playerOne, playerTwo, "with high card:")
  }

  val twoPairsCheck: PartialFunction[(Player, Player), Result] = {
    case (playerOne@Player(nameOne, handOne), playerTwo@Player(nameTwo, handTwo))
      if handOne.rank == twoPairsRanking && (handOne.rank == handTwo.rank) =>
      higherCards(playerOne, playerTwo, "with high card:")
  }

  val onePairCheck: PartialFunction[(Player, Player), Result] = {
    case (playerOne@Player(nameOne, handOne), playerTwo@Player(nameTwo, handTwo))
      if handOne.rank == onePairRanking && (handOne.rank == handTwo.rank) =>
      higherCards(playerOne, playerTwo, "with high card:")
  }

  val highCardCheck: PartialFunction[(Player, Player), Result] = {
    case (playerOne@Player(nameOne, handOne), playerTwo@Player(nameTwo, handTwo))
      if handOne.rank == highCardRanking && (handOne.rank == handTwo.rank) =>
      higherCards(playerOne, playerTwo, "with high card:")
  }

  val pokerHandsCheck: PartialFunction[(Player, Player), Result] = {
    case (playerOne@Player(nameOne, handOne), playerTwo@Player(nameTwo, handTwo)) if handOne.rank != handTwo.rank =>
      if (handOne.rank > handTwo.rank) Win(playerOne, s"with ${handOne.getClass.getSimpleName}")
      else Win(playerTwo, s"with ${handTwo.getClass.getSimpleName}")
  }

  private def higherCards(playerOne: Player, playerTwo: Player, message: String): Result =
    (playerOne.hand.highRanks zip playerTwo.hand.highRanks collectFirst {
      case (firstCard, secondCard) if firstCard != secondCard =>
        if (firstCard > secondCard) Win(playerOne, s"$message ${firstCard.name}")
        else Win(playerTwo, s"$message ${secondCard.name}")
    }).getOrElse(Tie)

  val checks = highCardCheck orElse
    fourOfAKindCheck orElse
    straightFlushCheck orElse
    threeOfAKindCheck orElse
    onePairCheck orElse
    twoPairsCheck orElse
    straightCheck orElse
    flushCheck orElse
    pokerHandsCheck

  val gameRe = "(\\w+?): *(\\w{2,3} *\\w{2,3} *\\w{2,3} *\\w{2,3} *\\w{2,3})".r

  def apply(gameString: String): Game = {
    val players = gameRe.findAllMatchIn(gameString).map {
      case gameRe(player, hand) => Player(player, PokerHand(hand))
    }.toList

    if (players.size == 2) Game(players.head, players.last)
    else throw new IllegalArgumentException("This game allows exactly 2 players.")
  }
}

case class Game(firstPlayer: Player, secondPlayer: Player)

case class Player(name: String, hand: PokerHand)

sealed trait Result
object Tie extends Result {
  override def toString = "Tie"
}

case class Win(val winner: Player, val notes: String) extends Result {
  override def toString = s"${winner.name} wins. - $notes"
}
