package org.teckhooi.dojo2.poker

import scala.util.matching.Regex

sealed trait Suit {
  val name: String
}

object Diamond extends Suit {
  override def toString: String = "D"

  override val name = "Diamond"
}

object Club extends Suit {
  override def toString: String = "C"

  override val name = "Club"
}

object Spade extends Suit {
  override def toString: String = "S"

  override val name = "Spade"
}

object Heart extends Suit {
  override def toString: String = "H"

  override val name = "Heart"
}

sealed trait Rank {
  val pos: Int
  val name: String = toString
}

object LowerAce extends Rank {
  override def toString: String = "1"

  override val pos = 1
}

object Two extends Rank {
  override def toString: String = "2"

  override val pos = 2
}

object Three extends Rank {
  override def toString: String = "3"

  override val pos = 3
}

object Four extends Rank {
  override def toString: String = "4"

  override val pos = 4
}

object Five extends Rank {
  override def toString: String = "5"

  override val pos = 5
}

object Six extends Rank {
  override def toString: String = "6"

  override val pos = 6
}

object Seven extends Rank {
  override def toString: String = "7"

  override val pos = 7
}

object Eight extends Rank {
  override def toString: String = "8"

  override val pos = 8
}

object Nine extends Rank {
  override def toString: String = "9"

  override val pos = 9
}

object Ten extends Rank {
  override def toString: String = "10"

  override val pos = 10
}

object Jack extends Rank {
  override val pos = 11
  override val name = "Jack"

  override def toString: String = "J"
}

object Queen extends Rank {
  override val pos = 12
  override val name = "Queen"

  override def toString: String = "Q"
}

object King extends Rank {
  override val pos = 13
  override val name = "King"

  override def toString: String = "K"
}

object Ace extends Rank {
  override val pos = 14
  override val name = "Ace"

  override def toString: String = "A"
}

object Ranks {
  implicit class RichRank(r: Rank) extends Rank {
    def of(s: Suit): Card = new Card(r, s)

    override val pos = r.pos
  }

  implicit object RankOrdering extends Ordering[Rank] {
    override def compare(x: Rank, y: Rank) = x.pos compare y.pos
  }
}

object Cards {
  type Cards = List[Card]

  implicit object CardOrdering extends Ordering[Card] {
    def compare(x: Card, y: Card) = x.rank.pos compare y.rank.pos
  }

  def apply(cardsRaw: String): Cards = {
    val cards = cardsRaw.split(" +").map(Card.apply).toList.sorted.reverse
    if (cards.distinct.size == 5) cards
    else throw new IllegalArgumentException("Each player must be dealt exactly 5 cards with no duplicates.")
  }
}

object Card {
  val toRank: Map[String, Rank] = Map(
    Two.toString -> Two,
    Three.toString -> Three,
    Four.toString -> Four,
    Five.toString -> Five,
    Six.toString -> Six,
    Seven.toString -> Seven,
    Eight.toString -> Eight,
    Nine.toString -> Nine,
    Ten.toString -> Ten,
    Jack.toString -> Jack,
    Queen.toString -> Queen,
    King.toString -> King,
    Ace.toString -> Ace
  )

  val toSuit: Map[String, Suit] = Map(
    Spade.toString -> Spade,
    Club.toString -> Club,
    Diamond.toString -> Diamond,
    Heart.toString -> Heart
  )

  val re: Regex = "(10|[AKQJ(2-9)])([HCDS])".r

  def apply(s: String): Card = {
    val re(rank, suit) = s
    Card(toRank(rank), toSuit(suit))
  }
}

case class Card(rank: Rank, suit: Suit) {
  override def toString: String = rank.toString + suit.toString
}
