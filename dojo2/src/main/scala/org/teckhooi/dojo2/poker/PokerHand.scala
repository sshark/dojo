package org.teckhooi.dojo2.poker

import org.teckhooi.dojo2.poker.Cards.{Cards, _}
import org.teckhooi.dojo2.poker.Ranks.RankOrdering

import scala.annotation.tailrec

object PokerHand {
  type Ranks = List[Rank]

  val straightFlushRanking = 9
  val fourOfAKindRanking = 8
  val fullHouseRanking = 7
  val flushRanking = 6
  val straightRanking = 5
  val threeOfAKindRanking = 4
  val twoPairsRanking = 3
  val onePairRanking = 2
  val highCardRanking = 1

  sealed class PokerHand(val highRanks: Ranks, val rank: Int)
  case class StraightFlush(override val highRanks: Ranks) extends PokerHand(highRanks, straightFlushRanking)
  case class FourOfAKind(override val highRanks: Ranks) extends PokerHand(highRanks, fourOfAKindRanking)
  case class FullHouse(override val highRanks: Ranks) extends PokerHand(highRanks, fullHouseRanking)
  case class Flush(override val highRanks: Ranks) extends PokerHand(highRanks, flushRanking)
  case class Straight(override val highRanks: Ranks) extends PokerHand(highRanks, straightRanking)
  case class ThreeOfAKind(override val highRanks: Ranks) extends PokerHand(highRanks, threeOfAKindRanking)
  case class TwoPairs(override val highRanks: Ranks) extends PokerHand(highRanks, twoPairsRanking)
  case class OnePair(override val highRanks: Ranks) extends PokerHand(highRanks, onePairRanking)
  case class HighCard(override val highRanks: Ranks) extends PokerHand(highRanks, highCardRanking)

  object StraightFlushMatcher {
    def unapply(cards: Cards) =
      if (sameSuit(cards) && isDescendingOrder(cards)) {
        if (cards.count(c => c.rank == Ace || c.rank == Two) == 2) Some(StraightFlush(List(Five)))
        else Some(StraightFlush(List(cards.max.rank)))
      } else {
        None
      }
  }

  object FourOfAKindMatcher {
    def unapply(cards: Cards) = cards.groupBy(_.rank).values.filter(_.size == 4).flatten match {
      case Nil => None
      case x :: xs => Some(FourOfAKind(List(x.rank)))
    }
  }

  object FullHouseMatcher {
    def unapply(cards: Cards) = {
      val groups = cards.groupBy(_.rank)
      val minor = groups.collectFirst{ case (_, cards) if cards.size == 2 => cards}
      val major = groups.collectFirst{ case (_, cards) if cards.size == 3 => cards}

      for {
        twos <- minor
        threes <- major
      } yield FullHouse(List(threes.head.rank))
    }
  }

  object FlushMatcher {
    def unapply(cards: Cards) = if (sameSuit(cards)) Some(Flush(cards.map(_.rank).sorted)) else None
  }

  object StraightMatcher {
    def unapply(cards: Cards) = if (isDescendingOrder(cards)) {
      if (cards.count(c => c.rank == Ace || c.rank == Two) == 2) Some(Straight(List(Five)))
      else Some(Straight(List(cards.max.rank)))
    } else {
      None
    }
  }

  object ThreeOfAKindMatcher{
    def unapply(cards: Cards) = cards.groupBy(_.rank).values.filter(_.size == 3).flatten match {
      case Nil => None
      case x :: xs => Some(ThreeOfAKind(List(x.rank)))
    }
  }

  object TwoPairsMatcher {
    def unapply(cards: Cards) = {
      val groups = cards.groupBy(_.rank)
      val pairs = groups.collect { case (_, cards) if cards.size == 2 => cards}.toList
      if (pairs.size == 2) {
        val sortedPairs = pairs.sortBy(_.head)
        Some(TwoPairs(List(sortedPairs.head.head.rank, sortedPairs.last.head.rank)))
      } else None
    }
  }

  object OnePairMatcher {
    def unapply(cards: Cards) = {
      val groups = cards.groupBy(_.rank)
      if (groups.size == 4) {
        groups.collect { case (_, cards) if cards.size == 2 => cards} match {
          case List(x) => Some(OnePair(List(x.head.rank)))
          case _ => None
        }
      } else None
    }
  }

  object HighCardMatcher {
    def unapply(cards: Cards) = Some(HighCard(cards.map(_.rank)))
  }

  def apply(cardsRaw: String): PokerHand = apply(Cards.apply(cardsRaw))

  def apply(cards: Cards): PokerHand = cards match {
    case StraightFlushMatcher(hand) => hand
    case FourOfAKindMatcher(hand) => hand
    case FullHouseMatcher(hand) => hand
    case FlushMatcher(hand) => hand
    case StraightMatcher(hand) => hand
    case ThreeOfAKindMatcher(hand) => hand
    case TwoPairsMatcher(hand) => hand
    case OnePairMatcher(hand) => hand
    case HighCardMatcher(hand) => hand
  }

  private def sameSuit(cards: Cards): Boolean = cards.tail.forall(_.suit == cards.head.suit)

  private def isDescendingOrder(cards: Cards): Boolean = {
    @tailrec
    def isDescendingOrder(head: Rank, tail: Seq[Rank], acc: Boolean): Boolean = tail match {
      case Nil => acc
      case x :: xs => if (head.pos == x.pos + 1) isDescendingOrder(x, xs, true)
        else isDescendingOrder(x, Nil, false)
    }
    val justRanks = cards.map(_.rank)
    isDescendingOrder(justRanks.head, justRanks.tail, false) ||
      (justRanks.count(_ == Ace) == 1 && isDescendingOrder(justRanks.tail.head, justRanks.tail.tail :+ LowerAce, false))
  }
}
