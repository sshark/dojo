package org.teckhooi.dojo2

object Permutation {
  def rotate[A](s: List[A]): List[List[A]] = {
    def rotate_(s1: List[A], acc: List[List[A]], count: Int): List[List[A]] =
      if (count == 0) acc
      else {
        val xs = s1.tail :+ s1.head
        rotate_(xs, xs :: acc, count - 1)
      }

    if (s.isEmpty) Nil else rotate_(s, List.empty, s.length)
  }

  def permutation[A](xs: List[A]): List[List[A]] = {
    def _permutation[A](ys: List[A]): List[List[A]] =
      if (ys.length == 1) List(ys)
      else rotate(ys).flatMap(zs => permutation(zs.tail).map(zs.head +: _))

    _permutation(xs).distinct
  }
}
