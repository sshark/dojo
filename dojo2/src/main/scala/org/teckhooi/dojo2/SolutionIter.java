package org.teckhooi.dojo2;


public class SolutionIter {
    public static void main(String[] args) {
//        System.out.println("\nEx1: " + solution(102));
        System.out.println("\nEx1: " + solution(0b1100110001));
//        System.out.println("\nEx2: " + solution(1651));
//        System.out.println("\nEx2: " + getBinaryPeriodForInt(955));
    }

    static int getBinaryPeriodForInt(int n) {
        int[] d = new int[30];
        int l = 0;
        while (n > 0) {
            d[l] = n % 2;
            n /= 2;
            l++;
        }

        for (int p = 1; p < l; p++) {
            if (p <= l / 2) {
                boolean ok = true;
                for (int i = 0; i < l - p; i++) {
                    if (d[i] != d[i + p]) {
                        ok = false;
                        break;
                    }
                }
                if (ok) {
                    return p;
                }
            }
        }

        return -1;
    }

    static int solution(int n) {
        int[] d = new int[30];
        int l = 0;
        int p;
        while (n > 0) {
            d[l] = n % 2;
            n /= 2;
            l++;
        }
        for (p = 1; p < 1 + l; ++p) {
            int i;
            boolean ok = true;
            for (i = 0; i < l - p; ++i) {
                if (d[i] != d[i + p]) {
                    ok = false;
                    break;
                }
            }
            if (ok) {
                return p;
            }
        }
        return -1;
    }
}
