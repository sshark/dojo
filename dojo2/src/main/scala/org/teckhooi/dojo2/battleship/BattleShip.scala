package org.teckhooi.dojo2.battleship

sealed trait Result
case object Hit extends Result
case object Sink extends Result
case object Miss extends Result

case class Point(y: Int, x: Char)
case class Ship(tl: Point, br: Point)

object BattleShip {
  def solution(n: Int, ship: String, target: String): String = {

    val hits = target.split(" ").map(h => matchHit(h)).toSet

    val rawShips = matchShip(ship).toList

    // match the number of coordinates given??

    val (sink, hit) = rawShips.foldLeft((0, 0))((result, s) => evalDamage(s, hits) match {
      case Hit => (result._1, result._2 + 1)
      case Sink => (result._1 + 1, result._2)
      case Miss => result
    })
    s"$sink,$hit"
  }

  val hitRE = "(\\d*)([A-Z])".r
  val shipRE = "((\\d*)([A-Z]) (\\d*)([A-Z]))*".r

  def matchShip(s: String) = shipRE.findAllIn(s).matchData.filter(_.group(0).nonEmpty)
    .map(m => Ship(Point(m.group(2).toInt, m.group(3)(0)),
      Point(m.group(4).toInt, m.group(5)(0))))

  def matchHit(s: String): String = {
    val hitRE(a, b) = s
    a + b
  }

  def evalDamage(ship: Ship, hits: Set[String]): Result = {
    val shipSpaces = buildHitPoint(ship)
    shipSpaces.intersect(hits).size match {
      case 0 => Miss
      case x if x == shipSpaces.size => Sink
      case _ => Hit
    }
  }

  def buildHitPoint(ship: Ship): Set[String] = {
    import ship._

    def _build(row: Int, col: Char, endRow: Int, endCol: Char, acc: Set[String]): Set[String] = (row, col) match {
      case (r, c) if endRow == r && endCol == c => acc + (r.toString + c.toString)
      case (r, c) if endRow == r => _build(tl.y, (c + 1).toChar, endRow, endCol, acc + (r.toString + c.toString))
      case (r, c) => _build((r + 1).toChar, c, endRow, endCol, acc + (r.toString + c.toString))
    }

    _build(tl.y, tl.x, br.y, br.x, Set.empty[String])
  }
}
