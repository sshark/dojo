package org.teckhooi.dojo2.dynamic

import scala.annotation.tailrec
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}
import scala.math._

/**
  * CoinsChangeIntegral vs CoinsChangeInt: CoinsChangeInt uses Int, whereas CoinsChangeIntegral uses typeclass
  * Integral. Therefore, CoinsChangeIntegral is flexible to accept any classes that implement Integral typeclass.
  * CoinsChangeIntegral is 2x-3x slower (unless @specialize is used) compare to CoinsChangeInt to complete the
  * tasks.
  *
  * Update #1: Type specialization, @specialize[Int, Long], generates specialized generics for Int and Long to
  * speed up the execution by avoiding unnecessary objects instantiation. Refer to,
  *
  * 1. https://scalac.io/specialized-generics-object-instantiation/
  * 2. https://stackoverflow.com/questions/47205057/why-cant-i-specialize-a-generic-function-in-scala
  */
object CoinsChangeIntegral {
  def main(args: Array[String]): Unit = {
    val results = Await.result(
      Future.sequence(
        List(
          Future(pick(change(List(1, 2, 5))(11)).size == 3),
          Future(pick(change(List(10, 5))(15)).size == 2),
          Future(pick(change(List(2))(3)).isEmpty),
          Future(pick(change(List(1, 2, 3, 21))(63)).size == 3),
          Future(pick(change(List(2))(4)).size == 2),
          Future(pick(change(List(2))(1)).isEmpty),
          Future(pick(change(List(1, 9, 5, 6))(11)).size == 2),
          Future(pick(change(List(357, 239, 73, 52))(9832)).size == 35),
          Future(pick(change(List(470, 35, 120, 81, 121))(9825)).size == 30),
          Future(pick(change(List(186, 419, 83, 408))(6249)).size == 20)
        )),
      Duration.Inf
    )

    results.zipWithIndex.foreach {
      case (result, ndx) => println(s"""${ndx + 1}: ${if (result) "pass" else "fail"}""")
    }
    assert(results.forall(_ == true))
  }

  def pick[A: Integral](xs: List[A]): List[A] = {
    val integral = implicitly[Integral[A]]
    import integral._

    @tailrec
    def _pick(ys: List[A], acc: List[A]): List[A] =
      if (ys.isEmpty) {
        acc
      } else {
        val num = ys.head
        if (num < integral.zero) {
          List.empty
        } else {
          _pick(ys.drop(integral.toInt(num)), ys.head :: acc)
        }
      }

    _pick(xs.reverse, List.empty)
  }

  def change[@specialized(Int, Long) A: Integral](coins: List[A], acc: List[A] = List.empty)(amount: A): List[A] =
    change(coins, implicitly[Integral[A]].one, acc)(amount)

  @tailrec
  def change[@specialized(Int, Long) A: Integral](coins: List[A], current: A, acc: List[A])(amount: A): List[A] = {
    val integral = implicitly[Integral[A]]
    import integral._

    if (current > amount) {
      acc
    } else {
      def _change(balance: A, coins: List[A], acc: List[A], current: List[A]): List[List[A]] =
        coins.map(coin =>
          toInt(balance - coin) match {
            case 0 => current :+ coin
            case x if x > 0 => backtrack(fromInt(x), acc, current :+ coin)
            case _ => List.empty[A]
          })

      val xss = _change(current, coins.filter(_ <= current), acc, List.empty[A])
      if (xss.isEmpty) {
        change(coins, integral.fromInt(integral.toInt(current) + 1), acc :+ integral.fromInt(-1))(amount)
      } else {
        val yss: List[List[A]] = xss.filter(_.nonEmpty)
        change(coins,
          integral.fromInt(integral.toInt(current) + 1),
          acc :+ yss
            .minByOption(_.size)
            .fold(integral.fromInt(-1))(_.head))(amount)
      }
    }
  }

  @tailrec
  private def backtrack[@specialized(Int, Long) A: Integral](balance: A, acc: List[A], current: List[A]): List[A] = {
    val integral = implicitly[Integral[A]]
    import integral._

    if (balance == zero) {
      current
    } else {
      val lastCoin: A = acc(toInt(balance) - 1)
      if (lastCoin < zero) {
        List.empty[A]
      } else {
        backtrack(balance - lastCoin, acc, current :+ balance)
      }
    }
  }
}
