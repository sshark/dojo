package org.teckhooi.dojo2.dynamic

import scala.annotation.tailrec
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}

/**
  * This is an over engineered solution. Please refer to QuickCoinChange.scala instead
  */
object CoinsChangeInt {

  def main(args: Array[String]): Unit = {
    val results = Await.result(
      Future.sequence(
        List(
          Future(pick(change(List(1, 2, 5), 11)).size == 3),
          Future(pick(change(List(10, 5), 15)).size == 2),
          Future(pick(change(List(2), 3)).isEmpty),
          Future(pick(change(List(1, 2, 3, 21), 63)).size == 3),
          Future(pick(change(List(2), 4)).size == 2),
          Future(pick(change(List(2), 1)).isEmpty),
          Future(pick(change(List(1, 9, 5, 6), 11)).size == 2),
          Future(pick(change(List(357, 239, 73, 52), 9832)).size == 35),
          Future(pick(change(List(470, 35, 120, 81, 121), 9825)).size == 30),
          Future(pick(change(List(186, 419, 83, 408), 6249)).size == 20)
        )),
      Duration.Inf
    )

    results.zipWithIndex.foreach {
      case (result, ndx) => println(s"""${ndx + 1}: ${if (result) "pass" else "fail"}""")
    }
    assert(results.forall(_ == true))
  }

  def pick(xs: List[Int]): List[Int] = {
    @tailrec
    def _pick(ys: List[Int], acc: List[Int]): List[Int] =
      if (ys.isEmpty) acc
      else {
        val num = ys.head
        if (num < 0) List.empty
        else _pick(ys.drop(num), ys.head :: acc)
      }

    _pick(xs.reverse, List.empty)
  }

  def change(coins: List[Int], amount: Int, acc: List[Int] = List.empty, current: Int = 1): List[Int] =
    if (current > amount) acc
    else {
      def _change(balance: Int, coins: List[Int], acc: List[Int], current: List[Int]): List[List[Int]] =
        coins.map(coin =>
          balance - coin match {
            case 0 => current :+ coin
            case x if x > 0 => backtrack(x, acc, current :+ coin)
            case _ => List.empty[Int]
          })

      val xss = _change(current, coins.filter(_ <= current), acc, List.empty[Int])
      if (xss.isEmpty) change(coins, amount, acc :+ -1, current + 1)
      else {
        val yss: List[List[Int]] = xss.filter(_.nonEmpty)
        change(coins,
          amount,
          acc :+ yss
            .minByOption(_.size)
            .fold(-1)(_.head),
          current + 1)
      }
    }

  @tailrec
  private def backtrack(balance: Int, acc: List[Int], current: List[Int]): List[Int] =
    if (balance == 0) current
    else {
      val lastCoin = acc(balance - 1)
      if (lastCoin < 0) List.empty[Int]
      else backtrack(balance - lastCoin, acc, current :+ balance)
    }
}
