package org.teckhooi.dojo2.dynamic

import java.lang.Math._

object LargestSubarray {
  def largestSubArray(ints: Seq[Int]): Seq[Int] = {
    def largestSubArray_(
      zs: Seq[Int],
      localMax: Int = 0,
      globalMax: Int = Int.MinValue,
      start: Int = 0,
      end: Int = 1,
      globalStart: Int = 0,
      globalEnd: Int = 1
    ): (Int, Int) =
      zs match {
        case Nil =>
          if (globalMax == Int.MinValue) (0, 0) else (globalStart, globalEnd)
        case x :: xs =>
          val local = max(x + localMax, x)
          val global = max(globalMax, local)
          if (x + localMax > x && globalMax < local)
            largestSubArray_(xs, local, global, start, end + 1, start - 1, end)
          else if (x + localMax > x && globalMax > local)
            largestSubArray_(
              xs,
              local,
              global,
              start,
              end + 1,
              globalStart,
              globalEnd
            )
          else if (x + localMax < x && globalMax > local)
            largestSubArray_(
              xs,
              local,
              global,
              end,
              end + 1,
              globalStart,
              globalEnd
            )
          else // if (x + localMax < x && globalMax <= local)
            largestSubArray_(xs, local, global, end, end + 1, end - 1, end)
      }

    val (start, end) = largestSubArray_(ints)
    ints.slice(start, end)
  }
}
