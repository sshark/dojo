package org.teckhooi.dojo2.dynamic

import scala.annotation.tailrec

/**
  * Changing from mutable Array to immutable List deteriorates performance. However, changing to Vector from List, which
  * is a linked list in Scala, improves performance.
  */
object QuickCoinChange extends App {
  def coinChange(coins: Vector[Int], amount: Int): Vector[Int] = {

    @tailrec
    def _coinChange(currAmt: Int, acc: Vector[Int], coinsAcc: Vector[Int]): (Vector[Int], Vector[Int]) =
      if (currAmt > amount) (acc, coinsAcc)
      else {
        val xs: Vector[(Int, Int)] = coins.filter(currAmt >= _).map(coin => (coin, _backTrack(currAmt - coin, acc)))
        if (xs.isEmpty)
          _coinChange(currAmt + 1, acc, coinsAcc)
        else {
          val minPos = xs.minBy(_._2)
          _coinChange(currAmt + 1, acc.updated(currAmt, minPos._2), coinsAcc.updated(currAmt, minPos._1))
        }
      }

    def _backTrack(x: Int, acc: Vector[Int]): Int =
      if (acc(x) == Int.MaxValue) Int.MaxValue
      else acc(x) + 1

    val result = Vector.fill(amount + 1)(Int.MaxValue).updated(0, 0)
    if (amount == 0) Vector.empty
    else _coinChange(1, result, result)._2
  }

  def pick(xs: Vector[Int]): Vector[Int] = {
    @tailrec
    def _pick(ys: Vector[Int], acc: Vector[Int]): Vector[Int] =
      if (ys.head == 0) acc
      else {
        val num = ys.head
        if (num == Int.MaxValue) Vector.empty
        else _pick(ys.drop(num), num +: acc)
      }

    _pick(xs.reverse, Vector.empty)
  }

  assert(pick(coinChange(Vector(1, 2, 5), 11)).size == 3)
  assert(pick(coinChange(Vector(2), 3)).isEmpty)
  assert(pick(coinChange(Vector(186, 419, 83, 408), 6249)).size == 20)
  assert(pick(coinChange(Vector(1, 2, 3, 21), 63)).size == 3)
  assert(pick(coinChange(Vector(2), 4)).size == 2)
  assert(pick(coinChange(Vector(2), 1)).isEmpty)
  assert(pick(coinChange(Vector(357, 239, 73, 52), 9832)).size == 35)
  assert(pick(coinChange(Vector(470, 35, 120, 81, 121), 9825)).size == 30)
  assert(pick(coinChange(Vector(1, 9, 5, 6), 11)).size == 2)
}
