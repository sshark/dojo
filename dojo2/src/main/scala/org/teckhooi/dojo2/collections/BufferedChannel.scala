package org.teckhooi.dojo2.collections

object BufferedChannel { // extends cats.effect.IOApp {
  /*def newChannel[A, F[_] : Concurrent]: F[UnboundedChannel[A, F]] =
    for {
      r <- MVar.uncancelableEmpty[F, Item[A, F]]
      a <- MVar.uncancelableOf[F, Stream[A, F]](r)
      b <- MVar.uncancelableOf[F, Stream[A, F]](r)
    } yield UnboundedChannel(a, b)

  // Same effect as that of the `uncancelableXXX` equivalent; but it can
  // introduce memory leaks if i/you are not careful.
  def newChannel2[A, F[_] : Concurrent]: F[UnboundedChannel[A, F]] = for {
    r <- MVar.empty[F, Item[A, F]]
    a <- MVar.of[F, Stream[A, F]](r)
    b <- MVar.of[F, Stream[A, F]](r)
  } yield UnboundedChannel(a, b)

  def readChannel[A, F[_] : Concurrent](ch: UnboundedChannel[A, F]): F[A] = {
    for {
      stream <- ch.reader.take
      item <- stream.read
      _ <- ch.reader.put(item.tail)
    } yield item.head
  }

  def writeChannel[A, F[_] : Concurrent](ch: UnboundedChannel[A, F], value: A): F[Unit] = {
    for {
      newHole <- MVar.empty[F, Item[A, F]]
      oldHole <- ch.writer.take
      _ <- ch.writer.put(newHole)
      _ <- oldHole.put(Item(value, newHole))
    } yield ()
  }

  def dupChan[A, F[_] : Concurrent](ch: UnboundedChannel[A, F]): F[UnboundedChannel[A, F]] = {
    for {
      hole <- ch.writer.read
      newReadVar <- MVar.uncancelableOf[F, Stream[A, F]](hole)
    } yield UnboundedChannel(newReadVar, ch.writer)
  }

  def unGetChan[A, F[_] : Concurrent](ch: UnboundedChannel[A, F], value: A): F[Unit] = {
    for {
      newReadEnd <- MVar.empty[F, Item[A, F]]
      readEnd <- ch.reader.take
      _ <- newReadEnd.put(Item(value, readEnd))
      _ <- ch.reader.put(newReadEnd)
    } yield {}
  }

  def sumChannel[F[_] : Concurrent](ch: UnboundedChannel[Int, F], sum: Long): F[Long] = {
    val F = implicitly[Concurrent[F]]
    for {
      flag <- ch.reader.read
      empty <- flag.isEmpty
      result <- if (empty) F.pure(sum) else
        F.suspend {
          for {
            stream <- ch.reader.take
            item <- stream.read
            _ <- ch.reader.put(item.tail)
            sum <- sumChannel(ch, sum + item.head.toLong)
          } yield sum
        }
    } yield result
  }

  def run(args: List[String]): IO[ExitCode] = {

    // Ungetting the UnboundedChannel sounds a little weird and unprofessional,
    // but what it really does is to push a value back onto the read end of the UnboundedChannel.
    def ungetChanTask[F[_] : Concurrent] =
      for {
        channel <- newChannel[Int, F]
        _ <- writeChannel(channel, 1)
        a <- readChannel(channel)
        _ <- unGetChan(channel, 2)
        b <- readChannel(channel)
      } yield (a, b)

    // duplicate a channel after two values are deposited
    // and then write two more values (i.e. 2) into the duplicate
    // channel and draw some values from it.
    def dupChanTask[F[_] : Concurrent] =
      for {
        channel <- newChannel[Int, F]
        _ <- writeChannel(channel, 1)
        _ <- writeChannel(channel, 2)
        d <- readChannel(channel)
        dup <- dupChan(channel)
        _ <- writeChannel(dup, 3)
        _ <- writeChannel(dup, 4)
        _ <- writeChannel(dup, 5)
        a <- readChannel(dup)
        b <- readChannel(dup)
        c <- readChannel(dup)
      } yield (a, b, c, d)

    // Putting data values in and draining them out
    def putNDrain =
      for {
        channel <- newChannel[Int, IO]
        a <- writeChannel(channel, 1).start
        b <- writeChannel(channel, 2).start
        c <- writeChannel(channel, 3).start
        d <- writeChannel(channel, 4).start
        el1 <- readChannel(channel)
        el2 <- readChannel(channel)
        el3 <- readChannel(channel)
        el4 <- readChannel(channel)
        _ <- a.join // in this scenario, it has no effect as there're no downstream operations
        _ <- b.join
      } yield (el1, el2, el3, el4)

    // Put data values and collects them applying a reduction.
    def sumTask =
      for {
        channel <- newChannel[Int, IO]
        _ <- writeChannel(channel, 1).start
        _ <- writeChannel(channel, 2).start
        _ <- writeChannel(channel, 3).start
        _ <- writeChannel(channel, 4).start
        _ <- writeChannel(channel, 5).start
        _ <- writeChannel(channel, 6).start
        _ <- writeChannel(channel, 7).start
        _ <- writeChannel(channel, 8).start
        _ <- writeChannel(channel, 9).start
        sum <- sumChannel(channel, 0L)
      } yield sum

    // Concurrent writes with interfering reads.
    def sumTask2 =
      for {
        channel <- newChannel[Int, IO]
        _ <- writeChannel(channel, 1).start
        _ <- writeChannel(channel, 2).start
        _ <- writeChannel(channel, 3).start
        _ <- writeChannel(channel, 4).start
        _ <- readChannel(channel) *> readChannel(channel) *> readChannel(channel)
        _ <- writeChannel(channel, 5).start
        _ <- writeChannel(channel, 6).start
        _ <- readChannel(channel) *> readChannel(channel) *> readChannel(channel)
        _ <- writeChannel(channel, 7).start
        _ <- writeChannel(channel, 8).start
        _ <- writeChannel(channel, 9).start
        sum <- sumChannel(channel, 0L)
      } yield sum

    putNDrain.map(x => println(s"Result of draining an channel is: $x")) *>
      sumTask.map(x => println(s"Result of summing an channel with no reads is: $x")) *>
      sumTask2.map(x => println(s"Result of summing an channel with interfering reads is: $x")) *>
      dupChanTask[IO].map(x => println(s"Result of duplicating an channel is: $x")) *>
      IO.pure(ExitCode.Success)

*/
}
