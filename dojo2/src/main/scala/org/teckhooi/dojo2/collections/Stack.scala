package org.teckhooi.dojo2.collections

trait Stack[+A]

case class Stk[A](a: Seq[A]) extends Stack[A]

object Stack {
  def push[A](a: A, stack: Stack[A]): Stack[A] = stack match {
    case Stk(xs) => Stk(a +: xs)
  }

  def pop[A](stack: Stack[A]): Stack[A] = stack match {
    case Stk(x :: xs) => Stk(xs)
    case Stk(Nil) => throw new NoSuchElementException("pop cannot work with empty stack")
  }

  def top[A](stack: Stack[A]): A = stack match {
    case Stk(x :: xs) => x
    case Stk(Nil) => throw new NoSuchElementException("top cannot work with empty stack")
  }

  def empty[A]: Stack[A] = Stk(Seq.empty[A])

  def isEmpty[A](stack: Stack[A]): Boolean = stack match {
    case Stk(xs) => false
    case _ => true
  }
}
