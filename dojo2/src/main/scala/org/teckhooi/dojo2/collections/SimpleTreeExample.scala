package org.teckhooi.dojo2.collections

/**
  *
  * @author Lim, Teck Hooi
  *
  */

sealed trait SimpleTree[+A] {
  val head: A

  def toList: SimpleLinkedList[A] = {
    def _toListByDepth(t: SimpleTree[A], acc: SimpleLinkedList[A]): SimpleLinkedList[A] = t match {
      case Branch(v, l, r) => v :: _toListByDepth(l, acc) ++ _toListByDepth(r, acc)
      case Leaf(v) => v :: acc
    }

    _toListByDepth(this, SimpleLinkedList.empty[A])
  }
}

case class Branch[+A](value: A, left: SimpleTree[A], right: SimpleTree[A]) extends SimpleTree[A] {
  override val head = value
}

case class Leaf[+A](value: A) extends SimpleTree[A] {
  override val head = value
}

object SimpleTreeExample extends App {
  val tree = Branch(0, Branch(1, Leaf(2), Branch(10, Branch(11, Leaf(99), Leaf(77)), Leaf(30))), Leaf(20))

  SimpleLinkedList.print(tree.toList)
}
