package org.teckhooi.dojo2.collections

sealed trait SimpleLinkedList[+A] {
  def hasNext: Boolean

  def head: A

  def last: SimpleLinkedList[A]

  def append[B >: A](a: B): SimpleLinkedList[B] = this match {
    case Node(x, xs) => Node(x, xs.append(a))
    case EmptyNode => Node(a, EmptyNode)
  }

  def ::[B >: A](a: B): SimpleLinkedList[B] = Node(a, this)

  def ++[B >: A](ll: SimpleLinkedList[B]): SimpleLinkedList[B] = {
    def _appendAll(partialList: SimpleLinkedList[B], acc: SimpleLinkedList[B]): SimpleLinkedList[B] = partialList match {
      case EmptyNode => acc
      case Node(v, t) => _appendAll(partialList.last, acc append partialList.head)
    }

    _appendAll(ll, this)
  }
}

case class Node[+A](value: A, tail: SimpleLinkedList[A]) extends SimpleLinkedList[A] {
  def hasNext: Boolean = tail != EmptyNode

  def head: A = value

  def last: SimpleLinkedList[A] = tail
}

case object EmptyNode extends SimpleLinkedList[Nothing] {
  def hasNext = false

  def head: Nothing = throw new IllegalArgumentException("empty list has no head")

  def last: SimpleLinkedList[Nothing] = throw new IllegalArgumentException("empty list has no tail")
}

object SimpleLinkedList {
  def empty[A]: SimpleLinkedList[A] = EmptyNode

  def print[A](l: SimpleLinkedList[A]): Unit = {
    if (l.hasNext) {
      Console.print(s"${l.head} -> ")
      print(l.last)
    }
    else println(s"${l.head} -> Nil")
  }
}

object SimpleLinkedListExample extends App {
  val n = Node(2, Node(1, EmptyNode))
  val m = 3 :: n.append(10)

  SimpleLinkedList.print(m)
  SimpleLinkedList.print(m ++ n)
}


