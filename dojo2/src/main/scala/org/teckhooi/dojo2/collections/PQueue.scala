package org.teckhooi.dojo2.collections

trait PQueue[+A]

case class PQ[A: Ordering](a: Seq[A]) extends PQueue[A]

object PQueue {
  def empty[A: Ordering]: PQueue[A] = PQ(Seq.empty[A])

  def isEmpty[A](q: PQueue[A]): Boolean = q match {
    case PQ(Nil) => true
    case _ => false
  }

  def enqueue[A: Ordering](a: A, q: PQueue[A]): PQueue[A] = {
    val ordering = implicitly[Ordering[A]]
    import ordering._

    q match {
      case PQ(x :: xs) => PQ(if (x > a) {
        a +: x +: xs
      } else {
        x +: a +: xs
      })
      case PQ(Nil) => PQ(a +: Seq.empty[A])
    }
  }

  def dequeue[A: Ordering](q: PQueue[A]): PQueue[A] = q match {
    case PQ(x :: xs) => PQ(xs)
    case PQ(Nil) => throw new NoSuchElementException("Cannot dequeue empty queue")
  }

  def top[A: Ordering](q: PQueue[A]): A = q match {
    case PQ(x :: xs) => x
    case PQ(Nil) => throw new NoSuchElementException("Cannot view empty queue")
  }
}
