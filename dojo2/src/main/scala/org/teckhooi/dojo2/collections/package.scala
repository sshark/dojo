package org.teckhooi.dojo2

package object collections {


  /*
    type Stream[A, F[_]] = MVar[F, Item[A, F]]

    case class Item[A, F[_]](head: A, tail: Stream[A, F])

    case class Channel[A, F[_] : Concurrent](
      reader: MVar[F, Stream[A, F]],
      writer: MVar[F, Stream[A, F]],
      max: Ref[F, Int]) {
    }

    case class UnboundedChannel[A, F[_] : Concurrent](
      reader: MVar[F, Stream[A, F]],
      writer: MVar[F, Stream[A, F]])
  */
}
