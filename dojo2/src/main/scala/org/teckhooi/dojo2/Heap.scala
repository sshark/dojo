package org.teckhooi.dojo2

import scala.collection.mutable.ArrayBuffer

trait Heap[A] {
  /*
    def isEmpty: Boolean
    def merge(as: Heap[A]): Heap[A]
    def insert(a: A): Heap[A]
    def findMin: A
    def deleteMin(): Heap[A]
  */
}

class LeftistHeap(ints: List[Int]) extends Heap[Int] {

  private var buffer: ArrayBuffer[Int] = makeHeap(ints, new ArrayBuffer[Int]())

  private def parentNdx(ndx: Int): Int = (ndx - 1) / 2

  private def childLeftNdx(parent: Int): Int = 2 * parent + 1

  private def childRightNdx(parent: Int): Int = 2 * parent + 2

  def isEmpty: Boolean = buffer.isEmpty

  private def bubbleUp(buffer: ArrayBuffer[Int], ndx: Int, last: Int): Unit = {
    if (ndx != 0) {
      val parent = buffer(parentNdx(ndx))
      if (parent > last) {
        buffer(parentNdx(ndx)) = last
        buffer(ndx) = parent
      }
      bubbleUp(buffer, parentNdx(ndx), last)
    }
  }

  def add(x: Int): Unit = {
    add(x, buffer)
  }

  private def add(x: Int, buffer: ArrayBuffer[Int]): Unit = {
    buffer.append(x)
    bubbleUp(buffer, buffer.size - 1, buffer.last)
  }

  private def makeHeap(xs: List[Int], buffer: ArrayBuffer[Int]): ArrayBuffer[Int] = {
    if (xs.isEmpty) buffer
    else {
      add(xs.head, buffer)
      makeHeap(xs.tail, buffer)
    }
  }

  def findMin: Option[Int] = buffer.headOption

  def deleteMin(): Option[Int] = {
    val top = buffer.headOption
    if (buffer.size == 1) {
      buffer.remove(0)
    } else if (buffer.nonEmpty) {
      buffer.remove(0)
      val last = buffer.remove(buffer.size - 1)
      buffer.prepend(last)
      bubbleDown(buffer, 0, buffer.head)
    }
    top
  }

  private def bubbleDown(buffer: ArrayBuffer[Int], ndx: Int, value: Int): Unit = {
    val leftNdx = childLeftNdx(ndx)
    val rightNdx = childRightNdx(ndx)
    (if (childLeftNdx(ndx) > buffer.length - 1) None else Some(buffer(leftNdx)),
      if (childRightNdx(ndx) > buffer.length - 1) None else Some(buffer(rightNdx))) match {
      case (Some(x), None) => if (value > x) {
        swap(buffer, leftNdx, ndx)
        bubbleDown(buffer, leftNdx, buffer(leftNdx))
      }
      case (None, Some(x)) => if (value > x) {
        swap(buffer, rightNdx, ndx)
        bubbleDown(buffer, rightNdx, buffer(rightNdx))
      }
      case (Some(x), Some(y)) =>
        if (value > x && value > y) {
          if (x < y) {
            swap(buffer, leftNdx, ndx)
            bubbleDown(buffer, leftNdx, buffer(leftNdx))
          } else {
            swap(buffer, rightNdx, ndx)
            bubbleDown(buffer, rightNdx, buffer(rightNdx))
          }
        }
      case (None, None) =>
    }
  }

  private def swap[A](buffer: ArrayBuffer[A], i: Int, j: Int): Unit = {
    val temp = buffer(i)
    buffer(i) = buffer(j)
    buffer(j) = temp
  }

  def ordered: List[Int] = {
    def sort(buffer: LeftistHeap, xs: List[Int]): List[Int] = {
      if (buffer.isEmpty) xs
      else {
        sort(buffer, buffer.deleteMin().map(xs :+ _).get)
      }
    }

    sort(new LeftistHeap(buffer.toList), List.empty)
  }

  def list: List[Int] = buffer.toList
}
