package org.teckhooi.dojo2

object RadixSort {
  def sort(l: Seq[Int]) = {
    def _sort(m: Seq[Int], decPos: Int): Seq[Int] =
      if (decPos > maxDecPos) m
      else
        _sort(
          arrange(m, Seq.fill(m.length)(0), countsSum(mark(m, Seq.fill(10)(0), decPos)), decPos),
          decPos * 10
        )

    val (negatives, positives) = l.partition(_ < 0)
    _sort(negatives.map(Math.abs), 1).map(Math.negateExact) ++ _sort(positives, 1).reverse
  }

  private def arrange(in: Seq[Int], out: Seq[Int], acc: Seq[Int], decPos: Int): Seq[Int] =
    if (in.isEmpty) out
    else {
      val digit = digits(in.head, decPos)
      val pos   = acc(digit)

      arrange(in.tail, out.updated(out.length - pos, in.head), acc.updated(digit, pos - 1), decPos)
    }

  private def mark(m: Seq[Int], counts: Seq[Int], tens: Int): Seq[Int] =
    if (m.isEmpty) counts
    else {
      val ndx = digits(m.head, tens)
      mark(m.tail, counts.updated(ndx, counts(ndx) + 1), tens)
    }

  private def digits(d: Int, tens: Int) = (d / tens) % 10

  private def countsSum(l: Seq[Int]) = l.scanLeft(0)(_ + _).tail

  private val maxDecPos = 1000000000
}
