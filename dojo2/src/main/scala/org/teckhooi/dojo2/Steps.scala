package org.teckhooi.dojo2

object Steps extends App {

  def stepsSet(
      height: Int,
      stepsAllowed: List[Int],
      acc: List[Int] = List.empty,
      xss: List[List[Int]] = List.empty
  ): List[List[Int]] =
    if (height == 0) acc :: xss
    else if (height < 0) xss
    else
      stepsAllowed.foldLeft(xss)((yss, c) =>
        yss ++ stepsSet(height - c, stepsAllowed, c :: acc, xss)
      )
}
