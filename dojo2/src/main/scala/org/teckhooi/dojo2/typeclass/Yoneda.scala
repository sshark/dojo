package org.teckhooi.dojo2.typeclass

import scala.language.higherKinds

abstract class Yoneda[F[_], A] {
  self =>
  def transformation[B](f: A => B): F[B]

  def run: F[A] = transformation(identity)

  def map[B](f: A => B): Yoneda[F, B] = new Yoneda[F, B] {
    override def transformation[C](g: B => C): F[C] = self.transformation(g compose f)
  }
}

object Yoneda extends App {
  private val plus1 = (_: Int) + 1
  private val plus2 = (_: Int) + 2
  private val plus3 = (_: Int) + 3

  import Implicits._

  val s: List[String] = toYoneda(List(1, 2, 3))
    .map(plus1)
    .map(plus2)
    .map(plus3)
    .map(_.toString)
    .run

  val t: List[String] = List(1, 2, 3)
    .map(plus1)
    .map(plus2)
    .map(plus3)
    .map(_.toString)

  def toYoneda[F[_], A](fa: F[A])(implicit ev: Functor[F]): Yoneda[F, A] =
    new Yoneda[F, A] {
      override def transformation[B](f: A => B): F[B] = ev.map(fa)(f)
    }

  s.foreach(println)

  def fromYoneda[F[_], A](lf: Yoneda[F, A]): F[A] = lf.run

  t.foreach(println)
}


