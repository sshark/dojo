package org.teckhooi.dojo2.typeclass

import scala.language.implicitConversions

case class Reader[E, A](f: E => A)

/**
  * Alternatively, map and flatMap methods can be added to Reader class which makes it easier
  * to support for-comprehension for Scala. However,the knowledge is not transferable to Haskell
  *
  * case class Reader[E, A](f: E => A) {
  * def map[B](g: A => B): Reader[E, B]                = Reader(g.compose(f))
  * def flatMap[B](g: A => Reader[E, B]): Reader[E, B] = Reader((e: E) => g(f(e)).f(e))
  * }
  */

/**
  * Monad is not required if Reader class is defined with map and flatMap methods. Class with
  * methods is simpler to work with for-comprehension than a method-less class
  */
trait Monad[F[_]] extends Functor[F] {
  def pure[A](x: A): F[A]

  override def map[A, B](fa: F[A])(f: A => B): F[B] = flatMap(fa)((x: A) => pure(f(x)))

  def flatMap[A, B](fa: F[A])(f: A => F[B]): F[B]
}

/**
  * Monad is not required if Reader class is defined with map and flatMap methods
  */
object Monad {
  def apply[F[_]](implicit F: Monad[F]): Monad[F] = F

  implicit class MonadSyntax[F[_], A](fa: F[A]) {
    def map[B](f: A => B)(implicit F: Monad[F]): F[B] = F.map(fa)(f)

    def flatMap[B](afb: A => F[B])(implicit F: Monad[F]): F[B] = F.flatMap(fa)(afb)
  }
}


/**
  * ReaderMonad is not required if Reader class is defined with map and flatMap methods
  */
object ReaderMonad {
  implicit def monadReader[E]: Monad[Reader[E, *]] =
    new Monad[Reader[E, *]] {
      override def pure[A](a: A): Reader[E, A] = Reader(_ => a)

      override def flatMap[A, B](fa: Reader[E, A])(f: A => Reader[E, B]): Reader[E, B] =
        Reader((e: E) => f(fa.f(e)).f(e))
    }
}

object ReaderApp extends App {

  import Monad._
  import ReaderMonad._

  assert((for {
    a <- Reader[Int, Int](_ + 10)
    b <- Reader[Int, Int](_ * 3)
  } yield a + b).f(2) == 18)
}
