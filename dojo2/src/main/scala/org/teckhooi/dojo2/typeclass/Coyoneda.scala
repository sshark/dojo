package org.teckhooi.dojo2.typeclass

import org.teckhooi.dojo2.typeclass.Coyoneda.toCoyoneda

import scala.language.higherKinds

sealed trait Coyoneda[F[_], A] {
  type UnderlyingType

  val underlyingValue: F[UnderlyingType]

  val transformation: UnderlyingType => A

  def run(f: Functor[F]): F[A] = f.map(underlyingValue)(transformation)

  def map[B](f: A => B): Coyoneda[F, B] = Coyoneda.toCoyoneda(underlyingValue)(transformation andThen f)
}

object Coyoneda {
  def toCoyoneda[F[_], A, B](fa: F[A])(f: A => B): Coyoneda[F, B] = new Coyoneda[F, B] {
    override type UnderlyingType = A
    override val underlyingValue: F[A] = fa
    override val transformation: UnderlyingType => B = f
  }
}

case class Person[A](a: A)

object RunCoyenda extends App {
  val person: Coyoneda[Person, Int] = toCoyoneda(Person("42"))(_.toInt)
    .map(_ + 1).map(_ + 2).map(_ + 3)
  val personFunctor = new Functor[Person] {
    override def map[A, B](fa: Person[A])(f: A => B): Person[B] = Person(f(fa.a))
  }

  println(person.run(personFunctor).a)
}
