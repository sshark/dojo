package org.teckhooi.dojo2

object ZeroOneKnapsack {
  trait ValueWeightProps[A] {
    def value(a: A): Int

    def weight(a: A): Int
  }

  def max[A: Ordering](a: A, b: A): A = if (implicitly[Ordering[A]].compare(a, b) > 0) a else b

  def bestFitChart[A: ValueWeightProps](weight: Int, xs: List[A])(implicit
      ev: Ordering[List[A]]
  ): List[List[List[A]]] = {

    def _emptyBag(numItems: Int, weight: Int): List[List[List[A]]] =
      List.fill(numItems + 1)(List.fill(weight + 1)(List.empty))

    def _bestFitChart(bag: List[List[List[A]]], ndx: Int, w: Int): List[List[List[A]]] = {
      val vwr = implicitly[ValueWeightProps[A]]

      if (ndx > xs.size) {
        bag
      } else if (w > weight) {
        _bestFitChart(bag, ndx + 1, 1)
      } else {
        val itemWeight = vwr.weight(xs(ndx - 1))
        if (itemWeight > w) {
          _bestFitChart(bag.updated(ndx, bag(ndx).updated(w, bag(ndx - 1)(w))), ndx, w + 1)
        } else {
          val v1: List[A] = bag(ndx - 1)(w)
          val v2: List[A] = xs(ndx - 1) :: bag(ndx - 1)(w - vwr.weight(xs(ndx - 1)))
          _bestFitChart(bag.updated(ndx, bag(ndx).updated(w, max(v1, v2))), ndx, w + 1)
        }
      }
    }

    _bestFitChart(_emptyBag(xs.length, weight), 1, 1)
  }
}
