val foo: LazyList[Option[String]] = None #:: None #:: Some("Foo") #:: foo
val bar: LazyList[Option[String]] = None #:: None #:: None #:: None #:: Some("Bar") #:: bar

val foobar = (LazyList.from(1) zip foo zip bar).map {
  case ((_, Some(x)), Some(y)) => x + y
  case ((_, Some(x)), None) => x
  case ((_, None), Some(x)) => x
  case ((w, None), None) => w.toString
}

val foo: LazyList[String] = "" #:: "" #:: "Foo" #:: foo
val bar: LazyList[String] = "" #:: "" #:: "" #:: "" #:: "Bar" #:: bar

val foobar = (LazyList.from(1) zip foo zip bar).map {
  case ((w, x), y) => if ((x + y).isEmpty) w.toString else x + y
}

foobar.take(30).toList
