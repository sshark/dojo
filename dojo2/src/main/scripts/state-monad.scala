case class Better(total: List[Int])

object Better {

  def sum(i: Int)(b: Better): (Better, Int) = {
    val result = if (b.total.length > 1) 99 else b.total.sum
    val newBetter = b.copy(total = i :: b.total)
    (newBetter, result)
  }
}


type BetterUpdate = Better => (Better, Int)

val firstF: BetterUpdate = Better.sum(1)
val secondF: BetterUpdate = Better.sum(2)
val thirdF: BetterUpdate = Better.sum(3)


//     the external state is threaded through a sequence of computations
val composeSumResults: BetterUpdate = better => {

  val (firstState, firstResult) = firstF(better)
  val (secondState, secondResult) = secondF(firstState)
  val (thirdStater, thirdResult) = thirdF(secondState) // now when this blows up, we have secondState in scope for inspection

  (thirdStater, firstResult + secondResult + thirdResult)
}

case class StateChange[S, A](run: S => (S, A)) {

  def mapResult[B](f: A => B): StateChange[S, B] = StateChange { (s: S) =>
    val (s2, a) = run(s)
    (s2, f(a))
  }

  /*
   A magical moment!!!!!!  Now I can stick 2 StateChange's together: the resulting
   StateChange runs the first one, then uses the resulting state a the param for
   the second one: I have now threaded my state through 2 computations.
   */

  def doWithNewState[B](f: A => StateChange[S, B]): StateChange[S, B] = StateChange { (s: S) =>
    val (s2, a) = run(s)
    val stateChangeToB = f(a)
    val (s3, b) = stateChangeToB.run(s2)
    (s3, b)
  }
}

def sum(i: Int) = StateChange[Better, Int](s => Better.sum(i)(s))

// Lets run the sum computation, and add all the results together

def composeAddResultS(one: Int, two: Int, three: Int): StateChange[Better, Int] =
  sum(one).doWithNewState(
    firstResult => {
      println(3);
      sum(two).doWithNewState(secondResult => {
        println(2);
        sum(three).mapResult(thirdResult => {
          println(1); firstResult + secondResult + thirdResult
        })
      })
    })
