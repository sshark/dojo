implicit object IntShow extends Show[Int] {
  override def show(a: Int) = a.toString
}

implicit def optionShow[T: Show]: Show[Option[T]] = (_: Option[T]) => {
  case Some(x) => implicitly[Show[T]].show(x)
  case None => "Nothing"
}

/*
implicit object StringShow extends Show[String] {
  override def show(a: String) = s"The length of $a is ${a.length}"
}
*/

implicit val noneShow = new Show[None.type] {
  override def show(a: None.type) = "Yes, really nothing"
}
implicit val fruitShow = new Show[Fruit] {
  override def show(a: Fruit) = "my Fruit"
}

/* alternative to the def above
implicit def optionShow[T](implicit ev: Show[T]): Show[Option[T]] = new Show[Option[T]] {
    override def show(a: Option[T]) = a match {
            case Some(x) => ev.show(x)
            case None => "Nothing"
        }
    }
}
*/
var bar: Option[Int] = None // or Option[Int] if it is provided in the implicits

def foo[A: Show](a: A): String = implicitly[Show[A]].show(a)

foo(Option(123))
foo(Some(456))
foo(bar)
foo(None)
foo(Option(null)) // Option[Null]


// run with commented StringShow object, then run again with StringShow uncommented
//foo(Option("abc"))

trait Show[-A] {
  def show(a: A): String
}
trait Fruit
case class Orange() extends Fruit
case object Apple extends Fruit

implicitly[Show[Orange]]
// implicitly[Show[Apple]]  // compile error, won't work with object or case object

