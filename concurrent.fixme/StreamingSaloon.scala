package org.teckhooi.fp.dojo2.concurrent

import cats.effect.concurrent.Ref
import cats.effect.{ConcurrentEffect, ExitCode, IO, IOApp, Sync, Timer}
import cats.syntax.all._
import fs2.Pipe
import fs2.concurrent.Queue

import java.util.concurrent.TimeUnit
import scala.concurrent.duration._

object StreamingSaloon extends IOApp {
  case class CustomersQC(total: Long = 0, serviced: Long = 0, left: Long = 0)

  val maxChairs: Int                 = 5
  val openingSeconds: FiniteDuration = 3.second
  val maxBarbers: Int                = 3

  override def run(args: List[String]): IO[ExitCode] =
    for {
      previous <- timer.clock.realTime(TimeUnit.MILLISECONDS)
      accRef <- Ref.of[IO, CustomersQC](CustomersQC())
      _ <- program(accRef).compile.drain
      now <- timer.clock.realTime(TimeUnit.MILLISECONDS)
      acc <- accRef.get
      _ <- IO(
        println(s"Total time used to service ${acc.serviced} customers out of ${acc.total} is ${now - previous}ms")
      )
    } yield
      if (acc.total == acc.serviced + acc.left) ExitCode.Success
      else ExitCode.Error

  def program(acc: Ref[IO, CustomersQC]): fs2.Stream[IO, Unit] =
    fs2.Stream.eval(Queue.bounded[IO, Option[Long]](maxChairs)).flatMap { q =>
      val p = customers[IO](10.millis, 110.millis, openingSeconds).evalMap(writeToQueue[IO](q, acc))
      val c = q.dequeue.unNoneTerminate.parEvalMap(maxBarbers)(barbers[IO](300.millis, 400.millis))
      c.concurrently(p)
    }

  def barbers[F[_] : ConcurrentEffect : Timer](minDuration: FiniteDuration, maxDuration: FiniteDuration)(
    id: Long
  ): F[Unit] =
    ConcurrentEffect[F]
      .defer(randomDelay(minDuration, maxDuration))
      .as(println(s"Barber ${Thread.currentThread().getId} cutting customer $id hair"))

  def writeToQueue[F[_] : Sync](queue: Queue[F, Option[Long]], accRef: Ref[F, CustomersQC])(
    chunk: Option[Long]
  ): F[Unit] =
    chunk match {
      case None => queue.enqueue1(chunk) *> Sync[F].delay(println("Shop is CLOSED"))
      case _ =>
        queue
          .offer1(chunk)
          .flatMap(
            b =>
              if (b)
                accRef
                  .update(acc => acc.copy(total = acc.total + 1, serviced = acc.serviced + 1))
                  .map(
                    _ =>
                      chunk
                        .foreach(id =>
                          println(s"Customer $id walks into the saloon (${Thread.currentThread().getName})")))
              else
                accRef
                  .update(acc => acc.copy(total = acc.total + 1, left = acc.left + 1))
                  .map(_ => chunk.foreach(id => println(s"Customer $id LEAVES (${Thread.currentThread().getName})"))))
    }

  def randomDelay[F[_] : Sync : Timer, A](minDuration: FiniteDuration, maxDuration: FiniteDuration): F[Unit] =
    Sync[F]
      .delay(scala.util.Random.between(minDuration.toMillis, maxDuration.toMillis).millis)
      .map(delay => Timer[F].sleep(delay))

  def randomDelayStream[F[_] : ConcurrentEffect : Timer, A](
    minDuration: FiniteDuration,
    maxDuration: FiniteDuration
  ): Pipe[F, A, A] =
    _.flatMap(value => fs2.Stream.eval(randomDelay(minDuration, maxDuration)).as(value))

  def customers[F[_] : ConcurrentEffect : Timer](
    minDuration: FiniteDuration,
    maxDuration: FiniteDuration,
    openingHours: FiniteDuration
  ): fs2.Stream[F, Option[Long]] =
    fs2.Stream
      .constant(1L)
      .scan1(_ + _)
      .map(Some.apply)
      .through(randomDelayStream(minDuration, maxDuration))
      .interruptWhen(fs2.Stream.sleep[F](openingHours).as(true))
      .onComplete(fs2.Stream.emit(None))
}
