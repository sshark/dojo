package org.teckhooi.fp.dojo2.concurrent

import cats.effect._
import cats.effect.kernel._
import cats.syntax.apply.catsSyntaxApply

import scala.concurrent.duration.{FiniteDuration, _}

object CatsPubSub extends IOApp {

  // Signaling option, because we need to detect completion
  /*
    type Channel[A] = MVar[IO, Option[A]]
  
    override def run(args: List[String]): IO[ExitCode] =
      (for {
        close <- Ref.of[IO, Boolean](false)
        _ <- signalCloseAfter(close, 1.seconds).start
        channel <- MVar[IO].empty[Option[Int]]
        _ <- producer(channel, close, 0).start
        fc <- consumer(channel, 0L).start
        sum <- fc.join
      } yield println(sum)).as(ExitCode.Success)
  
    def signalCloseAfter[F[_] : Sync : Timer](closeShop: Ref[F, Boolean], closingSeconds: FiniteDuration): F[Unit] =
      Timer[F].sleep(closingSeconds) *> closeShop.set(true)
  
    def consumer(ch: Channel[Int], sum: Long): IO[Long] =
      IO(println("consuming")) *> ch.take.flatMap(_.fold(IO.pure(sum))(x => consumer(ch, sum + x)))
  
    def producer(ch: Channel[Int], close: Ref[IO, Boolean], counter: Int): IO[Unit] =
      close.get.flatMap(
        b =>
          if (b)
            ch.put(None) // we are done!
          else
            IO(println(s"counter: $counter")) *> ch.put(Some(counter)) *> producer(ch, close, counter + 1))
  */

}
