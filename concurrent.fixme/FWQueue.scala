package org.teckhooi.fp.dojo2.concurrent

import cats.effect._
import cats.effect.concurrent._
import cats.effect.implicits._
import cats.implicits._

import scala.concurrent.duration._

/**
  * unbounded queue, `dequeue` semantically blocks on empty queue
  */
trait FWQueue[F[_], A] {
  def enqueue(a: A): F[Unit]

  def dequeue: F[A]

  def enqueue1(a: A): F[Boolean]

  def dequeue1: F[Option[A]]
}

object FWQueue {
  def bounded[F[_] : Concurrent, A](max: Int): F[FWQueue[F, A]] =
    Ref[F]
      .of(Vector.empty[A] -> Vector.empty[Deferred[F, A]])
      .map { state =>
        new FWQueue[F, A] {
          def enqueue(a: A): F[Unit] =
            state.modify {
              case (elems, deqs) if deqs.isEmpty =>
                (elems :+ a, deqs) -> ().pure[F]
              case (elems, deq +: deqs) =>
                (elems, deqs) -> deq.complete(a).start.void
            }.flatten

          def enqueue1(a: A): F[Boolean] =
            state.modify {
              case (elems, deqs) if deqs.isEmpty && elems.size < max =>
                println(s"a $elems")
                (elems :+ a, deqs) -> true.pure[F]
              case (elems, deq +: deqs) if elems.size < max =>
                println("b $elems")
                (elems, deqs) -> deq.complete(a).start.void.as(true)
              case (elems, deqs) =>
                println(s"c $elems")
                (elems, deqs) -> false.pure[F]
            }.flatten

          def dequeue: F[A] =
            Deferred[F, A].flatMap { wait =>
              state.modify {
                case (elems, deqs) if elems.isEmpty =>
                  (elems, deqs :+ wait) -> wait.get
                case (e +: elems, deqs) =>
                  (elems, deqs) -> e.pure[F]
              }
            }.flatten

          def dequeue1: F[Option[A]] =
            state.modify {
              case (elems, deqs) if elems.isEmpty =>
                (elems, deqs) -> None
              case (e +: elems, deqs) =>
                (elems, deqs) -> Some(e)
            }
        }
      }
}

object RunQueue extends IOApp {
  override def run(args: List[String]): IO[ExitCode] =
    for {
      queue <- FWQueue.bounded[IO, Int](2)
      b <- (IO.sleep(500.millis) *> queue.enqueue1(10))
      _ <- (IO.sleep(100.millis) *> queue.enqueue1(20))
      a <- (IO.sleep(100.millis) *> queue.enqueue1(30))
      w <- queue.dequeue1
      x <- queue.dequeue1
      _ <- (IO.sleep(100.millis) *> queue.enqueue1(40))
      _ <- (IO.sleep(100.millis) *> queue.enqueue1(50))
      y <- queue.dequeue1
      z <- queue.dequeue1
      _ <- IO(println(List(w, x, y, z)))
    } yield ExitCode.Success

  //      IO.pure(ExitCode.Success)
}
