package org.teckhooi.fp.dojo2.concurrent

import org.teckhooi.dojo3.concurrent.SleepingBarberShop

import java.util.concurrent._
import java.util.concurrent.atomic.{AtomicBoolean, AtomicInteger}
import java.util.{Optional, Random}

object WannabeBarberShop {
  val MAX_CHAIRS: Int = 3
  val POISON_PILL: Int = -1
  val BARBERS: Int = 3

  private[concurrent] val waitingRoom: BlockingQueue[Integer] = new LinkedBlockingQueue[Integer](MAX_CHAIRS)

  @throws[Exception]
  def main(args: Array[String]): Unit = {
    val barberShop: SleepingBarberShop = new SleepingBarberShop
    val rnd: Random = new Random
    val open: AtomicBoolean = new AtomicBoolean(true)
    val leftAtomic: AtomicInteger = new AtomicInteger
    val servicedAtomic: AtomicInteger = new AtomicInteger
    val waitedAtomic: AtomicInteger = new AtomicInteger
    val totalAtomic: AtomicInteger = new AtomicInteger
    val shopIsOpen: AtomicBoolean = new AtomicBoolean(true)
    val barber: Runnable = () => {
      while (shopIsOpen.get) {
        val customerOpt: Optional[java.lang.Long] = barberShop.customer
        customerOpt.ifPresent(customer =>
          if (customer < 0) {
            shopIsOpen.set(false)
          } else {
            System.out.println(
              String.format("Barber %s servicing customer %d hair", Thread.currentThread.getId, customer)
            )
            servicedAtomic.incrementAndGet
            try Thread.sleep(50 + rnd.nextInt(100))
            catch {
              case e: InterruptedException =>
                e.printStackTrace()
            }
          }
        )
      }
    }

    val customers: Runnable = () => {
      while (open.get) {
        if (!(barberShop.fullHouse)) {
          barberShop.walkIn(totalAtomic.getAndIncrement + 1)
          waitedAtomic.incrementAndGet
          System.out.println(String.format("Customer %d walked in", totalAtomic.get))
        } else {
          System.out.println(String.format("Customer %d LEFT", totalAtomic.get))
          leftAtomic.incrementAndGet
          totalAtomic.incrementAndGet
        }
        try Thread.sleep(rnd.nextInt(50))
        catch {
          case e: InterruptedException =>
            e.printStackTrace()
        }
      }
      barberShop.close()
    }

    val executorService: ScheduledExecutorService = Executors.newSingleThreadScheduledExecutor
    executorService.schedule(new Runnable {
      override def run(): Unit = {
        open.set(false)
        System.out.println("Barber shop CLOSED")
      }
    }, 3, TimeUnit.SECONDS)

    val barbers: Array[Thread] = new Array[Thread](BARBERS)
    for (i <- 0 until BARBERS) {
      val t: Thread = new Thread(barber)
      barbers(i) = t
      t.start()
    }

    val customersThread: Thread = new Thread(customers)
    customersThread.start()
    for (i <- 0 until BARBERS) {
      barbers(i).join()
    }

    customersThread.join()
    executorService.shutdown()
    val total: Int = totalAtomic.get
    val waited: Int = waitedAtomic.get
    val serviced: Int = servicedAtomic.get
    val left: Int = leftAtomic.get
    System.out.println(String.format("Total: %d, waited: %d, serviced: %d, left: %d", total, waited, serviced, left))
    assert((total == serviced + left))
    assert((waited == serviced))
  }
}
