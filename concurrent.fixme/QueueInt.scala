package org.teckhooi.fp.dojo2.concurrent

import cats.effect.{ExitCode, IO, IOApp, Timer}
import fs2._
import fs2.concurrent.Queue

import scala.concurrent.duration._

class QueueInt(q: Queue[IO, Int])(implicit timer: Timer[IO]) { //I need implicit timer for metered
  def startPushingToQueue: Stream[IO, Unit] =
    Stream(1, 2, 3)
      .covary[IO]
      .evalTap(n =>
        IO.delay(println(s"Pushing element $n to Queue"))
      )                    //eval tap evaluates effect on an element but doesn't change stream
      .metered(500.millis) //it will create 0.5 delay between enqueueing elements of stream,
      // I added it to make visible that elements can be pushed and pulled from queue concurrently
      .through(q.enqueue)

  def pullAndPrintElements: Stream[IO, Unit] =
    q.dequeue.evalMap(n => IO.delay(println(s"Pulling element $n from Queue")))
}

object testingQueues extends IOApp {
  override def run(args: List[String]): IO[ExitCode] = {

    val program = for {
      q <- Queue.bounded[IO, Int](10)
      b = new QueueInt(q)
      _ <- b.startPushingToQueue.compile.drain.start //start at the end will start running stream in another Fiber
      _ <- b.pullAndPrintElements.compile.drain //compile.draing compiles stream into io byt pulling all elements.
    } yield ()

    program.as(ExitCode.Success)
  }

}
